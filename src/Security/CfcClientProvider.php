<?php

namespace App\Security;

use App\Entity\Wcocon;
use App\Entity\Wdeclar;
use App\Services\CustomerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class CfcClientProvider implements UserProviderInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager, CustomerService $customerService)
    {
        $this->entityManager = $entityManager;
        $this->customerService     = $customerService;
    }

    public function loadUserByUsername($username)
    {
       dd($username);
        $user = $this->entityManager->getRepository(Wdeclar::class)->findOneBy([
            'wde_dossier' => $username,
        ]);
       
        if ( ! $user) {
            throw new UsernameNotFoundException(
                sprintf(
                    'User with "%s" username does not exist.',
                    $username
                )
            );
        }

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        if(!empty($_SESSION['_sf2_attributes']['_security_main'])){
            $token=unserialize($_SESSION['_sf2_attributes']['_security_main']);
            $user_data=$user->getSsoSyncData();
        }elseif(!empty($_SESSION['user_data']["token"]))
        {
            $user_data=$_SESSION['user_data']["token"];
        }

        if(empty($token)){
            return false;
        }
        $reloaded_user=$this->customerService->syncUser($user_data);
        return $reloaded_user;

    }
    public function supportsClass($class)
    {
        return $class=="Proxies\__CG__\App\Entity\Wcocon" || $class === Wcocon::class ;
    }
}
