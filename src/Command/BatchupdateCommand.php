<?php 
namespace App\Command;

use App\Entity\Wconf;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Filesystem\Filesystem;
use Twig\Environment;

use App\Entity\Wcocon;
use App\Entity\Wdeclar;
use App\Entity\Wtype;




class BatchupdateCommand extends Command
{
    private $nb_thread;
    private $my_argument_name;

    /**
     *  @var ManagerRegistry
     */
    private $doctrine;
    /**
     * @var Environment
     */
    protected $twig;
    /** 
     * @var \Swift_Mailer
     */
    private $mailer;

    public function __construct(ManagerRegistry $doctrine, Environment $twig, \Swift_Mailer $mailer, ParameterBagInterface $params)
    {
        parent::__construct();

        $this->doctrine = $doctrine;
        $this->twig = $twig;
        $this->mailer = $mailer;
        $this->em = $this->doctrine->getManager();
        $this->params = $params;
    }

    protected function configure()
    {
        $this->setName('delef:batch:update')
            ->setDescription('apply batch update configuration')
            ->setHelp('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->property_accessor = PropertyAccess::createPropertyAccessor();
        
        
        
        $qb=$this->em->createQueryBuilder();
        $qb->select('c')->from('App\Entity\Wconf', 'c');
        $confs=$qb->getQuery()->getResult();
        foreach ($confs as $c) {
            $objConf=$c->getTypeObjetConf();
            if($c->getSynchroField()=="wco_copos" || $c->getSynchroField()=="wco_tel"){
              
                $objConf['display_column']=1;
                $objConf['display_label_size']=4;

            }
            
            if($c->getSynchroField()=="wco_ville"){
                $objConf['display_column']=2;
                $objConf['display_label_size']=2;
                $objConf['label_class']="text-right pr-2";

            
            }
            if($c->getSynchroField()=="wco_email"){
                $objConf['display_column']=2;
                $objConf['display_label_size']=2;
                $objConf['label_class']="text-right pr-2";
                $c->setLabel("Email");
            
            }
            
            if($c->getSynchroField()=="wde_tel_resp"){
               $objConf['display_column']=1;
               $objConf['display_label_size']=4;

            }
            if($c->getSynchroField()=="wde_mel_resp"){
                $objConf['display_column']=2;
                $objConf['display_label_size']=2;
                $objConf['label_class']="text-right pr-2";

            
            }
            
           
            
            /*if($c->getSynchroField()=="wde_fax_resp"){
                $c->setActif(false);
            
            }*/
            
            if($c->getSynchroField()=="wco_nom_legal"){
               
                $objConf['contact_is_contact']="1";
                $objConf['contact_field_civilite']="wco_tit_legal";
                $objConf['contact_field_function']="wco_fct_legal";
                $objConf['contact_field_email']="wco_mel_legal";
            }
            if($c->getSynchroField()=="wde_nom_resp"){
                 
                $objConf['contact_is_contact']="1";
                $objConf['contact_field_civilite']="wde_tel_resp";
                $objConf['contact_field_function']="wde_fct_resp";
                $objConf['contact_field_email']="wde_mel_resp";
            }
            
            
            $c->setTypeObjetConf($objConf);
            $this->em->persist($c);
            $this->em->flush();
            
        }
       
        die();
        
        
        
        $from_email = $this->params->get('delef_from_email');
        $from_name = $this->params->get('delef_from_name');

        $io = new SymfonyStyle($input, $output);
        
        $io->title('Delef Email Validation');
        
        /** @var Wdeclar[] */
        $qb=$this->em->createQueryBuilder();
        $qb->select('d')->from('App\Entity\Wdeclar', 'd')
        ->leftJoin(
            'App\Entity\Wtype',
            't',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'd.wde_typcont = t.label'
            )
        ->where('d.wde_etat_declar = :wde_etat_declar')
        ->AndWhere('d.wde_email_sent = :wde_email_sent')
        //->AndWhere('t.priorite = :priorite')
        ->setParameter('wde_etat_declar', 'F')
        ->setParameter('wde_email_sent', 0)
        ->groupBy('d.wde_dossier');
        //->setParameter('priorite', 0)
        ;
        $declars=$qb->getQuery()->getResult();

        foreach ($declars as $declar) {
            $wcocon = $declar->getWdeDossier();
            /** @var Wtype */
            $wtype = $this->doctrine
                ->getRepository(Wtype::class)
                ->findOneBy(['label' => $declar->getWdeTypcont()])
            ;
            /** @var Wconf[] */
            $wconfs = $this->doctrine
                ->getRepository(Wconf::class)
                ->findBy([
                    'wconf_typcont' => $wtype->getId(),
                    'actif'  => TRUE
                ])
            ;

            $mail_content = $this->twig->render(
                'emails/validation.html.twig', [
                    'signature' => $wtype->getEmailSignature()
                ]
            );

            foreach ($wconfs as $wconf) {
                switch ($wconf->getValidationEmail()) {
                    case 1:
                        $primary = $this->getEmailValue($declar, $wcocon,  $wconf->getSynchroField());
                        break;
                    case 2:
                        $secondary = $this->getEmailValue($declar, $wcocon, $wconf->getSynchroField());
                        break;
                    case 3:
                        $secondary_bis = $this->getEmailValue($declar, $wcocon, $wconf->getSynchroField());
                        break;
                }
            }

            $recipients = [];
            //if primary is filled => primary + secondary
            if (!empty($primary)) {
                $recipients[] = $primary;
                $recipients[] = $secondary;
                
            }else{
                //if primary is empty => secondary + secondary bis
                $recipients[] = $secondary;
                if(!empty($secondary_bis)){
                    $recipients[] = $secondary_bis;
                }
            }


            $message = (new \Swift_Message('Declaration CFC - Validation'))
                ->setFrom($from_email, $from_name)
                ->setTo($recipients)
                ->setBody($mail_content, 'text/html')
            ;
   
            $WcoconRepository = $this->doctrine->getRepository(Wcocon::class);
            $pdf_file_path = $WcoconRepository->GeneratePdf($declar->getWdeDeclar());

            if ($pdf_file_path) {
                $message->attach(\Swift_Attachment::fromPath($pdf_file_path['path']));
            }

            if ($this->mailer->send($message) == 0) {
                $io->error('Error sending email!');
            } else {
                $io->success('Emails sent succesfully.');
                $cn = $this->em->getConnection();
                $stmt = $cn->prepare("update wdeclar set wde_email_sent=1  where wde_dossier='".$wcocon->getWcoDossier()."' ;");
                $stmt->execute();
            }
        }
    }
    
    private function getEmailValue($declar, $wcocon, $field_name){
        
        if (substr($field_name, 0, 3) == 'wde') {
            $target_email = $this->property_accessor->getValue($declar, $field_name);
        }
        else if (substr($field_name, 0, 3) == 'wco') {
            $target_email = $this->property_accessor->getValue($wcocon, $field_name);
        }
        return $target_email;
    }
}
