<?php 
namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\Console\Style\SymfonyStyle;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Filesystem\Filesystem;


class ImportChorusCommand extends ContainerAwareCommand
{
    private $container;
    private $nb_thread;
    private $my_argument_name;
    
    protected function configure()
    {
        $this
        // the name of the command (the part after "bin/console")
        ->setName('delef:import:chorus')
        
        // the short description shown while running "php bin/console list"
        ->setDescription('Import Chorus database')
        
        // the full command description shown when running the command with
        // the "--help" option
        ->setHelp('');
        
        //add an argument to the task
        //$this->addArgument('extension', InputArgument::REQUIRED, 'my_argument_name');
        
    }
    
    public function __construct(ContainerInterface $container)
    {
        // best practices recommend to call the parent constructor first and
        // then set your own properties. That wouldn't work in this case
        // because configure() needs the properties set in this constructor
        //$this->requirePassword = $requirePassword;
    
        parent::__construct();
        $this->container = $container;
        
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        
        
        //$this->my_argument_name = $input->getArgument('my_argument_name');
        $io->title('Import des données chorus');
        
        //$file_url="https://chorus-pro.gouv.fr/cpp/static/far0037/courant/FAR0037.zip";
        $file_url="https://cpro.chorus-pro.gouv.fr/cpp/static/far0037/courant/FAR0037.zip";
        $io->note('url : '.$file_url);
        $filesystem = new Filesystem();
        $filesystem->mkdir('./var/chorus', 0700);
        
        $filesystem->remove(['./var/chorus/FAR0037.zip']);
        $filesystem->remove(['./var/chorus/extract/']);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $file_url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec ($ch);
        $error = curl_error($ch);
        curl_close ($ch);

        if(!empty($error)){
            $io->error($error);
            exit;
        }else{
            $io->text('Fichier correctement téléchargé.');
            $file = fopen('./var/chorus/FAR0037.zip', "w+");
            fputs($file, $data);
            fclose($file);
        }
        
        $zip = new \ZipArchive;
        if ($zip->open('./var/chorus/FAR0037.zip') === TRUE) {
            $filesystem->mkdir('./var/chorus/extract/', 0700);
            $zip->extractTo('./var/chorus/extract/');
            $zip->close();
            $files = scandir('./var/chorus/extract/');
            
        } else {
           $io->error("Le fichier './var/chorus/FAR0037.zip' ne peux pas être ouvert.");
           exit;
        }
        
        if(count($files)!=3){
            $io->error("Impossible de détecter le fichier xml");
            exit;
        }else{
            $xml_file=$files[2];
            $io->text('Fichier trouvé : '.$xml_file);
        }
     
        $string=file_get_contents('./var/chorus/extract/'.$xml_file);
        $xml = simplexml_load_string($string);
        
        //print_r($xml->CPPStructurePartenaireUnitaire);
        
        $number_items=count($xml);
        $io->text('Nombre d\'enregistrements : '.$number_items);
        $io->section('Insertion en base');
        $io->progressStart($number_items);
        
        $cn = $this->container->get('doctrine')->getManager()->getConnection();
        
        $stmt = $cn->prepare("TRUNCATE `chorus`");
        $stmt->execute();
        $stmt = $cn->prepare("SET autocommit=0");
        $stmt->execute();
        $stmt = $cn->prepare("SET unique_checks=0;");
        $stmt->execute();
        $stmt = $cn->prepare("SET foreign_key_checks=0;");
        $stmt->execute();
        foreach($xml as $t=>$i){
            $type_identifiant    = @$i->TypeIdentifiant;
            $identifiant        = @$i->Identifiant;
            $raison_sociale      = @$i->RaisonSociale;
            $adresse            = @$i->AdressePostale->Adresse;
            $code_postal         = @$i->AdressePostale->CodePostal;
            $ville              = @$i->AdressePostale->Ville;
            $pays_code          = @$i->AdressePostale->Pays->Code;
            $pays_libelle       = @$i->AdressePostale->Pays->Libelle;
            $emetteur_edi        = @$i->EmetteurEdi=="true"?1:0;;
            $recepteur_edi       = @$i->RecepteurEdi=="true"?1:0;
            $gestion_statut_mise_en_paiement = @$i->GestionStatutMiseEnPaiement=="true"?1:0;
            $gestion_engagement  = @$i->GestionEngagement=="true"?1:0;
            $gestion_service     = @$i->GestionService=="true"?1:0;
            $gestion_service_engagement=@$i->GestionServiceEngagement=="true"?1:0;
            $est_MOA             = @$i->EstMOA=="true"?1:0;
            $est_MOA_uniquement   = @$i->EstMOAUniquement=="true"?1:0;
            $structure_active    = @$i->StructureActive=="true"?1:0;
            //19974578700010
            if(!empty($i->Services->Service)){
                foreach($i->Services->Service as $s){
                   /* if($s->Code!="FACTURES_PUBLIQUES")
                    {*/
                        $service_code           = $s->Code;
                        $service_nom            = $s->Nom;
                        $service_gestion_EGMT   = $s->GestionEGMT=="true"?1:0;
                        $service_service_actif  = $s->ServiceActif=="true"?1:0;
                        
                        
                        $sql="INSERT INTO `chorus` (`id`, `type_identifiant`, `identifiant`, `raison_sociale`, `adresse`, `code_postal`, `ville`, `pays_code`, `pays_libelle`, `service_code`, `service_nom`, `service_gestion_egmt`, `service_service_actif`, `emetteur_edi`, `recepteur_edi`, `gestion_statut_mise_en_paiement`, `gestion_engagement`, `gestion_service`, `gestion_service_engagement`, `est_moa`, `est_moa_uniquement`, `structure_active`) VALUES (
                            NULL,
                            :type_identifiant,
                            :identifiant,
                            :raison_sociale,
                            :adresse,
                            :code_postal,
                            :ville,
                            :pays_code,
                            :pays_libelle,
                            :service_code,
                            :service_nom,
                            :service_gestion_EGMT,
                            :service_service_actif,
                            :emetteur_edi,
                            :recepteur_edi,
                            :gestion_statut_mise_en_paiement,
                            :gestion_engagement,
                            :gestion_service,
                            :gestion_service_engagement,
                            :est_MOA,
                            :est_MOA_uniquement,
                            :structure_active
                            );";
                        $stmt = $cn->prepare($sql);
                        $stmt->execute(array(
                            'type_identifiant' => $type_identifiant,
                            'identifiant' => $identifiant,
                            'raison_sociale' => $raison_sociale,
                            'adresse' => $adresse,
                            'code_postal' => $code_postal,
                            'ville' => $ville,
                            'pays_code' => $pays_code,
                            'pays_libelle' => $pays_libelle,
                            'service_code' => $service_code,
                            'pays_libelle' => $pays_libelle,
                            'service_code' => $service_code,
                            'service_nom' => $service_nom,
                            'service_gestion_EGMT' => $service_gestion_EGMT,
                            'service_service_actif' => $service_service_actif,
                            'emetteur_edi' => $emetteur_edi,
                            'recepteur_edi' => $recepteur_edi,
                            'gestion_statut_mise_en_paiement' => $gestion_statut_mise_en_paiement,
                            'gestion_engagement' => $gestion_engagement,
                            'gestion_service' => $gestion_service,
                            'gestion_service_engagement' => $gestion_service_engagement,
                            'est_MOA' => $est_MOA,
                            'est_MOA_uniquement' => $est_MOA_uniquement,
                            'structure_active' => $structure_active,
                    
                        ));
                   /* }   */
                }
            }
            $io->progressAdvance();
        }
        $stmt = $cn->prepare("COMMIT;");
        $stmt->execute();
        $stmt = $cn->prepare("SET unique_checks=1;");
        $stmt->execute();
        $stmt = $cn->prepare("SET foreign_key_checks=1;");
        $stmt->execute();
        $io->progressFinish();
       
    }
}
?>