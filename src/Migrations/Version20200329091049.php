<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200329091049 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE wtype ADD archive INT NOT NULL');
        $this->addSql('ALTER TABLE wcompos CHANGE wcp_declar wcp_declar INT NOT NULL, CHANGE wcp_libelle wcp_libelle VARCHAR(100) NOT NULL, CHANGE wcp_effectif wcp_effectif INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE wcompos CHANGE wcp_declar wcp_declar INT DEFAULT 0 NOT NULL, CHANGE wcp_libelle wcp_libelle VARCHAR(100) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci, CHANGE wcp_effectif wcp_effectif INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE wtype DROP archive');
    }
}
