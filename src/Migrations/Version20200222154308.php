<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200222154308 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE wconf (id INT AUTO_INCREMENT NOT NULL, wconf_typcont INT DEFAULT NULL, actif TINYINT(1) DEFAULT \'0\' NOT NULL, type_objet VARCHAR(255) DEFAULT NULL, label VARCHAR(255) DEFAULT NULL, aide_contextuelle LONGTEXT DEFAULT NULL, label_visible TINYINT(1) DEFAULT \'0\' NOT NULL, etape VARCHAR(255) DEFAULT NULL, synchro_field VARCHAR(255) NOT NULL, group_key VARCHAR(255) DEFAULT NULL, position INT NOT NULL, INDEX IDX_B8679D763394A2DD (wconf_typcont), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wtype (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, status INT DEFAULT 0 NOT NULL, activation_debut DATE DEFAULT NULL, activation_fin DATE DEFAULT NULL, introduction LONGTEXT DEFAULT NULL, message_rappel LONGTEXT DEFAULT NULL, introduction_ouvert LONGTEXT DEFAULT NULL, introduction_ferme LONGTEXT DEFAULT NULL, actif TINYINT(1) DEFAULT \'0\' NOT NULL, UNIQUE INDEX UNIQ_204A43F7EA750E8 (label), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE wconf ADD CONSTRAINT FK_B8679D763394A2DD FOREIGN KEY (wconf_typcont) REFERENCES wtype (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE wdeclar DROP FOREIGN KEY FK_DCA7A4F5F5B61815');
        $this->addSql('ALTER TABLE wconf DROP FOREIGN KEY FK_B8679D763394A2DD');
        $this->addSql('DROP TABLE wconf');
        $this->addSql('DROP TABLE wtype');
    }
}
