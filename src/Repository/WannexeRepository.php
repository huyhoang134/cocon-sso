<?php

namespace App\Repository;

use App\Entity\Wannexe;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Wannexe|null find($id, $lockMode = null, $lockVersion = null)
 * @method Wannexe|null findOneBy(array $criteria, array $orderBy = null)
 * @method Wannexe[]    findAll()
 * @method Wannexe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WannexeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Wannexe::class);
    }

    // /**
    //  * @return Wannexe[] Returns an array of Wannexe objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Wannexe
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
