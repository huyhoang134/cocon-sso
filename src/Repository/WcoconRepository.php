<?php

namespace App\Repository;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Twig\Environment;
use Mpdf\Mpdf;
use Mpdf\MpdfException;
use Mpdf\Output\Destination;
use Psr\Container\ContainerInterface;
use App\Entity\Wcocon;
use App\Entity\Wdeclar;
use App\Entity\Wtype;
use App\Entity\Chorus;


use Symfony\Component\HttpFoundation\RequestStack;

use App\Application\Sonata\MediaBundle\Entity\Media;
use Symfony\Component\HttpKernel\KernelInterface;

use Sonata\MediaBundle\Provider\FileProvider;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Shared\Font as Share_Font;
use Imagine\Image\Palette\RGB;

use Gedmo\Sluggable\Util\Urlizer;



/**
 * @method Wcocon|null find($id, $lockMode = null, $lockVersion = null)
 * @method Wcocon|null findOneBy(array $criteria, array $orderBy = null)
 * @method Wcocon[]    findAll()
 * @method Wcocon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WcoconRepository extends ServiceEntityRepository
{
    var $download_key='ec6bdd93b5da97cac519f4f534fd3de6';
    public function __construct(ManagerRegistry $registry,RequestStack $requestStack, Environment $twig, ParameterBagInterface $parameter, ContainerInterface $container, KernelInterface $kernel, FileProvider $provider)
    {
        $this->registry=$registry;
        $this->connection=$this->registry->getManager()->getConnection();
        $this->requestStack = $requestStack;
        $this->twig = $twig;
        $this->parameter = $parameter;
        $this->container = $container;
        $this->kernel = $kernel;
        $this->provider=$provider;
        parent::__construct($registry, Wcocon::class);
    }

    public function getChainedDeclarationConfiguration($Wcocon)
    {
        $conf=$this->getDeclarationConfiguration($Wcocon );
       
        foreach($conf as $i=>$c){
            if($c[0]->type_etape=='declaration' && $c[0]->type_interface>0){
                unset($conf[$i]);
            }
        }
        //reorder index
        $rconf=[];
        foreach($conf as $i=>$c){
            $rconf[]=$c;
        }

        $this->ChainedDeclarationConfiguration=$rconf;
        return $rconf;
    }
   
    public function getDeclarationConfigurationByWdeclar($wdeclar)
    {

        //$Wcocon=$wdeclar->getWdeDossier();
        $alldeclars=$this->getDeclarationConfiguration($wdeclar, $wtype_id=false, $forceAlldeclar=true);
        $declar=false;
        foreach($alldeclars as $d){
            if($d[0]->wde_declar==$wdeclar->getWdeDeclar()){
                $declar=$d;
            }
        }
        return [0=>$alldeclars[0],1=>$declar];
    }
    
    public function getDeclarationConfigurationByWcocon($wcocon, $wtype_id=false, $forceAlldeclar=false )
    {
        $wcocon=$wdeclar->getWdeDossier();
    }
    /** @var Wcocon */
    public function getDeclarationConfiguration($wcocon, $wtype_id=false, $forceAlldeclar=false )
    {

        $declarations=$wcocon->getWcontrats()->toArray();

        $configuration=[];
        $index_coordonnees=-2;
        foreach($declarations as $i=>$d){

            $wtype = $this->registry->getRepository('App\Entity\Wtype')->findOneBy(['label'=>$d->getWctTypcont()]);
            // Skip non-exist configuration
            if (empty($wtype)) {
                continue;
            }
            $conf = $this->registry->getRepository('App\Entity\Wconf')->createQueryBuilder('c');
            $conf->where('c.wconf_typcont  = ' . $wtype->getId());
            $conf->andWhere('c.actif  = 1');
            $conf->orderBy('c.etape', 'asc');
            $conf->addOrderBy('c.position', 'asc');


            $conf_restults = $conf->getQuery()->getResult();
            $wde_dossier = $d->getWctDossier()->getWcoDossier();
            $wde_declar  = $d->getWctContrat();
            $wde_contrat  = $d->getWctContrat();
            $wde_delef = $d->getWdelefs()[0]->getWdfDeclar();

            $wde_etat_declar = $d->getWdelefs()[0]->getWdfEtatDeclar();

            $wde_annee  = $d->getWdelefs()[0]->getWdfAnnee();
            $wde_numperiod=$d->getWdelefs()[0]->getWdfNumperiod();
            $label_annee = !empty($d->getWdelefs()[0]->getWdfIntituleDecl())?$d->getWdelefs()[0]->getWdfIntituleDecl().' '.$d->getWdelefs()[0]->getWdfAnneeDecl():null;
            $wde_annee_declar = $d->getWdelefs()[0]->getWdfAnneeDecl();


            //we remove the priority rule to chain all the declarations.
            //the order is allready set
            $label_interface=str_replace('%wde_annee%',$wde_annee, $wtype->getLabelInterface());
            $label_interface=str_replace('[wde_annee]',$wde_annee, $label_interface);

            $label_interface_home=str_replace('%wde_annee%',$wde_annee, $wtype->getLabelInterfaceHome());
            $label_interface_home=str_replace('[wde_annee]',$wde_annee, $label_interface_home);

            $label_interface     =str_replace('%wde_numperiod%',$wde_numperiod, $label_interface);
            $label_interface_home=str_replace('[wde_numperiod]',$wde_numperiod, $label_interface_home);


            $index_coordonnees=$index_coordonnees+2;
            $index_declaration=$index_coordonnees+1;

            foreach($conf_restults as $j => $items){
                if($items->getEtape()=="coordonnees"){
                    //en mode chaine, on affiche que la premiere étape de coordonnées.


                    if(empty($wtype_id) && $index_coordonnees==0){

                        $el=clone($items);
                        $el->wde_dossier=$wde_dossier;
                        $el->wdf_declar = $wde_delef;
                        $el->wde_declar=$wde_declar;
                        $el->wde_contrat=$wde_contrat;
                        $el->wde_etat_declar=$wde_etat_declar;
                        $el->wde_annee=$wde_annee;
                        $el->wde_numperiod=$wde_numperiod;
                        $el->label_annee=$label_annee;
                        $el->annee_declar=$wde_annee_declar;
                        $el->type_etape="coordonnees";
                        $el->type_interface=$wtype->getTypeInterface();
                        $el->label_interface=$label_interface;
                        $el->label_interface_home=$label_interface_home;
                        $el->type_object=$el->getTypeObjet();
                        $el->wtype=$wtype;
                        $configuration[$index_coordonnees][]=$el;
                        //en mode non chainé, on ne récupere que les coordonées du type en cours.
                    }elseif($wtype_id==$wtype->getId())
                    {

                        $el=clone($items);
                        $el->wde_dossier=$wde_dossier;
                        $el->wdf_declar = $wde_delef;
                        $el->wde_declar=$wde_declar;
                        $el->wde_contrat=$wde_contrat;
                        $el->wde_etat_declar=$wde_etat_declar;
                        $el->wde_annee=$wde_annee;
                        $el->wde_numperiod=$wde_numperiod;
                        $el->label_annee=$label_annee;
                        $el->annee_declar=$wde_annee_declar;
                        $el->type_etape="coordonnees";
                        $el->type_interface=$wtype->getTypeInterface();
                        $el->label_interface=$label_interface;
                        $el->label_interface_home=$label_interface_home;
                        $el->type_object=$el->getTypeObjet();
                        $el->wtype=$wtype;
                        $configuration[$index_coordonnees][]=$el;
                    }

                }

                if($items->getEtape()=="declaration"){

                    if(empty($wtype_id)){
                        $el=clone($items);
                        $el->wde_dossier=$wde_dossier;
                        $el->wdf_declar = $wde_delef;
                        $el->wde_declar = $wde_declar;
                        $el->wde_contrat=$wde_contrat;
                        $el->wde_etat_declar=$wde_etat_declar;
                        $el->wde_annee=$wde_annee;
                        $el->wde_numperiod=$wde_numperiod;
                        $el->label_annee=$label_annee;
                        $el->annee_declar=$wde_annee_declar;
                        $el->type_etape="declaration";
                        $el->type_interface=$wtype->getTypeInterface();
                        $el->label_interface=$label_interface;
                        $el->label_interface_home=$label_interface_home;
                        $el->type_object=$el->getTypeObjet();
                        $el->wtype=$wtype;
                        $configuration[$index_declaration][]=$el;
                    }elseif($wtype_id==$wtype->getId())
                    {
                        $el=clone($items);
                        $el->wde_dossier=$wde_dossier;
                        $el->wdf_declar = $wde_delef;
                        $el->wde_declar = $wde_declar;
                        $el->wde_contrat=$wde_contrat;
                        $el->wde_etat_declar=$wde_etat_declar;
                        $el->wde_annee=$wde_annee;
                        $el->wde_numperiod=$wde_numperiod;
                        $el->label_annee=$label_annee;
                        $el->annee_declar=$wde_annee_declar;
                        $el->type_etape="declaration";
                        $el->type_interface=$wtype->getTypeInterface();
                        $el->label_interface=$label_interface;
                        $el->label_interface_home=$label_interface_home;
                        $el->type_object=$el->getTypeObjet();
                        $el->wtype=$wtype;
                        $configuration[$index_declaration][]=$el;
                    }

                }
            }
        }
        /*dirty fix to get number chaining when extra contract don't have coordinate steps.*/
        /* Todo: optimize*/
        $conf=[];
        foreach($configuration as $c){
            $conf[]=$c;
        }

        if(count($conf)<2){
            return false;
        }

        return $conf;
    }



    public function getValue($item, $wco_dossier, $wde_declar, $wdf_declar)
    {

        $values = $this->getShowObjectValues($wco_dossier, $wde_declar, $wdf_declar);
        $item   = trim($item);

        /*table de correspondance*/
        if($item=='wde_id_ct_fact')$item='wct_id_ct_fact';
        if($item=='wde_id_ct_nom_sec_gen')$item='wct_id_ct_nom_sec_gen';
        if($item=='wde_id_ct_resp')$item='wct_id_ct_resp';
        if($item=='wde_id_adr_fact')$item='wct_id_adr_fact';

        if($item=='wde_id_ct_nom_sec_gen_question')$item='wct_id_ct_nom_sec_gen_question';
        if($item=='wde_id_ct_resp_question')$item='wct_id_ct_resp_question';
        if($item=='wde_id_ct_fact_question')$item='wct_id_ct_fact_question';
        if($item=='wde_id_adr_fact_question')$item='wct_id_adr_fact_question';

        if($item=='wde_is_chorus_pro')$item='wct_is_chorus_pro';
        if($item=='wde_siret')$item='wct_siret';


        $prefix = substr($item, 0, 3);

        if($prefix=='wde'){
            $item=str_replace('wde_', 'wct_', $item);
            $prefix='wct';
        }
        if($prefix == 'wco' && $item != 'wcompos')
        {
            switch ($item)
            {
                case 'wco_stamp':
                case 'wco_synchro':
                    return $values['wco'][$item]->date;
                default:
                    return $values['wco'][$item];
            }
        }elseif($prefix=='wct'){

            return $values['wct'][$item];
        }elseif($prefix=='wdf'){

            return $values['wdf'][$item];
        }elseif($prefix=='wde'){
            switch ($item)
            {
                case 'wde_stamp':
                case 'wde_synchro':
                case 'wde_date_validation':
                    return $values['wde'][$item]->date;
                default:
                    return $values['wde'][$item];
            }
        }elseif($prefix=='wan'){
            return !empty($values['wan'][$item])?$values['wan'][$item]:false;
        }elseif($item=='wcompos'||$item=='wcompos_stages'){
            return $values['wcompos'];
        }elseif($item=='panorama_press'){
            return $values['wpp'];
        }elseif($item=='abo_press_livres'){
            return $values['wabo'];
        }elseif($item=='abo_press'){
            return $values['wabo'];
        }elseif($item=='wprestataire'){
            return $values['wprestataire'];
        }elseif($item=='relation_publique'){
            return $values['wrp'];
        }elseif($item=='cma_btp'){
            return $values['wrp'];
        }elseif($item=='wrgpd'){
            return $values['wrgpd'];
        }elseif($item=='wcpextw'){
            return $values['wcpextw'];
        }elseif($item=='wcpextc'){
            return $values['wcpextc'];
        }else{
            /*dump($item);
            dump($values);*/
        }

        return 'value ' . $item;
    }

    public function getDeclarById($wdeclar_id)
    {
        dd('getDeclarById');
        $wde = $this->registry->getRepository('App\Entity\Wdeclar')->createQueryBuilder('d');
        $wde->where('d.wde_declar  = '.$wdeclar_id);
        $wde_results = $wde->getQuery()->getResult();
        if(!empty($wde_results))
        {
            return $wde_results[0];
        }else{
            return false;
        }
        
    }

    public function getShowObjectValues($wco_dossier, $wde_declar, $wdf_declar)
    {


        if(empty($this->ObjVal[$wco_dossier])){
            $wco = $this->registry->getRepository('App\Entity\Wcocon')->createQueryBuilder('c');
            $wco->where('c.wco_dossier  = '.$wco_dossier);
            $wco_results = $wco->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            $this->ObjVal[$wco_dossier]=$wco_results[0];
        }

        if(empty($this->ObjVal[$wde_declar])){
            $wde = $this->registry->getRepository('App\Entity\Wcontrat')->createQueryBuilder('d');
            $wde->where('d.wct_contrat  = '.$wde_declar);
            $wde_results = $wde->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

            $this->ObjVal['wct'.$wde_declar]=$wde_results[0];
        }

        if(empty($this->ObjVal[$wde_declar])){
            $wde = $this->registry->getRepository('App\Entity\Wdelef')->createQueryBuilder('d');
            $wde->where('d.wdf_declar  = '.$wdf_declar);
            $wde_results = $wde->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

            $this->ObjVal['wdf'.$wde_declar]=$wde_results[0];
        }

        /*if(empty($this->ObjVal[$wde_declar])){
            $wde = $this->registry->getRepository('App\Entity\Wdeclar')->createQueryBuilder('d');
            $wde->where('d.wde_declar  = '.$wde_declar);
            $wde_results = $wde->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

            $this->ObjVal[$wde_declar]=$wde_results[0];
        }*/

        if(empty($this->ObjVal['wan'.$wde_declar])){
            $wan = $this->registry->getRepository('App\Entity\Wannexe')->createQueryBuilder('a');
            $wan->where('a.wan_declar  = '.$wdf_declar);
            $wan_results = $wan->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            $this->ObjVal['wan'.$wde_declar]=!empty($wan_results[0])?$wan_results[0]:false;
        }
        //see ENS/UNI to test (ex : dossier 166459 	or 166460)
        // /admin/app/viewwtype/18/wcocon/3024/show
        if(empty($this->ObjVal['wcompos'.$wde_declar])){
            $wcompos = $this->registry->getRepository('App\Entity\Wcompos')->createQueryBuilder('a');
            $wcompos->where('a.wcp_declar  = '.$wdf_declar);
            $wcompos_results = $wcompos->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            $this->ObjVal['wcompos'.$wde_declar]=!empty($wcompos_results)?$wcompos_results:false;
        }
        if(empty($this->ObjVal['wpp'.$wde_declar])){
            $wpp_array=[];
            $wpp = $this->registry->getRepository('App\Entity\Wpanoramapress')->createQueryBuilder('w');
            $wpp->where('w.wppDeclar  = '.$wdf_declar);
            $wpp_results = $wpp->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            //$wpp_results = $wpp->getQuery()->getResult();
            if(!empty($wpp_results)){
                $wppId=$wpp_results[0]["wppId"];
                $wpp_array=[
                    'wpp_id' => $wpp_results[0]["wppId"],
                    /*'wpp_prestataire' => $wpp_results[0]["wppPrestataire"],
                    'wpp_prestataire_label' => $wpp_results[0]["wppPrestataireLabel"],*/
                ];
                $wppl = $this->registry->getRepository('App\Entity\WpanoramapressListe')->createQueryBuilder('wl');
                $wppl->where('wl.wpplWpp  = '.$wppId);
                $wppl_results = $wppl->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

                //$wpp_array['wpanoramapress_liste']=$wppl_results;
                if(!empty($wppl_results)){
                    foreach($wppl_results as $i=>$list){
                        if(!empty($list['wpplFichier'])){
                            $mediaManager = $this->container->get('sonata.media.manager.media');
                            $media=$mediaManager->find($list['wpplFichier']);
                            $file_name=$media->getName();
                            $file_reference=$this->provider->getReferenceFile($media,'reference')->getName();
                            $media_key=md5($list['wpplFichier'].$this->download_key).'%'.$list['wpplFichier'];
                        }else{
                            $file_name=false;
                            $media=false;
                            $media_key=false;
                            $file_reference=false;
                        }


                        $wpp_array['wpanoramapress_liste'][$i]=[
                            'wppl_id'=>$list['wpplId'],
                            'wppl_label'=>$list['wpplLabel'],
                            'wppl_periode'=>$list['wpplPeriode'],
                            'wppl_periode_nombre'=>$list['wpplPeriodeNombre'],
                            'wppl_moyenne_repro'=>$list['wpplMoyenneRepro'],
                            'wppl_moyenne_exemplaire'=>$list['wpplMoyenneExemplaire'],


                            'wppl_total'=>$list['wpplTotal'],
                            'wppl_fichier'=>$list['wpplFichier'],



                            'wppl_fichier_name'=>$file_name,
                            'wppl_fichier_media'=>$media,
                            'wppl_fichier_media_key'=>$media_key,
                            'wppl_fichier_media_reference'=>$file_reference,

                            'wppl_is_chorus_pro'=>$list['wpplIsChorusPro'],
                            'wppl_siret'=>$list['wpplSiret'],
                            'wppl_service_code'=>$list['wpplServiceCode'],
                            'wppl_service_list'=>$this->registry->getRepository(Chorus::class)->findServicesBySiret( $list['wpplSiret'] ),
                            'wppl_has_order'=>$list['wpplHasOrder'],
                            'wppl_order_number'=>$list['wpplOrderNumber'],

                        ];
                        $wpplp = $this->registry->getRepository('App\Entity\WpanoramapressListePublication')->createQueryBuilder('wlp');
                        $wpplp->where('wlp.wpplpWppl  = '.$list['wpplId']);
                        $wpplp_results = $wpplp->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
                        if(!empty($wpplp_results)){
                            foreach($wpplp_results as $j=>$publication){
                                $wpp_array['wpanoramapress_liste'][$i]['wpanoramapress_liste_publication'][$j]=['wpplp_label'=>$publication['wpplpLabel'], 'wpplp_total'=> $publication['wpplpTotal']];
                            }
                        }else{
                            $wpp_array['wpanoramapress_liste'][$i]['wpanoramapress_liste_publication']=[];
                        }

                    }
                }else{
                    $wpp_array['wpanoramapress_liste']=[0=>[
                        'wppl_id'=>"",
                        'wppl_label'=>"",
                        'wppl_periode'=>"",
                        'wppl_periode_nombre'=>"",
                        'wppl_moyenne_repro'=>"",
                        'wppl_moyenne_exemplaire'=>"",
                        'wppl_total'=>"",
                        'wppl_fichier'=>"",
                        'wppl_fichier_media_key'=>"",
                        'wppl_fichier_media_reference'=>"",
                        'wppl_fichier_name'=>"",

                        'wppl_is_chorus_pro'=>'',
                        'wppl_siret'=>'',
                        'wppl_service_code'=>'',
                        'wppl_order_number'=>'',
                        'wppl_service_list'=>[],
                        'wppl_has_order'=>'',
                        'wppl_order_number'=>'',


                        'wpanoramapress_liste_publication'=>[0=>['wpplp_label'=>'','wpplp_total'=>'']]
                    ]];
                }

            }else{
                $wpp_array=[
                    'wpp_id' => false,
                    /*'wpp_prestataire' => null,
                    'wpp_prestataire_label' => "",*/
                    'wpanoramapress_liste'=>[0=>[
                        'wppl_label'=>"",
                        'wppl_periode'=>"",
                        'wppl_periode_nombre'=>"",
                        'wppl_moyenne_repro'=>"",
                        'wppl_moyenne_exemplaire'=>"",
                        'wppl_total'=>"",
                        'wppl_fichier'=>"",
                        'wppl_fichier_media_key'=>"",
                        'wppl_fichier_media_reference'=>"",
                        'wppl_fichier_name'=>"",

                        'wppl_is_chorus_pro'=>'',
                        'wppl_siret'=>'',
                        'wppl_service_code'=>'',
                        'wppl_order_number'=>'',
                        'wppl_service_list'=>[],
                        'wppl_has_order'=>'',
                        'wppl_order_number'=>'',

                        'wpanoramapress_liste_publication'=>[0=>['wpplp_label'=>'','wpplp_total'=>'']]
                    ]]
                ];
            }

            $this->ObjVal['wpp'.$wde_declar]=!empty($wpp_array)?$wpp_array:false;
        }

        if(empty($this->ObjVal['wabo'.$wde_declar])){
            $wabo_array=[];
            $wabo = $this->registry->getRepository('App\Entity\Wabo')->createQueryBuilder('w');
            $wabo->where('w.waboDeclar  = '.$wdf_declar);
            $wabo_results = $wabo->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            if(!empty($wabo_results)){

                if(!empty($wabo_results[0]["waboFichier"])){
                    $mediaManager = $this->container->get('sonata.media.manager.media');
                    $media=$mediaManager->find($wabo_results[0]["waboFichier"]);
                    $file_name=$media->getName();
                    $file_reference=$this->provider->getReferenceFile($media,'reference')->getName();
                    $media_key=md5($wabo_results[0]["waboFichier"].$this->download_key).'%'.$wabo_results[0]["waboFichier"];
                }else{
                    $file_name=false;
                    $media=false;
                    $media_key=false;
                    $file_reference=false;
                }

                $wabo_array=[
                    "wabo_id"=>$wabo_results[0]["waboId"],
                    "wabo_declar"=>$wabo_results[0]["waboDeclar"],
                    "wabo_dossier"=>$wabo_results[0]["waboDossier"],
                    "wabo_contrat"=>$wabo_results[0]["waboContrat"],
                    "wabo_prestataire"=>$wabo_results[0]["waboPrestataire"],
                    "wabo_prestataire_label"=>$wabo_results[0]["waboPrestataireLabel"],
                    "wabo_fichier"=>$wabo_results[0]["waboFichier"],

                    'wabo_fichier_name'=>$file_name,
                    'wabo_fichier_media'=>$media,
                    'wabo_fichier_media_key'=>$media_key,
                    'wabo_fichier_media_reference'=>$file_reference,


                    "wabo_type"=>$wabo_results[0]["waboType"]
                ];


                $waboPublication = $this->registry->getRepository('App\Entity\WaboPublication')->createQueryBuilder('wp');

                $waboPublication->where('wp.wabopWabo  = '.$wabo_array["wabo_id"]);
                $wabo_array['wabo_publications']=$waboPublication->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            }else{
                $wabo_array=[
                    "wabo_id"=>"",
                    "wabo_declar"=>"",
                    "wabo_dossier"=>"",
                    "wabo_contrat"=>"",
                    "wabo_prestataire"=>"",
                    "wabo_prestataire_label"=>"",
                    "wabo_publications"=>[
                        ['wabopTitre'=>'','wabopNombre'=>'','wabopAuteur'=>'','wabopEditeur'=>'','wabopType'=>'titre'],
                        ['wabopTitre'=>'','wabopNombre'=>'','wabopAuteur'=>'','wabopEditeur'=>'','wabopType'=>'site'],
                        ['wabopTitre'=>'','wabopNombre'=>'','wabopAuteur'=>'','wabopEditeur'=>'','wabopType'=>'livre'],

                    ],
                    "wabo_fichier"=>"",
                    'wabo_fichier_media_key'=>"",
                    'wabo_fichier_media_reference'=>"",
                    'wabo_fichier_name'=>"",

                    "wabo_type"=>"" ,

                ];
            }


            $this->ObjVal['wabo'.$wde_declar]=!empty($wabo_array)?$wabo_array:false;

        }
        if(empty($this->ObjVal['wpresta'.$wde_declar])){
            $wpresta = $this->registry->getRepository('App\Entity\Wprestataire')->createQueryBuilder('Wprestataire');
            $wpresta->where('Wprestataire.wprestaDeclar  = '.$wdf_declar);
            $wpresta_results = $wpresta->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

            $RepertoirePrestataire = $this->registry->getRepository('App\Entity\RepertoirePrestataire')->createQueryBuilder('Wprestataire');
            //$RepertoirePrestataire->where('Wprestataire.wprestaDeclar  = '.$wde_declar);
            $RepertoirePrestataire_results = $RepertoirePrestataire->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

            if(!empty($wpresta_results)){;

                $wpresta_array=[
                    "wpresta_id"=>$wpresta_results[0]['wprestaId'],
                    "wpresta_prestataire"=>$wpresta_results[0]['wprestaPrestataire'],
                    "wpresta_prestataire_label"=>$wpresta_results[0]['wprestaPrestataireLabel'],
                    "wpresta_prestataire_label2"=>$wpresta_results[0]['wprestaPrestataireLabel2'],
                    "repertoire_prestataire"=>$RepertoirePrestataire_results
                ];
            }else{
                $wpresta_array=[
                    "wpresta_id"=>"",
                    "wpresta_prestataire"=>"",
                    "wpresta_prestataire_label"=>"",
                    "wpresta_prestataire_label2"=>"",
                    "repertoire_prestataire"=>$RepertoirePrestataire_results

                ];
            }

            $this->ObjVal['wpresta'.$wde_declar]=!empty($wpresta_array)?$wpresta_array:false;
        }
        if(empty($this->ObjVal['wrp'.$wde_declar])){
            $wrp = $this->registry->getRepository('App\Entity\WrelationPublique')->createQueryBuilder('WrelationPublique');
            $wrp->where('WrelationPublique.wrpDeclar  = '.$wdf_declar);
            $wrp_results = $wrp->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

            if(!empty($wrp_results)){
                foreach($wrp_results as $w){
                    $wrp_array[]=[
                        "wrp_label"=>$w['wrpLabel'],
                        "wrp_adresse"=>$w['wrpAdresse'],
                        "wrp_cp"=>$w['wrpCp'],
                        "wrp_ville"=>$w['wrpVille'],
                        "wrp_volume"=>$w['wrpVolume'],
                        "wrp_telephone"=>$w['wrpTelephone'],
                        "wrp_email"=>$w['wrpEmail'],
                    ];
                }

            }else{
                $wrp_array[]=[
                    "wrp_label"=>"",
                    "wrp_adresse"=>"",
                    "wrp_cp"=>"",
                    "wrp_ville"=>"",
                    "wrp_volume"=>"",
                    "wrp_telephone"=>"",
                    "wrp_email"=>"",
                ];
            }

            $this->ObjVal['wrp'.$wde_declar]=!empty($wrp_array)?$wrp_array:false;
        }

        if(empty($this->ObjVal['wcpextw'.$wde_declar])){

            $wcpext = $this->registry->getRepository('App\Entity\Wcpext')->createQueryBuilder('wcpext');
            $wcpext->where('wcpext.wcpextDeclar  = '.$wdf_declar);
            $wcpext_results = $wcpext->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            if(!empty($wcpext_results)){
                foreach($wcpext_results as $i=>$r){
                    $wcpextmel = $this->registry->getRepository('App\Entity\WcpextMel')->createQueryBuilder('wcpextmel');
                    $wcpextmel->where('wcpextmel.wcpextId  = '.$r['wcpextId']);
                    $wcpextmel_results = $wcpextmel->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

                    if(!empty($wcpextmel_results)){
                        $wcpext_results[$i]['wcpextMel']=$wcpextmel_results;
                    }else{
                        $wcpext_results[$i]['wcpextMel']=[
                            0=>['wcpextmelMel'=>""]
                        ];
                    }
                }

                $this->ObjVal['wcpextw'.$wde_declar]=$wcpext_results;


            }else{
                $this->ObjVal['wcpextw'.$wde_declar][]=[
                    'wcpextPublication'=>"",
                    'wcpextDateMel'=>"",
                    'wcpextTitre'=>"",
                    'wcpextAuteur'=>"",
                    'wcpextParution'=>"",
                    'wcpextDestinataires'=>"",
                    'wcpextMel'=>[
                        0=>['wcpextmelMel'=>""]
                    ]

                ];
            }

        }

        if(empty($this->ObjVal['wcpextc'.$wde_declar])){

            $wcpext = $this->registry->getRepository('App\Entity\Wcpext')->createQueryBuilder('wcpext');
            $wcpext->where('wcpext.wcpextDeclar  = '.$wdf_declar);
            $wcpext_results = $wcpext->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            if(!empty($wcpext_results)){
                foreach($wcpext_results as $i=>$r){
                    $wcpextmel = $this->registry->getRepository('App\Entity\WcpextMel')->createQueryBuilder('wcpextmel');
                    $wcpextmel->where('wcpextmel.wcpextId  = '.$r['wcpextId']);
                    $wcpextmel_results = $wcpextmel->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
                    $wcpext_results[$i]['wcpextMel']=$wcpextmel_results;
                }

                $this->ObjVal['wcpextc'.$wde_declar]=$wcpext_results;
            }else{
                $this->ObjVal['wcpextc'.$wde_declar][]=[
                    'wcpextPublication'=>"",
                    'wcpextDateMel'=>"",
                    'wcpextTitre'=>"",
                    'wcpextAuteur'=>"",
                    'wcpextParution'=>"",
                    'wcpextDestinataires'=>"",
                    'wcpextMel'=>[]

                ];
            }

        }

        if(empty($this->ObjVal['wrgpd'.$wde_declar])){
            $wrgpd = $this->registry->getRepository('App\Entity\Wrgpd')->createQueryBuilder('Wrgpd');
            $wrgpd->where('Wrgpd.wrgpdDeclar  = '.$wdf_declar);
            $wrgpd_results = $wrgpd->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

            if(!empty($wrgpd_results)){
                $this->ObjVal['wrgpd'.$wde_declar]=$wrgpd_results[0]['wrgpdValue'];
            }else{
                $this->ObjVal['wrgpd'.$wde_declar]=0;
            }
        }

        if(!empty($this->ObjVal[$wde_declar]['wde_siret']))
        {
            $this->ObjVal[$wde_declar]['wde_service_list']=$this->registry->getRepository(Chorus::class)->findServicesBySiret( $this->ObjVal[$wde_declar]['wde_siret'] );
        }else{
            $this->ObjVal[$wde_declar]['wde_service_list']=[];
        }

        return [
            "wco"=>$this->ObjVal[$wco_dossier],
            /*"wde"=>$this->ObjVal[$wde_declar],*/
            "wct"=>$this->ObjVal['wct'.$wde_declar],
            "wdf"=>$this->ObjVal['wdf'.$wde_declar],
            "wan"=>$this->ObjVal['wan'.$wde_declar],
            "wcompos"=>$this->ObjVal['wcompos'.$wde_declar],
            'wpp'=>$this->ObjVal['wpp'.$wde_declar],
            'wabo'=>$this->ObjVal['wabo'.$wde_declar],
            'wprestataire'=>$this->ObjVal['wpresta'.$wde_declar],
            'wrp'=>$this->ObjVal['wrp'.$wde_declar],
            'wrgpd'=>$this->ObjVal['wrgpd'.$wde_declar],
            'wcpextw'=>$this->ObjVal['wcpextw'.$wde_declar],
            'wcpextc'=>$this->ObjVal['wcpextc'.$wde_declar]
        ];



    }


    public function updateForm($data, $conf)
    {

        $sql="set session sql_mode='NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION,IGNORE_SPACE' ";
        $stmt=$this->connection->prepare($sql);
        $stmt->execute();

        foreach($conf as $c)
        {
            $field = $c->getSynchroField();
            $object_conf=$c->getTypeObjetConf();

            $hidden_field=false;

            if(!empty($object_conf['contact_field_id'])){
                $hidden_field=$object_conf['contact_field_id'];
                $hidden_field_question=$object_conf['contact_field_id']."_question";
            }
            if(!empty($object_conf['adress_field_id'])){
                $hidden_field=$object_conf['adress_field_id'];
                $hidden_field_question=$object_conf['adress_field_id']."_question";
            }
            if(!empty($hidden_field)){
                $hidden_field_prefix = substr($hidden_field, 0, 3);
                if($hidden_field_prefix == 'wco')
                {
                    $sql="UPDATE wcocon set `".$hidden_field."` = :".$hidden_field.", `".$hidden_field_question."` = :".$hidden_field_question.", wco_stamp=now() WHERE wco_dossier='".$c->wde_dossier."' LIMIT 1";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute([
                        $hidden_field=>$data[$hidden_field],
                        $hidden_field_question=>$data[$hidden_field_question]

                    ]);
                }elseif($hidden_field_prefix=='wde'){

                    //fix pour mettre a jour les champs wde de l'étape de coordonnée dans toutes les lignes de la chaine wdeclar;
                    if($c->type_etape=='coordonnees' && !empty($this->ChainedDeclarationConfiguration)){
                        foreach($this->ChainedDeclarationConfiguration as $chain){
                            $sql="UPDATE wdeclar set `".$hidden_field."` = :".$hidden_field.", `".$hidden_field_question."` = :".$hidden_field_question.", wde_stamp=now() WHERE wde_declar='".$chain[0]->wde_declar."' LIMIT 1";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->execute([
                                $hidden_field=>$data[$hidden_field],
                                $hidden_field_question=>$data[$hidden_field_question]
                            ]);
                        }
                    }else{
                        $sql="UPDATE wdeclar set `".$hidden_field."` = :".$hidden_field.", `".$hidden_field_question."` = :".$hidden_field_question.", wde_stamp=now() WHERE wde_declar='".$c->wde_declar."' LIMIT 1";
                        $stmt=$this->connection->prepare($sql);
                        $stmt->execute([
                            $hidden_field=>$data[$hidden_field],
                            $hidden_field_question=>$data[$hidden_field_question]
                        ]);
                    }

                }elseif($hidden_field_prefix=='wct'){
                    // dd($c);
                    //fix pour mettre a jour les champs wde de l'étape de coordonnée dans toutes les lignes de la chaine wdeclar;
                    if($c->type_etape=='coordonnees' && !empty($this->ChainedDeclarationConfiguration)){
                        foreach($this->ChainedDeclarationConfiguration as $chain){
                            $sql="UPDATE wcontrat set `".$hidden_field."` = :".$hidden_field.", `".$hidden_field_question."` = :".$hidden_field_question.", wct_stamp=now() WHERE wct_contrat='".$chain[0]->wde_declar."' LIMIT 1";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->execute([
                                $hidden_field=>$data[$hidden_field],
                                $hidden_field_question=>$data[$hidden_field_question]
                            ]);
                        }
                    }else{
                        $sql="UPDATE wcontrat set `".$hidden_field."` = :".$hidden_field.", `".$hidden_field_question."` = :".$hidden_field_question.", wct_stamp=now() WHERE wct_contrat='".$c->wde_declar."' LIMIT 1";
                        $stmt=$this->connection->prepare($sql);
                        $stmt->execute([
                            $hidden_field=>$data[$hidden_field],
                            $hidden_field_question=>$data[$hidden_field_question]
                        ]);
                    }

                }elseif($hidden_field_prefix=='wdf'){
                    // dd($c);
                    //fix pour mettre a jour les champs wde de l'étape de coordonnée dans toutes les lignes de la chaine wdeclar;
                    if($c->type_etape=='coordonnees' && !empty($this->ChainedDeclarationConfiguration)){
                        foreach($this->ChainedDeclarationConfiguration as $chain){
                            $sql="UPDATE wdelef set `".$hidden_field."` = :".$hidden_field.", `".$hidden_field_question."` = :".$hidden_field_question.", wdf_stamp=now() WHERE wdf_declar='".$chain[0]->wdf_declar."' LIMIT 1";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->execute([
                                $hidden_field=>$data[$hidden_field],
                                $hidden_field_question=>$data[$hidden_field_question]
                            ]);
                        }
                    }else{
                        $sql="UPDATE wdelef set `".$hidden_field."` = :".$hidden_field.", `".$hidden_field_question."` = :".$hidden_field_question.", wdf_stamp=now() WHERE wdf_declar='".$c->wdf_declar."' LIMIT 1";
                        $stmt=$this->connection->prepare($sql);
                        $stmt->execute([
                            $hidden_field=>$data[$hidden_field],
                            $hidden_field_question=>$data[$hidden_field_question]
                        ]);
                    }

                }
            }



            if(in_array($c->getTypeObjet(), ['checkbox', 'civilite']))
            {
                $chk_conf=$c->getTypeObjetConf();
                if(isset($chk_conf['option_val_off'])){
                    $chk_conf_off=$chk_conf['option_val_off'];
                }else{
                    $chk_conf_off=0; //default value
                }
                $data[$field]=!empty($data[$field])?$data[$field]:"0";

            }





            if (!empty($c->getSynchroField()) || $c->getTypeObjet()=="chorus_pro" || $c->getTypeObjet()=="chorus_pro_extended" || $c->getTypeObjet()=="panorama_press_numerique" || $c->getTypeObjet()=="panorama_press_papier" || $c->getTypeObjet()=="accordion_options" || $c->getTypeObjet()=="bon_commande") {

                if($c->getTypeObjet()=="file_upload")
                {
                    if(!empty($_FILES['file_upload_file_input']['tmp_name'][$field])){

                        $name=$_FILES['file_upload_file_input']['name'][$field];

                        $temp_file= $this->kernel->getCacheDir()."/../../tmp_upload/$name";
                        if (!file_exists($this->kernel->getCacheDir()."/../../tmp_upload/")) {
                            mkdir($this->kernel->getCacheDir()."/../../tmp_upload/", 0777, true);
                        }

                        move_uploaded_file($_FILES['file_upload_file_input']['tmp_name'][$field], $temp_file);


                        $mediaManager = $this->container->get('sonata.media.manager.media');
                        $media = new Media();
                        $media->setBinaryContent($temp_file);
                        $media->setContext('file_upload');
                        $media->setAuthorName($c->wde_dossier);
                        $media->setCopyright($c->wde_contrat);
                        $media->setDescription("Déclaraiton : ".$c->wde_declar."\r\nDossier : ".$c->wde_dossier."\r\Contrat : ".$c->wde_contrat."\r\Date : ". date("Y-m-d H:i:s"));
                        $media->setProviderName('sonata.media.provider.file');
                        $ret=$mediaManager->save($media);
                        $media_id=$media->getId();
                        $data[$field]=$media_id;
                    }elseif(!empty($data['file_upload_fichier_media_key'][$field]))
                    {
                        $media_id=$this->checkMediaKey($data['file_upload_fichier_media_key'][$field]);
                        $data[$field]=$media_id;
                    }else{
                        $data[$field]=null;
                    }

                }



                $prefix = substr($field, 0, 3);
                if($prefix == 'wco' && $field != 'wcompos')
                {
                    $sql="UPDATE wcocon set `".$field."` = :".$field.", wco_stamp=now() WHERE wco_dossier='".$c->wde_dossier."' LIMIT 1";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute([$field=>$data[$field]]);
                }elseif($prefix=='wde' && $c->getTypeObjet()!="chorus_pro" && $c->getTypeObjet()!="chorus_pro_extended" && $c->getTypeObjet()!="bon_commande" ){
                    //fix pour mettre a jour les champs wde de l'étape de coordonnée dans toutes les lignes de la chaine wdeclar;
                    if($c->type_etape=='coordonnees' && !empty($this->ChainedDeclarationConfiguration)){
                        foreach($this->ChainedDeclarationConfiguration as $chain){
                            $sql="UPDATE wdeclar set `".$field."` = :".$field." , wde_stamp=now() WHERE wde_declar='".$chain[0]->wde_declar."' LIMIT 1";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->execute([$field=>$data[$field]]);
                        }
                    }else{
                        $sql="UPDATE wdeclar set `".$field."` = :".$field." , wde_stamp=now() WHERE wde_declar='".$c->wde_declar."' LIMIT 1";
                        $stmt=$this->connection->prepare($sql);
                        $stmt->execute([$field=>$data[$field]]);
                    }
                }elseif($prefix=='wct' && $c->getTypeObjet()!="chorus_pro" && $c->getTypeObjet()!="chorus_pro_extended" && $c->getTypeObjet()!="bon_commande" ){
                    //fix pour mettre a jour les champs wde de l'étape de coordonnée dans toutes les lignes de la chaine wdeclar;
                    if($c->type_etape=='coordonnees' && !empty($this->ChainedDeclarationConfiguration)){
                        foreach($this->ChainedDeclarationConfiguration as $chain){
                            $sql="UPDATE wcontrat set `".$field."` = :".$field." , wct_stamp=now() WHERE wct_contrat='".$chain[0]->wde_declar."' LIMIT 1";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->execute([$field=>$data[$field]]);
                        }
                    }else{
                        $sql="UPDATE wcontrat set `".$field."` = :".$field." , wct_stamp=now() WHERE wct_contrat='".$c->wde_declar."' LIMIT 1";
                        $stmt=$this->connection->prepare($sql);
                        $stmt->execute([$field=>$data[$field]]);
                    }
                }elseif($prefix=='wdf' && $c->getTypeObjet()!="chorus_pro" && $c->getTypeObjet()!="chorus_pro_extended" && $c->getTypeObjet()!="bon_commande" ){
                    //fix pour mettre a jour les champs wde de l'étape de coordonnée dans toutes les lignes de la chaine wdeclar;
                    if($c->type_etape=='coordonnees' && !empty($this->ChainedDeclarationConfiguration)){
                        foreach($this->ChainedDeclarationConfiguration as $chain){
                            $sql="UPDATE wdelef set `".$field."` = :".$field." , wdf_stamp=now() WHERE wdf_declar='".$chain[0]->wdf_declar."' LIMIT 1";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->execute([$field=>$data[$field]]);
                        }
                    }else{
                        $sql="UPDATE wdelef set `".$field."` = :".$field." , wdf_stamp=now() WHERE wdf_declar='".$c->wdf_declar."' LIMIT 1";
                        $stmt=$this->connection->prepare($sql);
                        $stmt->execute([$field=>$data[$field]]);
                    }
                }elseif($prefix=='wan'){
                    $wde = $this->registry->getRepository('App\Entity\Wannexe')->createQueryBuilder('w');
                    $wde->where('w.wan_declar  = '.$c->wdf_declar);
                    $wde_results = $wde->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
                    if(count($wde_results)){
                        $sql="UPDATE wannexe set `".$field."` = :".$field." WHERE wan_declar='".$c->wdf_declar."' LIMIT 1";
                    }else{
                        //$sql="INSERT INTO `wannexe` (`wan_declar`, `wan_numsite`,`wan_nom`, `wan_tit`, `wan_fct`, `".$field."`) VALUES ('".$c->wde_declar."', '0','', '', '', :".$field.");";
                        $sql="INSERT INTO `wannexe` (`wan_declar`,  `".$field."`) VALUES ('".$c->wdf_declar."', :".$field.");";
                    }

                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute([$field=>$data[$field]]);
                }elseif($field == 'wcompos'){
                    $sql="DELETE FROM wcompos where wcp_declar='".$c->wdf_declar."';";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute();

                    if($c->getTypeObjet()=="wcompos_stages"){
                        if(!empty($data['compos_stagiaires'])){
                            foreach($data['compos_stagiaires'] as $i=>$v){
                                $sql="insert into wcompos(wcp_declar,wcp_libelle,wcp_effectif, wcp_heures) values(:wcp_declar,:wcp_libelle,:wcp_effectif,:wcp_heures)";
                                $stmt=$this->connection->prepare($sql);
                                $stmt->execute(['wcp_declar'=>$c->wdf_declar,'wcp_libelle'=>$data['compos_title'][$i],'wcp_effectif'=>$v,'wcp_heures'=>$data['compos_heures'][$i]]);
                            }
                        }
                    }else{
                        if(!empty($data['compos_title'])){
                            foreach($data['compos_num'] as $i=>$v){
                                $sql="insert into wcompos(wcp_declar,wcp_libelle,wcp_effectif) values(:wcp_declar,:wcp_libelle,:wcp_effectif)";
                                $stmt=$this->connection->prepare($sql);
                                $stmt->execute(['wcp_declar'=>$c->wdf_declar,'wcp_libelle'=>$data['compos_title'][$i],'wcp_effectif'=>$v]);
                            }
                        }
                    }

                }elseif($c->getTypeObjet()=="chorus_pro"){
                    $sql="UPDATE wcontrat set   `wct_is_chorus_pro` = :wde_is_chorus_pro, `wct_siret` = :wde_siret, `wct_service_code` = :wde_service_code, `wct_is_order_number` = :wde_is_order_number, `wct_order_number` = :wde_order_number,  wct_stamp=now() WHERE wct_contrat='".$c->wde_declar."' LIMIT 1";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute(['wde_is_chorus_pro'=>$data['wde_is_chorus_pro'], 'wde_siret'=>$data['wde_siret'], 'wde_service_code'=>$data['wde_service_code'],'wde_is_order_number'=>$data['wde_is_order_number'], 'wde_order_number'=>$data['wde_order_number']]);
                }elseif($c->getTypeObjet()=="bon_commande"){

                    $sql="UPDATE wcontrat set   `wct_is_order_number` = :wde_is_order_number, `wct_order_number` = :wde_order_number,  wct_stamp=now() WHERE wct_contrat='".$c->wde_declar."' LIMIT 1";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute(['wde_is_order_number'=>$data['wde_is_order_number'],  'wde_order_number'=>$data['wde_order_number']]);
                }elseif($c->getTypeObjet()=="chorus_pro_extended"){

                    $wppl_is_chorus_pro = $data['chorus_pro_multi'][0]['wppl_is_chorus_pro'];
                    $wppl_siret         = $data['chorus_pro_multi'][0]['wppl_siret'];
                    $wppl_service_code  = $data['chorus_pro_multi'][0]['wppl_service_code'];
                    $wppl_has_order     = $data['chorus_pro_multi'][0]['wppl_has_order'];
                    $wppl_order_number  = $data['chorus_pro_multi'][0]['wppl_order_number'];


                    $sql="UPDATE wcontrat set   `wct_is_chorus_pro` = :wde_is_chorus_pro, `wct_siret` = :wde_siret, `wct_service_code` = :wde_service_code, `wct_is_order_number` = :wde_is_order_number,  `wct_order_number` = :wde_order_number,  wct_stamp=now() WHERE wct_contrat='".$c->wde_declar."' LIMIT 1";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute(['wde_is_chorus_pro'=>$wppl_is_chorus_pro, 'wde_siret'=>$wppl_siret, 'wde_service_code'=>$wppl_service_code, 'wde_is_order_number'=>$wppl_has_order, 'wde_order_number'=>$wppl_order_number]);


                }elseif($c->getTypeObjet()=="panorama_press_numerique" || $c->getTypeObjet()=="panorama_press_papier"){

                    $sql="DELETE FROM wpanoramapress where wpp_declar='".$c->wdf_declar."';";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute();

                    $sql="INSERT INTO `wpanoramapress` (`wpp_id`, `wpp_type`, `wpp_declar`, `wpp_dossier`, `wpp_contrat`) VALUES (NULL, '".str_replace('press','presse',$c->getTypeObjet())."', '".$c->wdf_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."');";
                    $stmt=$this->connection->prepare($sql);

                    /*$stmt->bindParam(':wpp_prestataire', $data['wpp_prestataire'], \PDO::PARAM_STR);
                    $stmt->bindParam(':wpp_prestataire_label', $data['wpp_prestataire_label'], \PDO::PARAM_STR);*/

                    $stmt->execute();
                    $id=$this->connection->lastInsertId();
                    if(!empty($data['wpanoramapress_liste'])){



                        foreach($data['wpanoramapress_liste'] as $i=>$p){
                            $wppl_fichier="NULL";
                            if(!empty($_FILES['wpanoramapress_liste']['tmp_name'][$i]['wppl_fichier'])){
                                $name=$_FILES['wpanoramapress_liste']['name'][$i]['wppl_fichier'];
                                $temp_file= $this->kernel->getCacheDir()."/../../tmp_upload/$name";
                                if (!file_exists($this->kernel->getCacheDir()."/../../tmp_upload/")) {
                                    mkdir($this->kernel->getCacheDir()."/../../tmp_upload/", 0777, true);
                                }

                                move_uploaded_file($_FILES['wpanoramapress_liste']['tmp_name'][$i]['wppl_fichier'], $temp_file);
                                $mediaManager = $this->container->get('sonata.media.manager.media');
                                $media = new Media();
                                $media->setBinaryContent($temp_file);
                                $media->setContext('panorama_press');
                                $media->setAuthorName($c->wde_dossier);
                                $media->setCopyright($c->wde_contrat);
                                $media->setDescription("Déclaraiton : ".$c->wde_declar."\r\nDossier : ".$c->wde_dossier."\r\Contrat : ".$c->wde_contrat."\r\Date : ". date("Y-m-d H:i:s"));
                                $media->setProviderName('sonata.media.provider.file');
                                $ret=$mediaManager->save($media);
                                $wppl_fichier=$media->getId();
                            }elseif(!empty($p['wppl_fichier_media_key']))
                            {
                                $wppl_fichier=$this->checkMediaKey($p['wppl_fichier_media_key']);
                            }

                            if(!empty($data['chorus_pro_multi'][$i]))
                            {
                                $wppl_is_chorus_pro = $data['chorus_pro_multi'][$i]['wppl_is_chorus_pro'];
                                $wppl_siret         = $data['chorus_pro_multi'][$i]['wppl_siret'];
                                $wppl_service_code  = $data['chorus_pro_multi'][$i]['wppl_service_code'];
                                $wppl_has_order     = $data['chorus_pro_multi'][$i]['wppl_has_order'];
                                $wppl_order_number  = $data['chorus_pro_multi'][$i]['wppl_order_number'];
                            }else{
                                $wppl_is_chorus_pro=null;
                                $wppl_siret=null;
                                $wppl_service_code=null;
                                $wppl_has_order=null;
                                $wppl_order_number=null;
                            }
                            $sql="INSERT INTO `wpanoramapress_liste` (`wppl_id`, `wppl_wpp_id`, `wppl_declar`, `wppl_dossier`, `wppl_contrat`, `wppl_label`, `wppl_periode`, `wppl_periode_nombre`,  `wppl_moyenne_repro`,`wppl_moyenne_exemplaire`, `wppl_total`, `wppl_fichier`, `wppl_is_chorus_pro`,`wppl_siret`,`wppl_service_code`, `wppl_has_order`,`wppl_order_number`)  VALUES 
                                (NULL, '".$id."', '".$c->wdf_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."', :wppl_label, :wppl_periode,:wppl_periode_nombre, :wppl_moyenne_repro, :wppl_moyenne_exemplaire, :wppl_total, $wppl_fichier,
                             :wppl_is_chorus_pro,
                                :wppl_siret,
                                :wppl_service_code,
                                :wppl_has_order,
                                :wppl_order_number
                            
                            );";
                            $stmt=$this->connection->prepare($sql);
                            $wppl_label=!empty($p['wppl_label'])?$p['wppl_label']:null;
                            $wppl_periode=!empty($p['wppl_periode'])?$p['wppl_periode']:null;
                            $wppl_total=!empty($p['wppl_total'])?$p['wppl_total']:null;
                            $wppl_periode_nombre=!empty($p['wppl_periode_nombre'])?$p['wppl_periode_nombre']:null;
                            $wppl_moyenne_repro=!empty($p['wppl_moyenne_repro'])?$p['wppl_moyenne_repro']:null;
                            $wppl_moyenne_exemplaire=!empty($p['wppl_moyenne_exemplaire'])?$p['wppl_moyenne_exemplaire']:null;



                            $stmt->bindParam(':wppl_label', $wppl_label, \PDO::PARAM_STR);
                            $stmt->bindParam(':wppl_periode', $wppl_periode, \PDO::PARAM_STR);
                            $stmt->bindParam(':wppl_total', $wppl_total, \PDO::PARAM_INT);

                            $stmt->bindParam(':wppl_periode_nombre', $wppl_periode_nombre, \PDO::PARAM_STR);
                            $stmt->bindParam(':wppl_moyenne_repro', $wppl_moyenne_repro, \PDO::PARAM_STR);
                            $stmt->bindParam(':wppl_moyenne_exemplaire', $wppl_moyenne_exemplaire, \PDO::PARAM_INT);

                            $stmt->bindParam(':wppl_is_chorus_pro', $wppl_is_chorus_pro, \PDO::PARAM_INT);
                            $stmt->bindParam(':wppl_siret', $wppl_siret, \PDO::PARAM_STR);
                            $stmt->bindParam(':wppl_service_code', $wppl_service_code, \PDO::PARAM_STR);
                            $stmt->bindParam(':wppl_has_order', $wppl_has_order, \PDO::PARAM_INT);
                            $stmt->bindParam(':wppl_order_number', $wppl_order_number, \PDO::PARAM_STR);

                            $stmt->execute();
                            $id_liste=$this->connection->lastInsertId();
                            if(!empty($p["publication"])){
                                foreach($p["publication"] as $publications){
                                    $sql="INSERT INTO `wpanoramapress_liste_publication` (`wpplp_id`, `wpplp_wppl_id`, `wpplp_label`, `wpplp_total`) VALUES (NULL, '".$id_liste."', :wpplp_label, :wpplp_total);";
                                    $stmt=$this->connection->prepare($sql);
                                    $stmt->bindParam(':wpplp_label', $publications['wpplp_label'], \PDO::PARAM_STR);
                                    $stmt->bindParam(':wpplp_total', $publications['wpplp_total'], \PDO::PARAM_INT);
                                    $stmt->execute();
                                }
                            }

                        }
                    }
                }elseif($c->getTypeObjet()=="abo_press_livres"){

                    $sql="DELETE FROM wabo where wabo_declar='".$c->wdf_declar."';";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute();
                    $wabo_fichier="NULL";
                    if(!empty($_FILES['wabo_fichier']['tmp_name'])){
                        $name=$_FILES['wabo_fichier']['name'];
                        $temp_file= $this->kernel->getCacheDir()."/../../tmp_upload/$name";
                        if (!file_exists($this->kernel->getCacheDir()."/../../tmp_upload/")) {
                            mkdir($this->kernel->getCacheDir()."/../../tmp_upload/", 0777, true);
                        }

                        move_uploaded_file($_FILES['wabo_fichier']['tmp_name'], $temp_file);
                        $mediaManager = $this->container->get('sonata.media.manager.media');
                        $media = new Media();
                        $media->setBinaryContent($temp_file);
                        $media->setContext('abo_press_livres');
                        $media->setAuthorName($c->wde_dossier);
                        $media->setCopyright($c->wde_contrat);
                        $media->setDescription("Déclaraiton : ".$c->wde_declar."\r\nDossier : ".$c->wde_dossier."\r\Contrat : ".$c->wde_contrat."\r\Date : ". date("Y-m-d H:i:s"));
                        $media->setProviderName('sonata.media.provider.file');
                        $ret=$mediaManager->save($media);
                        $wabo_fichier=$media->getId();
                    }elseif(!empty($data['wabo_fichier_media_key']))
                    {
                        $wabo_fichier=$this->checkMediaKey($data['wabo_fichier_media_key']);
                    }
                    $sql="INSERT INTO `wabo` (`wabo_id`, `wabo_declar`, `wabo_dossier`, `wabo_contrat`,  `wabo_fichier`, `wabo_type`) VALUES (NULL, '".$c->wdf_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."',  $wabo_fichier, '".$c->getTypeObjet()."');";
                    $stmt=$this->connection->prepare($sql);



                    $stmt->execute();
                    $id=$this->connection->lastInsertId();


                    if(!empty($data['wabop_titre'])){
                        foreach($data['wabop_titre'] as $n=>$titre){
                            $sql="INSERT INTO `wabo_publication` ( `wabop_id`, `wabop_wabo_id`, `wabop_titre`, `wabop_nombre`, `wabop_auteur`, `wabop_editeur`, `wabop_type`) VALUES ( NULL,'$id', :titre, :nombre, NULL, NULL, 'titre');";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->bindParam(':titre', $titre, \PDO::PARAM_STR);
                            $stmt->bindParam(':nombre', $data['wabop_titre_nombre'][$n], \PDO::PARAM_STR);
                            $stmt->execute();
                        }
                    }
                    if(!empty($data['wabop_site'])){
                        foreach($data['wabop_site'] as $n=>$site){
                            $sql="INSERT INTO `wabo_publication` ( `wabop_id`, `wabop_wabo_id`, `wabop_titre`, `wabop_nombre`, `wabop_auteur`, `wabop_editeur`, `wabop_type`) VALUES ( NULL,'$id', :site, :nombre, NULL, NULL, 'site');";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->bindParam(':site', $site, \PDO::PARAM_STR);
                            $stmt->bindParam(':nombre', $data['wabop_site_nombre'][$n], \PDO::PARAM_STR);
                            $stmt->execute();
                        }
                    }

                    if(!empty($data['wabop_livre'])){
                        foreach($data['wabop_livre'] as $i=>$site){
                            $sql="INSERT INTO `wabo_publication` ( `wabop_id`, `wabop_wabo_id`, `wabop_titre`, `wabop_nombre`, `wabop_auteur`, `wabop_editeur`, `wabop_type`) VALUES ( NULL,'$id', :titre, :nombre, :auteur, :editeur, 'livre');";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->bindParam(':titre', $site, \PDO::PARAM_STR);
                            $stmt->bindParam(':nombre', $data['wabop_livre_nombre'][$i], \PDO::PARAM_STR);
                            $stmt->bindParam(':auteur', $data['wabop_auteur'][$i], \PDO::PARAM_STR);
                            $stmt->bindParam(':editeur', $data['wabop_editeur'][$i], \PDO::PARAM_STR);
                            $stmt->execute();
                        }
                    }

                }elseif($c->getTypeObjet()=="abo_press"){

                    $sql="DELETE FROM wabo where wabo_declar='".$c->wdf_declar."';";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute();
                    $wabo_fichier="NULL";
                    if(!empty($_FILES['wabo_fichier']['tmp_name'])){
                        $name=$_FILES['wabo_fichier']['name'];
                        $temp_file= $this->kernel->getCacheDir()."/../../tmp_upload/$name";
                        if (!file_exists($this->kernel->getCacheDir()."/../../tmp_upload/")) {
                            mkdir($this->kernel->getCacheDir()."/../../tmp_upload/", 0777, true);
                        }

                        move_uploaded_file($_FILES['wabo_fichier']['tmp_name'], $temp_file);
                        $mediaManager = $this->container->get('sonata.media.manager.media');
                        $media = new Media();
                        $media->setBinaryContent($temp_file);
                        $media->setContext('abo_press_livres');
                        $media->setAuthorName($c->wde_dossier);
                        $media->setCopyright($c->wde_contrat);
                        $media->setDescription("Déclaraiton : ".$c->wde_declar."\r\nDossier : ".$c->wde_dossier."\r\Contrat : ".$c->wde_contrat."\r\Date : ". date("Y-m-d H:i:s"));
                        $media->setProviderName('sonata.media.provider.file');
                        $ret=$mediaManager->save($media);
                        $wabo_fichier=$media->getId();
                    }elseif(!empty($data['wabo_fichier_media_key']))
                    {
                        $wabo_fichier=$this->checkMediaKey($data['wabo_fichier_media_key']);
                    }
                    $sql="INSERT INTO `wabo` (`wabo_id`, `wabo_declar`, `wabo_dossier`, `wabo_contrat`, `wabo_prestataire`, `wabo_prestataire_label`, `wabo_fichier`, `wabo_type`) VALUES (NULL, '".$c->wdf_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."', :wabo_prestataire , :wabo_prestataire_label, $wabo_fichier, '".$c->getTypeObjet()."');";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->bindParam(':wabo_prestataire', $data['wabo_prestataire'], \PDO::PARAM_STR);
                    $stmt->bindParam(':wabo_prestataire_label', $data['wabo_prestataire_label'], \PDO::PARAM_STR);


                    $stmt->execute();
                    $id=$this->connection->lastInsertId();

                    if(!empty($data['wabop_titre'])){
                        foreach($data['wabop_titre'] as $n=>$titre){
                            $sql="INSERT INTO `wabo_publication` ( `wabop_id`, `wabop_wabo_id`, `wabop_titre`, `wabop_nombre`, `wabop_auteur`, `wabop_editeur`, `wabop_type`) VALUES ( NULL,'$id', :titre, :nombre, NULL, NULL, 'titre');";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->bindParam(':titre', $titre, \PDO::PARAM_STR);
                            $stmt->bindParam(':nombre', $data['wabop_titre_nombre'][$n], \PDO::PARAM_STR);
                            $stmt->execute();
                        }
                    }
                    if(!empty($data['wabop_site'])){
                        foreach($data['wabop_site'] as $n=>$site){
                            $sql="INSERT INTO `wabo_publication` ( `wabop_id`, `wabop_wabo_id`, `wabop_titre`, `wabop_nombre`, `wabop_auteur`, `wabop_editeur`, `wabop_type`) VALUES ( NULL,'$id', :site, :nombre,  NULL, NULL, 'site');";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->bindParam(':site', $site, \PDO::PARAM_STR);
                            $stmt->bindParam(':nombre', $data['wabop_site_nombre'][$n], \PDO::PARAM_STR);
                            $stmt->execute();
                        }
                    }

                }elseif($c->getTypeObjet()=="prestataire_veille_media"){
                    $sql="DELETE FROM wprestataire where wpresta_declar='".$c->wdf_declar."';";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute();

                    $sql="INSERT INTO `wprestataire` ( `wpresta_id`, `wpresta_declar`, `wpresta_dossier`, `wpresta_contrat`, `wpresta_prestataire`, `wpresta_prestataire_label`, `wpresta_prestataire_label2`) VALUES ( NULL,'".$c->wdf_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."', :wpresta_prestataire, :wpresta_prestataire_label, :wpresta_prestataire_label2 );";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->bindParam(':wpresta_prestataire', $data['wpresta_prestataire'], \PDO::PARAM_INT);
                    $stmt->bindParam(':wpresta_prestataire_label', $data['wpresta_prestataire_label'], \PDO::PARAM_STR);
                    $stmt->bindParam(':wpresta_prestataire_label2', $data['wpresta_prestataire_label2'], \PDO::PARAM_STR);
                    $stmt->execute();

                }elseif($c->getTypeObjet()=="accordion_options"){
                    $field_conf=$c->getTypeObjetConf();
                    $field_index[]=$field_conf['accordion_synchro_field_1'];
                    $field_index[]=$field_conf['accordion_synchro_field_2'];

                    foreach($field_index as $f){
                        $prefix = substr($f, 0, 3);
                        if($prefix == 'wco' && $f != 'wcompos')
                        {
                            $sql="UPDATE wcocon set `".$f."` = :".$f.", wco_stamp=now() WHERE wco_dossier='".$c->wde_dossier."' LIMIT 1";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->execute([$f=>!empty($data[$f])?$data[$f]:null]);
                        }elseif($prefix=='wde' && $c->getTypeObjet()!="chorus_pro"){
                            $sql="UPDATE wdeclar set `".$f."` = :".$f." , wde_stamp=now() WHERE wde_declar='".$c->wde_declar."' LIMIT 1";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->execute([$f=>!empty($data[$f])?$data[$f]:null]);
                        }elseif($prefix=='wct' && $c->getTypeObjet()!="chorus_pro"){
                            $sql="UPDATE wcontrat set `".$f."` = :".$f." , wct_stamp=now() WHERE wct_contrat='".$c->wde_declar."' LIMIT 1";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->execute([$f=>!empty($data[$f])?$data[$f]:null]);
                        }elseif($prefix=='wdf' && $c->getTypeObjet()!="chorus_pro"){
                            $sql="UPDATE wdelef set `".$f."` = :".$f." , wdf_stamp=now() WHERE wdf_declar='".$c->wdf_declar."' LIMIT 1";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->execute([$f=>!empty($data[$f])?$data[$f]:null]);
                        }elseif($prefix=='wan'){
                            $wde = $this->registry->getRepository('App\Entity\Wannexe')->createQueryBuilder('w');
                            $wde->where('w.wan_declar  = '.$c->wdf_declar);
                            $wde_results = $wde->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
                            if(count($wde_results)){
                                $sql="UPDATE wannexe set `".$f."` = :".$f." WHERE wan_declar='".$c->wdf_declar."' LIMIT 1";
                            }else{
                                $sql="INSERT INTO `wannexe` (`wan_declar`, `wan_numsite`,`wan_nom`, `wan_tit`, `wan_fct`, `".$f."`) VALUES ('".$c->wdf_declar."', '0','', '', '', :".$field.");";
                            }

                            $stmt=$this->connection->prepare($sql);
                            $stmt->execute([$f=>!empty($data[$f])?$data[$f]:null]);
                        }
                    }

                }elseif($c->getTypeObjet()=="relation_publique"){
                    $sql="DELETE FROM wrelation_publique where wrp_declar='".$c->wdf_declar."';";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute();
                    if(!empty($data['wrp_label'])){
                        foreach($data['wrp_label'] as $i=>$rp){
                            $sql="INSERT INTO `wrelation_publique` (`wrp_id`, `wrp_stamp`, `wrp_declar`, `wrp_dossier`, `wrp_contrat`, `wrp_label`, `wrp_adresse`, `wrp_cp`, `wrp_ville`, `wrp_volume`) VALUES (NULL, now(), '".$c->wdf_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."', :wrp_label, :wrp_adresse, :wrp_cp, :wrp_ville, :wrp_volume);";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->bindParam(':wrp_label',   $data['wrp_label'][$i],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wrp_adresse', $data['wrp_adresse'][$i],  \PDO::PARAM_STR);
                            $stmt->bindParam(':wrp_cp',      $data['wrp_cp'][$i],       \PDO::PARAM_STR);
                            $stmt->bindParam(':wrp_ville',   $data['wrp_ville'][$i],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wrp_volume',  $data['wrp_volume'][$i],   \PDO::PARAM_INT);
                            $stmt->execute();

                        }
                    }

                }elseif($c->getTypeObjet()=="cma_btp"){
                    $sql="DELETE FROM wrelation_publique where wrp_declar='".$c->wdf_declar."';";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute();
                    if(!empty($data['wrp_label'])){
                        foreach($data['wrp_label'] as $i=>$rp){
                            $sql="INSERT INTO `wrelation_publique` (`wrp_id`, `wrp_stamp`, `wrp_declar`, `wrp_dossier`, `wrp_contrat`, `wrp_label`, `wrp_adresse`, `wrp_telephone`, `wrp_email`) VALUES (NULL, now(), '".$c->wdf_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."', :wrp_label, :wrp_adresse, :wrp_telephone, :wrp_email);";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->bindParam(':wrp_label',   $data['wrp_label'][$i],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wrp_adresse', $data['wrp_adresse'][$i],  \PDO::PARAM_STR);
                            $stmt->bindParam(':wrp_telephone',      $data['wrp_telephone'][$i],       \PDO::PARAM_STR);
                            $stmt->bindParam(':wrp_email',   $data['wrp_email'][$i],    \PDO::PARAM_STR);
                            $stmt->execute();

                        }
                    }
                }elseif($c->getTypeObjet()=="copie_ext_web_reseau"){

                    $sql="DELETE FROM wcpext  where wcpext_declar='".$c->wdf_declar."';";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute();

                    $sql="DELETE FROM wcpext_mel  where wcpextmel_declar ='".$c->wdf_declar."';";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute();

                    if(!empty($data['wcpextw'])){
                        foreach($data['wcpextw'] as $i=>$d){
                            $sql="INSERT INTO `wcpext` (`wcpext_id`, `wcpext_stamp`, `wcpext_declar`, `wcpext_dossier`, `wcpext_contrat`, `wcpext_publication`, `wcpext_titre`, `wcpext_auteur`, `wcpext_parution`, `wcpext_date_mel`) VALUES (NULL, CURRENT_TIMESTAMP, '".$c->wdf_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."', :wcpext_publication, :wcpext_titre, :wcpext_auteur, :wcpext_parution, :wcpext_date_mel);";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->bindParam(':wcpext_publication',   $d['publication'],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wcpext_titre',   $d['titre'],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wcpext_auteur',   $d['auteur'],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wcpext_parution',   $d['parution'],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wcpext_date_mel',   $d['date_mel'],    \PDO::PARAM_STR);
                            $stmt->execute();
                            $wcpextw_id=$this->connection->lastInsertId();

                            if(!empty($data['wcpextw'][0]['wcpextMel'])){
                                foreach($data['wcpextw'][0]['wcpextMel'] as $j=>$p){
                                    $sql="INSERT INTO `wcpext_mel` (`wcpextmel_id`, `wcpext_id`, `wcpext_stamp`, `wcpextmel_declar`, `wcpextmel_dossier`, `wcpextmel_contrat`, `wcpextmel_mel`) VALUES (NULL, '".$wcpextw_id."', CURRENT_TIMESTAMP, '".$c->wdf_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."', :wcpextmel_mel);";
                                    $stmt=$this->connection->prepare($sql);
                                    $stmt->bindParam(':wcpextmel_mel',   $p,    \PDO::PARAM_STR);
                                    $stmt->execute();
                                }
                            }

                        }
                    }
                }elseif($c->getTypeObjet()=="copie_ext_cible"){
                    $sql="DELETE FROM wcpext  where wcpext_declar='".$c->wdf_declar."';";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute();

                    if(!empty($data['wcpextc'])){
                        foreach($data['wcpextc'] as $i=>$d){
                            $sql="INSERT INTO `wcpext` (`wcpext_id`, `wcpext_stamp`, `wcpext_declar`, `wcpext_dossier`, `wcpext_contrat`, `wcpext_publication`, `wcpext_titre`, `wcpext_auteur`, `wcpext_parution`, `wcpext_destinataires`) VALUES (NULL, CURRENT_TIMESTAMP, '".$c->wdf_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."', :wcpext_publication, :wcpext_titre, :wcpext_auteur, :wcpext_parution, :wcpext_destinataires);";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->bindParam(':wcpext_publication',   $d['publication'],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wcpext_titre',   $d['titre'],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wcpext_auteur',   $d['auteur'],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wcpext_parution',   $d['parution'],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wcpext_destinataires',   $d['destinataires'],    \PDO::PARAM_STR);
                            $stmt->execute();
                        }
                    }


                }elseif($field=="wrgpd"){

                    $sql="DELETE FROM wrgpd where wrgpd_declar='".$c->wdf_declar."';";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute();

                    $sql="INSERT INTO `wrgpd` (`wrgpd_id`, `wrgpd_stamp`, `wrgpd_declar`, `wrgpd_dossier`, `wrgpd_contrat`, `wrgpd_value`) VALUES ('', CURRENT_TIMESTAMP, '".$c->wdf_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."', :rgpd);";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->bindParam(':rgpd',   $data['wrgpd'],    \PDO::PARAM_INT);
                    $stmt->execute();
                }else{
                    dump($c->getTypeObjet());
                    if(!empty($item)){
                        dump($item);
                        dump($values);
                    }


                }
            }

        }

        //dd('fin');
    }
    
    /*
     * Return a an array containing the wde_contrat ids from the declaration conf array
     * */
    public function getListContrats($declaration_conf)
    {
       
        $contrats=[];
        foreach($declaration_conf as $i=>$c)
        {
            if($i>0){
                $contrats[]=$c[0]->wde_contrat;
            }
        }
        if(empty($contrats)){
            $contrats[]=$declaration_conf[0][0]->wde_contrat;
        }

        return array_unique($contrats);
    }
    
    
    public function getListContratsChained($declaration_conf)
    {

        $contrats['chaine']=[];
        $contrats['independant']=[];
        foreach($declaration_conf as $i=>$c)
        {   
                if($c[0]->type_interface==0){
                    //$contrats['chaine'][]=$c[0]->wtype;
                    $contrats['chaine'][]=$c[0];
                }else{
                    //$contrats['independant'][]=$c[0];
                    $contrats['independant'][]=$c[0];
                }
                
        }

        //on verifie les contrats indépendants. Si il y en a 2 du meme type on les chaines.
            $dec_type=[];
            foreach( $contrats['independant'] as $i=>$c){
                if($c->type_etape=="declaration"){
                    $dec_type[$c->wtype->__toString()][]=$i;
                }
                
            }
            foreach($dec_type as $d){
                if($d>1){
                    foreach($d as $c){
                        $contrats['chaine'][]=$contrats['independant'][$c];
                        unset($contrats['independant'][$c]);
                    }

                    
                }
            }
            //peut etre on a migré des contrats en chainé. Dans ce cas, si on a qu'un contrat et que c'est des coordonnées on l'éfface
            if(count($contrats['independant'])==1 && $contrats['independant'][0]->type_etape=="coordonnees")
            {
                unset($contrats['independant'][0]);
            }
        
        $contrats['chaine']=$contrats['chaine'];
        $contrats['independant']=$contrats['independant'];
        return $contrats;
    }
    
    
    
    function getContract($wco_dossier){

            $qb=$this->registry->getRepository('App\Entity\Wcontrat')->createQueryBuilder('d');
            $qb->leftJoin(
                'App\Entity\Wtype',
                't',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'd.wct_typcont = t.label'
                )
                ->where('d.wctDossier = :wct_dossier')
                ->orderBy('t.priorite', 'ASC')
                ->orderBy('t.id', 'ASC')
                //->AndWhere('t.priorite = :priorite')
                ->setParameter('wct_dossier', $wco_dossier)
                ->setMaxResults(1);
                //->setParameter('priorite', 0);
                list($wdeclar)=$qb->getQuery()->getResult();
        
                /** @var Wtype */
                $wtype = $this->registry
                ->getRepository('App\Entity\Wtype')
                ->findOneBy(['label' => $wdeclar->getWctTypcont()])
                ;

            return array('wdeclar'=>$wdeclar, 'wtype'=>$wtype);
    }
    
    function checkUrl($wdeclar){
        dd('checkUrl');
        $status=$wdeclar->getWdeEtatDeclar();
        
        $route=$this->requestStack->getCurrentRequest()->get('_route');

        if(in_array($route, ['app_pdf_declaration','declaration_admin_media_download','declaration_admin_composante_presse_download', 'declaration_admin_relation_publique_download', 'app_excell_declaration', 'declaration_admin_abo_presse_livre_download', 'declaration_admin_abo_presse_download', 'declaration_admin_composantes_download', 'declaration_admin_composantes_stages_download']))
        {
            return false;
        }

        $declaration_conf = $this->getDeclarationConfiguration($wdeclar);
        $display_open_interface=false;
        if(!empty($declaration_conf)){
            foreach($declaration_conf as $c){
                if( $c[0]->type_etape=='declaration' &&  in_array($c[0]->wde_etat_declar, ['N','V','A','P','W'])){
                    $display_open_interface=true;
                }
            }
            
            
        }

        if($display_open_interface){
            if(!in_array($route,['dashboard', 'interface_open','declaration','interface_contact','security_logout',
                'api-chorus-pro-siret','api-panorama-press-publications', 'api-panorama-press-publications-site', 'api-panorama-press-publications-ipro' , 'api-repertoire-cne-web' , 'api-repertoire-cne-presse-ciblees',
                'validation-success','validations-success', 'app_pdf_declaration','declaration_media_download', 'interface_history', 'interface_history_download_all', 'interface_history_download', 'interface_history_download_last', 'api-contact-remove', 'declaration-independant', 
                'interface_bdc', 'interface_bdc_success'])){
                return 'dashboard';
            }
        }else{
            if(!in_array($route,['interface_closed','interface_closed_coordonnees', 'interface_closed_coordonnees_success', 'interface_contact','security_logout','api-chorus-pro-siret','app_pdf_declaration','declaration_media_download', 'interface_history', 'interface_history_download_all', 'interface_history_download', 'interface_history_download_last', 'api-contact-remove'])){
                return 'interface_closed';
            }
        }
        
        /*sm($declaration_conf);
        die();
        switch($status){
            case 'V':
            case 'N':
            case 'A':
                if(!in_array($route,['interface_open','declaration','interface_contact','security_logout','api-chorus-pro-siret','api-panorama-press-publications', 'api-panorama-press-publications-site', 'api-panorama-press-publications-ipro' ,'validation-success','validations-success', 'app_pdf_declaration','declaration_media_download', 'interface_history', 'interface_history_download_all', 'interface_history_download', 'interface_history_download_last', 'api-contact-remove', 'declaration-independant'])){
                    return 'interface_open';
                }
                break;
            case 'P':
                if(!in_array($route,["interface_bdc",'security_logout'])){
                    return 'interface_bdc';
                }
                break;
            case 'W':
                    if(!in_array($route,["interface_bdc_success",'security_logout'])){
                    return 'interface_bdc_success';
                    }
                break;
            case 'F':
            case 'X':
            case 'I':
                if(!in_array($route,['interface_closed','interface_closed_coordonnees', 'interface_closed_coordonnees_success', 'interface_contact','security_logout','api-chorus-pro-siret','app_pdf_declaration','declaration_media_download', 'interface_history', 'interface_history_download_all', 'interface_history_download', 'interface_history_download_last', 'api-contact-remove'])){
                    return 'interface_closed';
                }
                break;
                
                
                
            default: return 'index';
        };
        */
        return false;
 
    }

   public function checkMediaKey($key){
       list($k,$id)=explode('%',$key);
       if(md5($id.$this->download_key)==$k){
           return $id;
       }else{
           return false;
       }
   }
   
}
