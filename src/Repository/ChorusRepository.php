<?php

namespace App\Repository;

use App\Entity\Chorus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Chorus|null find($id, $lockMode = null, $lockVersion = null)
 * @method Chorus|null findOneBy(array $criteria, array $orderBy = null)
 * @method Chorus[]    findAll()
 * @method Chorus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChorusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Chorus::class);
    }

    
    public function findBySiret($value)
    {
        $query=$this->createQueryBuilder('c')
        ->andWhere('c.identifiant like :val')
        ->setParameter('val', $value.'%')
        ->orderBy('c.identifiant', 'ASC')
        ->setMaxResults(10000)
        ->groupBy('c.identifiant')
        ->getQuery();
        $res=$query->getResult();
        
        return $res;
    }

    public function findServicesBySiret($value)
    {
        $query=$this->createQueryBuilder('c')->select(
            "c.service_code", 
            "c.service_nom", 
            "c.service_gestion_EGMT as service_gestion_Egmt",
            "c.gestion_engagement",
            "c.gestion_service",
            "c.gestion_service_engagement"
            
            )
        ->where("c.service_code != 'FACTURES_PUBLIQUES'")
        ->andWhere('c.identifiant =  :val')
        ->setParameter('val', $value)
        ->orderBy('c.service_code', 'ASC')
        
        ->setMaxResults(10000)
        ->groupBy('c.service_code')
        ->getQuery();
        $res=$query->getArrayResult();
        return $res;
    }
    
    // /**
    //  * @return Chorus[] Returns an array of Chorus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Chorus
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
