<?php

namespace App\Repository;

use App\Entity\Wcompos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Wcompos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Wcompos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Wcompos[]    findAll()
 * @method Wcompos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WcomposRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Wcompos::class);
    }

    // /**
    //  * @return Wcompos[] Returns an array of Wcompos objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Wcompos
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
