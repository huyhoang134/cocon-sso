<?php

namespace App\Repository;

use App\Entity\Wtype;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Wtype|null find($id, $lockMode = null, $lockVersion = null)
 * @method Wtype|null findOneBy(array $criteria, array $orderBy = null)
 * @method Wtype[]    findAll()
 * @method Wtype[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WtypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Wtype::class);
    }

    // /**
    //  * @return Wtype[] Returns an array of Wtype objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Wtype
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
