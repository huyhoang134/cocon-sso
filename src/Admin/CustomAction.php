<?php

namespace App\Admin;

use App\Entity\Wannexe;
use App\Entity\Wdeclar;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\Query\ResultSetMapping;
use Sonata\AdminBundle\Controller\CRUDController;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PropertyAccess\PropertyAccess;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Psr\Log\LoggerInterface;

class CustomAction extends CRUDController
{
    /**
     * @param $id
     */
    public function cloneAction($id)
    {
        $object = $this->admin->getObject($id);
        $objectConf=$object->getWtypeWconf()->toArray();
    
    
        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id: %s', $id));
        }
    
        $clonedObject = clone $object;
        $clonedObject->unsetId();
        $clonedObject->setLabel($object->getLabel().' clone('.uniqid().')');
        $this->admin->create($clonedObject);
        if(!empty($objectConf)){
            foreach($objectConf as $c){
                $clonedConf=clone $c;
                $clonedConf->setWconfTypcont($clonedObject);
                $this->admin->create($clonedConf);
            }
        }
        $this->addFlash('sonata_flash_success', 'L\'élément a correctement été dupliqué.');
        return new RedirectResponse($this->admin->generateUrl('list'));
    
        // if you have a filtered list and want to keep your filters after the redirect
        // return new RedirectResponse($this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()]));
    }
    
    /**
     * @param $id
     */
    public function cloneWconfAction($id)
    {
        $request=$this->admin->getRequest();
        $id=$request->attributes->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        //$request=$this->admin->getRequest();
        
        
        $clonedObject = clone $object;
        $clonedObject->unsetId();
        $clonedObject->setPosition(9999);
        $this->admin->create($clonedObject);

        $this->addFlash('sonata_flash_success', 'L\'élément '.$object->getLabel().' a correctement été dupliqué.');
        return new RedirectResponse($this->admin->generateUrl('list'));
    }
    
    //Action that make nothing to avoid buggy calculation of position from pix 
    //Edit : removed pix position. Replaced by custom js in public/js/admin.js:pixSortableBehaviorBundle.complete
    public function moveAction(){
        return new RedirectResponse($this->admin->generateUrl('list'));
    }
    
    
    public function importAction()
    {
       
        $this->admin->checkAccess('edit');
    
        /** @var FormInterface $form */
        $form = $this->createFormBuilder([])
            ->add('import-fp-men', FileType::class, ['required' => false, 'label' => 'FP/MEN (GRETA)'])
            ->add('import-press-clible', FileType::class, ['required' => false, 'label' => 'COPIES NUMERIQUES EXTERNES CIBLEES'])
            ->add('import-press-web-sites', FileType::class, ['required' => false,  'label' => 'COPIES NUMERIQUES WEB & RESEAUX SOCIAUX'])
            ->add('import', SubmitType::class, ['label' => 'Import'])
            ->getForm();

        $form->handleRequest($this->getRequest());
        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();


            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile */
            $file_fp_men = $task['import-fp-men'];
            $file_press_cible = $task['import-press-clible'];
            $file_press_web_site = $task['import-press-web-sites'];

            if(!empty($file_press_cible)){
                $message['updated'] = 0;
                $message['created']="";
                $spreadsheet = IOFactory::load($file_press_cible->getRealPath());
                $data_sheet = $spreadsheet->getSheet(0);
                if (empty($data_sheet)) {
                    throw $this->createNotFoundException('The sheet "CNE-CIBLEES" not found! Import aborted.');
                }

                $this->connection=$this->admin->createQuery()->getQueryBuilder()->getQuery()->getEntityManager()->getConnection();
                
                $stmt=$this->connection->prepare("TRUNCATE `repertoire_copies_num_ext_ciblees` ");
                $stmt->execute();
                
                $raw_data = $data_sheet->toArray(null, true, true, true);
                $propertyAccessor = PropertyAccess::createPropertyAccessor();
                $start=0;
                foreach ($raw_data as $row_id => $row) {
                    if($start==0 && $row['A']=="Editeurs"){
                        $start=1;
                    }elseif($start==1 && !empty($row['A'])) {
                        $sql="INSERT INTO `repertoire_copies_num_ext_ciblees` (`id`, `editeur`, `publications`, `redevance`, `inclusion`, `exclusion`) VALUES
                    (NULL, :editeur, :publications, :redevance, :inclusion, :exclusion); ";
                        $stmt=$this->connection->prepare($sql);
                        $stmt->bindParam(':editeur',   $row['A'],    \PDO::PARAM_STR);
                        $stmt->bindParam(':publications', $row['B'],  \PDO::PARAM_STR);
                        $stmt->bindParam(':redevance',      $row['C'],       \PDO::PARAM_STR);
                        $stmt->bindParam(':inclusion',      $row['D'],       \PDO::PARAM_STR);
                        $stmt->bindParam(':exclusion',      $row['E'],       \PDO::PARAM_STR);
                        $stmt->execute();
                        $message['updated'] = $message['updated'] + 1;
                    }
                   
                    
                }

                $this->addFlash(
                    'sonata_flash_success',
                    sprintf('Fichier "Répertoire numérique professionnel "COPIES NUMERIQUES EXTERNES CIBLEES France" correctement importé',
                        $message['updated'],
                        $message['created']
                        )
                    );
                
                
            }
            
            if(!empty($file_press_web_site)){
                $message['updated']=0;
                $message['created']="";
                $spreadsheet = IOFactory::load($file_press_web_site->getRealPath());
                $data_sheet = $spreadsheet->getSheet(0);
                if (empty($data_sheet)) {
                    throw $this->createNotFoundException('The sheet "CNE-WEB" not found! Import aborted.');
                }
                
                $this->connection=$this->admin->createQuery()->getQueryBuilder()->getQuery()->getEntityManager()->getConnection();
                
                $stmt=$this->connection->prepare("TRUNCATE `repertoire_copies_num_web_reseaux` ");
                $stmt->execute();
                
                $raw_data = $data_sheet->toArray(null, true, true, true);
                $propertyAccessor = PropertyAccess::createPropertyAccessor();
                $start=0;
                foreach ($raw_data as $row_id => $row) {
                    if($start==0 && $row['A']=="Editeurs"){
                        $start=1;
                    }elseif($start==1 && !empty($row['A'])) {
                        $sql="INSERT INTO `repertoire_copies_num_web_reseaux` (`id`, `editeur`, `publications`, `redevance`, `inclusion`, `exclusion`) VALUES
                    (NULL, :editeur, :publications, :redevance, :inclusion, :exclusion); ";
                        $stmt=$this->connection->prepare($sql);
                        $stmt->bindParam(':editeur',   $row['A'],    \PDO::PARAM_STR);
                        $stmt->bindParam(':publications', $row['B'],  \PDO::PARAM_STR);
                        $stmt->bindParam(':redevance',      $row['C'],       \PDO::PARAM_STR);
                        $stmt->bindParam(':inclusion',      $row['D'],       \PDO::PARAM_STR);
                        $stmt->bindParam(':exclusion',      $row['E'],       \PDO::PARAM_STR);
                        $stmt->execute();
                        $message['updated'] = $message['updated'] + 1;
                    }
                     
                
                }
                
                $this->addFlash(
                    'sonata_flash_success',
                    sprintf('Fichier "Répertoire numérique professionnel "COPIES NUMERIQUES WEB & RESEAUX SOCIAUX" correctement importé',
                        $message['updated'],
                        $message['created']
                        )
                    );
            }
            
            
            
            if(!empty($file_fp_men)){
                $spreadsheet = IOFactory::load($file_fp_men->getRealPath());
                $data_sheet = $spreadsheet->getSheetByName('Ets support');
                if (empty($data_sheet)) {
                    throw $this->createNotFoundException('The sheet "Ets support" not found! Import aborted.');
                }
                $raw_data = $data_sheet->toArray(null, true, true, true);
                $propertyAccessor = PropertyAccess::createPropertyAccessor();
                
                $message = [
                    'created'  => 0,
                    'updated'  => 0,
                    'warnings' => []
                ];
                $column_map = [
                    'B' => 'wan_raisoc1',
                    'C' => 'wan_raisoc2',
                    'D' => 'wan_raisoc3',
                    'E' => 'wan_adresse1',
                    'F' => 'wan_adresse2',
                    'G' => 'wan_adresse3',
                    'H' => 'wan_copos',
                    'I' => 'wan_ville',
                    'J' => 'wan_tel',
                    'K' => 'wan_fax',
                    'L' => 'wan_tit',
                    'M' => 'wan_nom',
                ];
                
                foreach ($raw_data as $row_id => $row) {
                    if ($row_id > 1) {
                        
                        $wde_dossier = $row['A'];
                        /** @var \App\Entity\Wannexe $wannexe */
                        $wdeclar = $this->getDoctrine()
                        ->getRepository(Wdeclar::class)
                        ->findOneBy(['wde_dossier' => $wde_dossier]);
                        if(!empty($wdeclar)){
                            $wan_declar=$wdeclar->getWdeDeclar();
                       
                            $wannexe = $this->getDoctrine()
                            ->getRepository(Wannexe::class)
                            ->findOneBy(['wan_declar' => $wan_declar]);
                            
                            
                            if ($wannexe) {
                                $message['updated'] = $message['updated'] + 1;
                            } else {
                                $entityManager = $this->getDoctrine()->getManager();
                                $wannexe = new Wannexe();
                                $wannexe->setWanDeclar($wan_declar);
                                $wannexe->setWanNumsite(0);
                                $wannexe->setWanNom('');
                                $wannexe->setWanTit('');
                                $wannexe->setWanFct('');
                                $entityManager->persist($wannexe);
                                $entityManager->flush();
                                $message['created'] = $message['created'] + 1;
                                $wannexe = $this->getDoctrine()
                                ->getRepository(Wannexe::class)
                                ->findOneBy(['wan_declar' => $wan_declar]);
                                
                            }
                            
                            foreach ($column_map as $col_name => $field_name) {
                            
                                if ($propertyAccessor->isWritable($wannexe, $field_name)) {
                                    $value = $row[$col_name] ? $row[$col_name] : '';
                                    $propertyAccessor->setValue($wannexe, $field_name, $value);
                                } else {
                                    $message['warnings'][] = "$field_name is not writable";
                                }
                            }

                            $entityManager = $this->getDoctrine()->getManager();
                            $entityManager->persist($wannexe);
                            $entityManager->flush();
                            
                        }

                        
                        

                    }
                }
                // Import status
                $this->addFlash(
                    'sonata_flash_success',
                    sprintf('Successfully Imported! Updated %d and created %d wannexe.',
                        $message['updated'],
                        $message['created']
                        )
                    );
                if (!empty($message['warnings'])) {
                    foreach ($message['warnings'] as $warning) {
                        $this->addFlash('warning', $warning);
                    }
                }
            }
            
            

           
        }

        return $this->render('Admin/import.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    
    public function exportDelefAction(){
        return $this->renderWithExtraParams('Admin/export.html.twig', array(
            'action' => 'import',
            'elements' => $this->admin->getShow(),
        ), null);
    }

}