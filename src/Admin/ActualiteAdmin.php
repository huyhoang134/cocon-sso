<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Sonata\Form\Type\DatePickerType;
use Sonata\Form\Type\DateTimePickerType;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;

final class ActualiteAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('label',null, ['label'=>'Titre'])
            //->add('content')
            ->add('dateDeb', \Sonata\DoctrineORMAdminBundle\Filter\DateFilter::class, ['label'=>'Date début'])
            ->add('dateFin', \Sonata\DoctrineORMAdminBundle\Filter\DateFilter::class,  ['label'=>'Date fin'])
            ->add('actif',null, ['label'=>'Activé'])
            ->add('allWtype',null, ['label'=>'Tous les contrats'])
            ->add('wtype',null, ['label'=>'Types de contrats'])
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('id')
            ->add('label',null, ['label'=>'Titre','editable' => true])
            //->add('content')
            ->add('dateDeb', null, ['label'=>'Date début', 'locale' => 'fr','editable' => true])
            ->add('dateFin', null, ['label'=>'Date fin', 'locale' => 'fr','editable' => true])
            ->add('actif',null, ['label'=>'Activé','editable' => true])
            ->add('allWtype',null, ['label'=>'Tous les contrats','editable' => true])
            ->add('wtype',null, ['label'=>'Types de contrats','editable' => true])
            ->add('_action', null, [
                'actions' => [
                    /*'show' => [],*/
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper->tab('General');
            $formMapper->with('', ['class' => 'col-md-8 abcdaire abcdaire-content']);
                $formMapper->add('label',null, ['label'=>'Titre']);
                $formMapper->add('content', SimpleFormatterType::class, [
                    'format' => 'richhtml',
                    'ckeditor_context' => 'default',
                    'required' => false,
                    'label'=>'Contenu de l\'actualité',
                    'attr' => ['placeholder' => '']]);
            $formMapper->end();
            $formMapper->with('Activation', ['class' => 'col-md-4 abcdaire']);
                $formMapper->add('dateDeb', DatePickerType::class, ['label'=>'Date début']);
                $formMapper->add('dateFin', DatePickerType::class, ['label'=>'Date fin']);
                $formMapper->add('actif',null, ['label'=>'Activé']);
            
            $formMapper->end();
            
            $formMapper->with('Affectation aux contrats', ['class' => 'col-md-4 abcdaire']);
                $formMapper->add('allWtype',null, ['label'=>'Tous les contrats']);
                $formMapper->add('wtype',null, ['label'=>'Types de contrats']);
            $formMapper->end();
            
        $formMapper->end();
        

    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('label',null, ['label'=>'Titre'])
            ->add('content')
            ->add('dateDeb', DatePickerType::class, ['label'=>'Date début'])
            ->add('dateFin', DatePickerType::class, ['label'=>'Date fin'])
            ->add('actif',null, ['label'=>'Activé'])
            ;
    }
}
