<?php

namespace App\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
//use Sonata\AdminBundle\Form\Type\ModelType;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\AdminBundle\Route\RouteCollection;
#use Pix\SortableBehaviorBundle\Services\PositionORMHandler as PositionHandler;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface as RoutingUrlGeneratorInterface;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;

use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use Burgov\Bundle\KeyValueFormBundle\Form\Type\KeyValueType;

class WconfAdmin extends AbstractAdmin
{
    

     
    protected $parentAssociationMapping = 'wconf_typcont';
    
    public $last_position = 0;
    private $positionService;
    
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_by' => 'position',
        '_sort_order' => 'ASC',
        '_per_page' => '256'
    );
    
    protected $perPageOptions = [16, 32, 64, 128, 192, 256];
    protected $maxPerPage = '256';
    
    private $etapes=array(
                            'Coordonnées' => 'coordonnees',
                            'Déclaration' => 'declaration',
                        );
    
    private $validation_email=array(
        '' => '0',
        'Email principal' => '1',
        'Email secondaire' => '2',
        'Email Secondaire BIS' => '3'
    );
    
    private $object_type=array(
                        'Interface : Titre' => 'title',
                        /*'Interface : Titre - oui/non' => 'title_yes_no',*/
                        'Interface : Sous-titre' => 'subtitle',
                        'Interface : Ligne séparatrice horizontal'=>'line_separator',
                        
                        'Champs : Liste' => 'list',
                        'Champs : Radio' => 'radio',
                        'Champs : checkbox' => 'checkbox',
                        'Champs : Text (max:25)' => 'text_max_25',
                        'Champs : Text (max:50)' => 'text_max_50',
                        'Champs : Text (max:40)' => 'text_max_40',
                        'Champs : Text (max:80)' => 'text_max_80',
                        'Champs : Text (max:100)' => 'text_max_100',
                        'Champs : Code Postal' => 'zipcode',
                        'Champs : Email' => 'email',
                        'Champs : Téléphone' => 'telephone',
                        'Champs : Civilité' => 'civilite',
                        'Champs : Numerique (max:2)' => 'num_2',
                        'Champs : Numerique (max:3)' => 'num_3',
                        'Champs : Numerique (max:4)' => 'num_4',
                        'Champs : Numerique (max:5)' => 'num_5',
                        'Champs : Numerique (max:6)' => 'num_6',
                        'Champs : Numerique (max:7)' => 'num_7',
                        /*'Champs/Titre : Numerique (max:5)' => 'title_num_5',
                        'Champs/Titre : Numerique (max:6)' => 'title_num_6',
                        'Champs/Titre : Numerique (max:7)' => 'title_num_7',*/
                        /*'Champs/Sous-Titre : Numerique (max:5)' => 'subtitle_num_5',
                        'Champs/Sous-Titre : Numerique (max:7)' => 'subtitle_num_7',*/
                        'Champs : Upload de fichier' => 'file_upload',
                        'Champs : Prestataire de veille' => 'prestataire_veille_media',
                        
                        /* 'Champs : Siret' => 'siret',*/
                        'Champs/Accordéon : num+options' => 'accordion_options',
                        'Bloc Bon de commande' => 'bon_commande',
                        'Bloc Chorus pro' => 'chorus_pro',
                        'Bloc Chorus pro étendu' => 'chorus_pro_extended',
                        'Bloc Composantes' => 'wcompos',
                        'Bloc Composantes Stages' => 'wcompos_stages',
                        
                        'Bloc panorama presse numérique' => 'panorama_press_numerique',
                        'Bloc panorama presse papier' => 'panorama_press_papier',
                        //Contrat CIP/DEA + CIP/ENS
                        'Bloc Abonnements presse + livres' => 'abo_press_livres',
                        'Bloc Abonnements presse' => 'abo_press',
                        //Contrat erp - relations publics//
                        'Bloc relation publique' => 'relation_publique',
                        'Bloc CMA/BTP' => 'cma_btp',
                        /*New contract, can be done on step 2 of the project.*/
                        //CNI/ST16
                        /*'Bloc Abonnements press' => 'abo_press',*/
                        'Bloc RGPD' => 'rgpd',
        
                        'Bloc Copie Ext Web/Reseau' => 'copie_ext_web_reseau',
                        'Bloc Copie Ext Cible' => 'copie_ext_cible',
                       
                        //Contrat LEEM
                        //'Bloc Abonnements titre + sites internet' => 'abo_titres_internet',
                        //'Bloc upload fichier excell' => 'abo_excell',
                        //Contrat EPP11
                        
                        //'Bloc panorama press light' => 'panorama_press_light',

    );
    
    private $type_objet_conf_key=array(
                                //display a sub label text
                                'Label description'=>'label_description',
                                'Label style'=>'label_style',
                                'Label class'=>'label_class',
                                'Conteneur class'=>'container_class',
                                'Conteneur style'=>'container_style',
                                

                                'Alerte au changement de liste'=> 'alert_change_list',

                                //if empty then display a popup configured as bellow
                                'Message non vide Popup titre'=> 'not_empty_message_pop_title',
                                'Message non vide Popup contenu'=> 'not_empty_message_pop_content',
                                'Message non vide Popup btn oui'=> 'not_empty_message_pop_btn_y',
                                'Message non vide Popup btn non'=> 'not_empty_message_pop_btn_n',

        
        
                                /* Groupe option : see Type : CS*/
                                'Groupe option : id'=> 'group_option_id',//Set an group of checkbox to act like option. If tow item of the same group are checked
                                'Groupe option : mess'=> 'group_option_msg',//raise an error message. It should be a bloking message.
                                
                                /* checkbox value in DB|value displayed when checked/unchecked */
                                'Checkbox valeur on'=>'option_val_on', //1|oui
                                'Checkbox valeur off'=>'option_val_off', //0|non
                                //'Checkbox uncheck ids'=>'option_id_uncheck', // uncheck id id current is checked
                                'group_accordion'=>'group_accordion_id', // id of group for accordion.
                                
                                /*set of options for accordion num+option*/
                                '===Champs/Accordion : num+options==='=>'===Champs/Accordion : num+options===',
                                'Accordion Valeur Label'=>'accordion_value_label',
                                'Accordion Bootstrap col Label'=>'accordion_value_label_col',
        
                                'Accordion Option title'=>'accordion_option_title',
        
                                'Accordion Option 1 Label'=>'accordion_option1_label',
                                'Accordion champs destination 1'=>'accordion_synchro_field_1',
        
                                'Accordion Option 2 Label'=>'accordion_option2_label',
                                'Accordion champs destination 2'=>'accordion_synchro_field_2',
        
                                '==== Bootstrap grid ====='=>'__',
                                'Bootstrap : label size (default col-md-: 2)'=>'display_label_size',
                                'Bootstrap : Column'=>'display_column',
                                'Bootstrap : 3 cols prefix label'=>'display_column_prefix',
                                'Bootstrap : 3 cols prefix size'=>'display_column_prefix_size',
                                '==== Simple ====='=>'__',
                                'Message erreur non vide'=> 'not_empty_message',
                                //Display a annuler/ok alert box if item is empty.
                                'Alerte non bloquante si vide'=>"not_empty_alert",
     
                                '==== Groupes ====='=>'_',
                                //group of items that have to be filled by at last 1 item of the group
                                //if none are filled, display message from not_empty_message
                                //It is a blocking message.
                                'Groupe 1 non vide : ID'=>'group_1_not_empty_id',
                                'Groupe 1 non vide : Message'=>'group_1_not_empty_message',
                                'Groupe 1 non vide : alert non bloquante'=>'group_1_not_empty_alert',
                                
                                //group of items that have to be filled if at least one is not empty.
                                //if have group of 4, and all empty. no message is display. 
                                //But if one have been filled, then all have to be filled. 
                                //It is a blocking message.
                                'Groupe si 1 => tous : ID'=>'group_if_one_then_all_id',
                                'Groupe si 1 => tous : Message'=>'group_if_one_then_all_message',
                                'Groupe si 1 => tous : cible uniquement'=>'group_if_one_then_all_target',
        
        
                                //Sum of the group id should be equal to student_sum_total value.
                                //there could be many student_sum_group_id in the same pages
                                'Groupe somme : ID'=> 'group_sum_id', // set an string id to group fields
                                'Groupe somme : Reference'=> 'group_sum_ref', // set 1 if this field is the sum reference
                                'Groupe somme : message err'=> 'group_sum_msg', // Set the message when the sum of the group is not equal to the ref field value
                                'Groupe somme : message alerte'=> 'group_sum_alert_msg', // Set the message when the sum of the group is not equal to the ref field value. This is a non blocking alert
                                
                                '==== Inférieur ou égale ====='=>'____',
                                'Champs numérique inférieur ou égale à (destination field)'=> 'field_inf_ou_egale', // set an string id to group fields
        
                                '==== Radio/Listes ====='=>'___',
                                //list of value for option or select value|label
                                'Valeur de liste : 1'=>'list_value_1',
                                'Valeur de liste : 2'=>'list_value_2',
                                'Valeur de liste : 3'=>'list_value_3',
                                'Valeur de liste : 4'=>'list_value_4',
                                'Valeur de liste : 5'=>'list_value_5',
                                'Valeur de liste : 6'=>'list_value_6',
                                'Valeur de liste : 7'=>'list_value_7',
                                'Valeur de liste : 8'=>'list_value_8',
                                'Valeur de liste : 9'=>'list_value_9',
                                'Valeur de liste : 10'=>'list_value_10',
        
                                '==== Chorus PRO ====='=>'___',
                                //Configuration for chorus pro process.
                                'Chorus : Bon de commande message'=>'chorus_bdc_message',
                                'Chorus : Siret message'=>'chorus_siret_message',
        
                                'Chorus : Bon de commande pas obligatoire (mettre 1)'=>'chorus_bdc_not_mandatory',
        
                                '==== Contact ====='=>'___',
                                //Configuration for chorus pro process.
                                'Contact : mettre 1 si champs contact'=>'contact_is_contact',
                                'Contact : Champs ID '=>'contact_field_id',
                                'Contact : Champs civilité'=>'contact_field_civilite',
                                'Contact : Champs fonction'=>'contact_field_function',
                                'Contact : Champs téléphone'=>'contact_field_telephone',
                                'Contact : Champs fax'=>'contact_field_fax',
                                'Contact : Champs email'=>'contact_field_email',
        
                                '==== Adresse ====='=>'___',
                                //Configuration for chorus pro process.
                                'Adresse : mettre 1 si champs adresse'=>'adress_is_adress',
                                'Adresse : Champs ID '=>'adress_field_id',
                                'Adresse : Champs Intitule'=>'adress_field_intitule',
                                'Adresse : Champs Nom1'=>'adress_field_nom1',
                                'Adresse : Champs Nom2'=>'adress_field_nom2',
                                'Adresse : Champs Nom3'=>'adress_field_nom3',
                                'Adresse : Champs Adresse1'=>'adress_field_adresse1',
                                'Adresse : Champs Adresse2'=>'adress_field_adresse2',
                                'Adresse : Champs Adresse3'=>'adress_field_adresse3',
                                'Adresse : Champs Code Postal'=>'adress_field_code_postal',
                                'Adresse : Champs Ville'=>'adress_field_ville',
                                'Adresse : Champs Web'=>'adress_field_web',
                                'Adresse : Champs Email'=>'adress_field_email',
                                'Adresse : Champs Tel'=>'adress_field_tel',
                                'Adresse : Champs Fax'=>'adress_field_fax',
                                                        
        
        
                                '==== Upload de fichiers ====='=>'______',
                                'Upload Fichier : Template (media id)'=>'upload_file_media_id',
                                'Upload Fichier : Label template (utiliser %link% pour lier le fichier)'=>'upload_file_template_label',
                                'Upload Fichier : Extensions (.xls, .xlsx)'=>'upload_file_extensions',
                                '==== RGPD ====='=>'_____',
                                'Text RGPD formulaire' => 'rgpd_text_form',
                                'Text RGPD accepté' => 'rgpd_text_oui',
                                'Text RGPD rejeté' => 'rgpd_text_non',
        
                                '==== Composantes ====='=>'_____',
                                'Composantes : Label intitulé' => 'wcompos_label_intitule',
                                'Composantes : Label effectif' => 'wcompos_label_effectif',
                                'Composantes : Label lien ajouter' => 'wcompos_label_lien_ajouter',
                                'Composantes + CMA/BTP : Label lien supprimer' => 'wcompos_label_lien_supprimer',
                                'Composantes : Message erreur si vide' => 'wcompos_msg_err_vide',
                                '==== CMA/BTP ====='=>'______',
                                'CMA/BTP : Label' => 'cma_btp_label',
                                'CMA/BTP : Label placeholder' => 'cma_btp_label_placeholder',
                                'CMA/BTP : label size (default col-md-: 2)' => 'cma_btp_label_size',
                                'CMA/BTP : Btn label' => 'cma_btp_btn_label',
                                'CMA/BTP : Message non vide' => 'cma_btp_emtpy_msg',

        

                                /*
                                '==== Composante ====='=>'___',
                                'Composante label 1'=> 'compos_label_1',
                                'Composante label 2'=> 'compos_label_2',
                                'Composante label 3'=> 'compos_label_3',*/

        
        
    );
    
    private $synchro_fields=array(
                            '=========' => '',
                            'wco_website' => 'wco_website',
                            'wco_synchro' => 'wco_synchro',
                            'wco_password' => 'wco_password',
                            'wco_raisoc1' => 'wco_raisoc1',
                            'wco_raisoc2' => 'wco_raisoc2',
                            'wco_raisoc3' => 'wco_raisoc3',
                            'wco_numvoie' => 'wco_numvoie',
                            'wco_adresse1' => 'wco_adresse1',
                            'wco_adresse2' => 'wco_adresse2',
                            'wco_adresse3' => 'wco_adresse3',
                            'wco_copos' => 'wco_copos',
                            'wco_ville' => 'wco_ville',
                            'wco_siret' => 'wco_siret',
                            'wco_tel' => 'wco_tel',
                            'wco_fax' => 'wco_fax',
                            'wco_email' => 'wco_email',
                            'wco_president' => 'wco_president',
        
                            'wco_nom_legal' => 'wco_nom_legal',
                            'wco_tit_legal' => 'wco_tit_legal',
                            'wco_fct_legal' => 'wco_fct_legal',
                            'wco_entete' => 'wco_entete',
                            'wco_typetabl' => 'wco_typetabl',
                            'wco_mel_legal' => 'wco_mel_legal',
                            'wco_tel_legal' => 'wco_tel_legal',
        


        
        
        
                            '================' => '================',
                            'wde_eleves' => 'wde_eleves',
                            'wde_copieurs' => 'wde_copieurs',
                            'wde_profs' => 'wde_profs',
                            'wde_etat_declar' => 'wde_etat_declar',
                            'wde_date_validation' => 'wde_date_validation',
                            'wde_email_sent' => 'wde_email_sent',
                            'wde_nom_resp' => 'wde_nom_resp',
                            'wde_tit_resp' => 'wde_tit_resp',
                            'wde_fct_resp' => 'wde_fct_resp',
                            'wde_mel_resp' => 'wde_mel_resp',
                            'wde_tranche' => 'wde_tranche',
                            'wde_tranche01' => 'wde_tranche01',
                            'wde_tranche02' => 'wde_tranche02',
                            'wde_tranche03' => 'wde_tranche03',
                            'wde_tranche04' => 'wde_tranche04',
                            'wde_tranche05' => 'wde_tranche05',
                            'wde_tranche06' => 'wde_tranche06',
                            'wde_tranche07' => 'wde_tranche07',
                            'wde_tranche08' => 'wde_tranche08',
                            'wde_tranche09' => 'wde_tranche09',
                            'wde_tranche10' => 'wde_tranche10',
                            'wde_tranche11' => 'wde_tranche11',
                            'wde_tranche12' => 'wde_tranche12',
                            'wde_tranche13' => 'wde_tranche13',
                            'wde_tranche14' => 'wde_tranche14',
                            'wde_tranche15' => 'wde_tranche15',
                            'wde_tranche16' => 'wde_tranche16',
                            'wde_tranche17' => 'wde_tranche17',
                            'wde_tranche18' => 'wde_tranche18',
                            'wde_tranche19' => 'wde_tranche19',
                            'wde_tranche20' => 'wde_tranche20',
                            'wde_tranche21' => 'wde_tranche21',
                            'wde_tranche22' => 'wde_tranche22',
                            'wde_nom_sec_gen' => 'wde_nom_sec_gen',
                            'wde_tit_sec_gen' => 'wde_tit_sec_gen',
                            'wde_fct_sec_gen' => 'wde_fct_sec_gen',
                            'wde_mel_sec_gen' => 'wde_mel_sec_gen',
                            'wde_annee_decl' => 'wde_annee_decl',
                            'wde_tel_resp' => 'wde_tel_resp',
                            'wde_fax_resp' => 'wde_fax_resp',
                            'wde_effectif' => 'wde_effectif',
                            'wde_effectif2' => 'wde_effectif2',
                            'wde_effectif3' => 'wde_effectif3',
                            'wde_effectif4' => 'wde_effectif4',
                            'wde_is_chorus_pro' => 'wde_is_chorus_pro',
                            'wde_siret' => 'wde_siret',
                            'wde_service_code' => 'wde_service_code',
                            'wde_is_order_number' => 'wde_is_order_number',
                            'wde_order_number' => 'wde_order_number',

                            'wde_nom_fact'=>'wde_nom_fact',
                            'wde_tit_fact'=>'wde_tit_fact',
                            'wde_fct_fact'=>'wde_fct_fact',
                            //'wde_numvoie_fact'=>'wde_numvoie_fact',
                            'wde_raisoc1_fact'=>'wde_raisoc1_fact',
                            'wde_adresse1_fact'=>'wde_adresse1_fact',
                            'wde_adresse2_fact'=>'wde_adresse2_fact',
                            'wde_adresse3_fact'=>'wde_adresse3_fact',
                            'wde_copos_fact'=>'wde_copos_fact',
                            'wde_ville_fact'=>'wde_ville_fact',
                            'wde_tel_fact'=>'wde_tel_fact',
                            //'wde_fax_fact'=>'wde_fax_fact',
                            'wde_mail_fact'=>'wde_mail_fact',
                            '===============' => '===============',
                            'wan_numsite' => 'wan_numsite',
                            'wan_raisoc1' => 'wan_raisoc1',
                            'wan_raisoc2' => 'wan_raisoc2',
                            'wan_raisoc3' => 'wan_raisoc3',
                            'wan_numvoie' => 'wan_numvoie',
                            'wan_adresse1' => 'wan_adresse1',
                            'wan_adresse2' => 'wan_adresse2',
                            'wan_adresse3' => 'wan_adresse3',
                            'wan_copos' => 'wan_copos',
                            'wan_ville' => 'wan_ville',
                            'wan_tel' => 'wan_tel',
                            'wan_fax' => 'wan_fax',
                            'wan_website' => 'wan_website',
                            'wan_nom' => 'wan_nom',
                            'wan_tit' => 'wan_tit',
                            'wan_fct' => 'wan_fct',
                            'wan_tranche01' => 'wan_tranche01',
                            'wan_tranche02' => 'wan_tranche02',
                            'wan_tranche03' => 'wan_tranche03',
                            'wan_tranche04' => 'wan_tranche04',
                            'wan_tranche05' => 'wan_tranche05',
                            'wan_tranche06' => 'wan_tranche06',
                            'wan_tranche07' => 'wan_tranche07',
                            'wan_tranche08' => 'wan_tranche08',
       
                            'wcompos' => 'wcompos',
                            'panorama_press'=>'panorama_press',
                            'abo_press_livres'=>'abo_press_livres',
                            'wprestataire'=>'wprestataire',
                            'relation_publique'=>'relation_publique',
                            'cma_btp'=>'cma_btp',
                            'wrgpd'=>'wrgpd',
                            'wcpextw'=>'wcpextw (copie ext web)',
                            'wcpextc'=>'wcpextc (copie ext cibles)'

                        );
    
    public function __construct( $code, $class, $baseControllerName ) {
        parent::__construct( $code, $class, $baseControllerName );
    
    }
    
    
    public function setPositionService(PositionHandler $positionHandler)
    {
        $this->positionService = $positionHandler;
    }
    
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper->tab('General');
            $formMapper->with('Informations générales', ['class' => 'col-md-8']);
                //$formMapper->add('id', IntegerType::class, ['required' => false,  'label'=>'Id','attr' => ['placeholder' => '']]);
                $formMapper->add('label', TextType::class, ['label'=>'Label', 'required' => false]);
                
                $formMapper->add('type_objet', ChoiceType::class, [
                    'choices' => $this->object_type,
                    'placeholder' => 'Type de l\'objet',
                    'required' => false,
                    'label' => 'Type de l\'objet'
                ]);


                $formMapper->add('type_objet_conf', KeyValueType::class, 
                    array(
                        'value_type' => TextType::class,
                        'value_options'=>['label'=>'Valeur'],
                        'key_type'=> ChoiceType::class,
                        'key_options'=>['choices' => $this->type_objet_conf_key, 'label'=>'Option'],
                        'label'=>'Options de l\'objet'
                    ));
                
                $formMapper->end();
                

                
                $formMapper->with('Contrat', ['class' => 'col-md-4 abcdaire'])
                    ->add('wconf_typcont', EntityType::class, [
                        'class' => 'App\Entity\Wtype',
                        'choice_label' => 'label',
                        'label' => 'Type de contrat',
                        'multiple' => false,
                        'required' => false,
                    ])
                ->end();
                
                $formMapper->with('Status', ['class' => 'col-md-4 abcdaire'])
                ->add('actif', CheckboxType::class, ['required' => false])
                ->add('label_visible', CheckboxType::class, ['label'=>'Affichage du label', 'required' => false])
                ->add('mandatory', CheckboxType::class, ['label'=>'Obligatoire', 'required' => false])
                ->add('affichage_espace_ferme', CheckboxType::class, ['label'=>'Affichage en espace fermé', 'required' => false])
                
                ->end();
                
                
                $formMapper->with('Positionnement', ['class' => 'col-md-4 abcdaire'])
                     ->add('etape', ChoiceType::class, [
                        'choices' => $this->etapes,
                        'placeholder' => 'Selection du champs',
                        'required' => false,
                        'label' => 'Etape'
                    ])
                    
                    ->add('position', IntegerType::class, [
                        'required' => false,  
                        'data' => $this->hasSubject() && null !== $this->getSubject()->getPosition() ? $this->getSubject()->getPosition() : 9999, 
                        'label'=>'Ordre',
                        'attr' => ['placeholder' => '']])
                ->end();

                $formMapper->with('Champ de synchronisation', ['class' => 'col-md-4 abcdaire'])
                    ->add('synchro_field', ChoiceType::class, [
                        'choices' => $this->synchro_fields,
                        'placeholder' => 'Selection du champs',
                        'required' => false,
                        'label' => 'Destination du champs '
                    ])
                ->end();
           

                $formMapper->with('Email', ['class' => 'col-md-4 abcdaire'])
                ->add('validation_email', ChoiceType::class, [
                    'choices' => $this->validation_email,
                    'placeholder' => 'Selection du champs',
                    'required' => false,
                    'label' => 'Type de champs email ',
                    'data' => $this->hasSubject() && null !== $this->getSubject()->getValidationEmail() ? $this->getSubject()->getValidationEmail() : 0,
                    'help' => 'Email principal : Est envoyé lors de l\'accusé reception et la validation<br>
                               Email secondaire : Est envoyé lors de l\'accusé reception si l\'email principal est vide, et la validation
                               Email secondaire Bis: Est envoyé lors de l\'accusé reception si l\'email principal est vide, et dans la validation si l\'email principal est vide.
                    '
                ])
                ->end();
                
                
           /*     
            *   private $validation_email=array(
                    '' => '0',
                    'Email principal' => '1',
                    'Email secondaire' => '2',
                );
                */
                
                
        $formMapper->end();
        

            
        
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        
        $q=$this->createQuery()->getQueryBuilder()->getEntityManager()->getRepository('App\Entity\Wtype')->createQueryBuilder("w")
        ->select(['w.id','w.label'])->getQuery()->getResult();
        $wtype_list=[];
        foreach($q as $i=>$v){
            $wtype_list[$v['label']]=$v;
        }
       // sm($wtype_list);
        //SELECT id, label FROM `wtype`
            //sm($this->createQuery()->getQueryBuilder()->getEntityManager()->getRepository('App\Entity\Wtype')->createQueryBuilder("w"));
            //$entityManager = $this->container->get('doctrine')->getManager();
          //EZ  die();
        
        $listMapper->add('_action', null, array(
            'actions' => array(
                'move' => array(
                    'template' => 'Admin/sort_drag_drop.html.twig',
                    'enable_top_bottom_buttons' => false, //optional
                ),
            ),
        ))
        ;


        $listMapper->add('etape', 'choice', ['label' => 'Etape', 'editable' => true, 'choices' => array_flip($this->etapes), 'choices_as_values' => true ]);
        $listMapper->add('position', null, [ 'label' => 'position','sortable'=>true, 'editable' => true]);
        $listMapper->add('id', null, ['label' => 'id','sortable'=>true]);
        $listMapper->add('label', null, ['editable' => true , 'label' => 'Titre']);
        $listMapper->add('label_visible', null, [ 'label' => 'Aff. titre', 'editable' => true]);

        
        $listMapper->add('type_objet', 'choice', ['editable' => true,'choices' => array_flip($this->object_type), 'choices_as_values' => true,  'label' => 'Objet ']);
        $listMapper->add('synchro_field', 'choice', ['editable' => true, 'choices' => $this->synchro_fields, 'label' => 'Champ Synchro ']);
        $listMapper->add('actif', null, ['editable' => true]);
        $listMapper->add('mandatory', null, [ 'label' => 'obligatoire', 'editable' => true]);
        $listMapper->add('affichage_espace_ferme', null, [ 'label' => 'Espace Ferme', 'editable' => true]);
        
        
        
        
        $listMapper->add('validation_email', 'choice', ['editable' => true,'choices' => array_flip($this->validation_email), 'choices_as_values' => true,  'label' => 'Type d\'email ']);
        
        
        $listMapper->add('wconf_typcont', 'choice', ['label' => 'Type de contrat','editable' => true, 'choices' =>$wtype_list]);
        
        
        $listMapper->add('_edition', 'actions', [
            'actions' => [
                'edit' => [],
                'clone_wconf' => [
                    'template' => 'Admin/list__action_clone_wconf.html.twig'
                ]
            ]
        ]);

        //unset mosaic mode in list view
        unset($this->listModes['mosaic']);
    }
 
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter().'/move_null/{position}');
        $collection->add('clone_wconf', $this->getRouterIdParameter().'/clone_wconf');
        
        
        if ($this->isChild()) {
            return;
        }
        
        $collection->clear();
    }
    
}