<?php

/*
 * This file is part of the Sonata Project package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\Type;

use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\ClassificationBundle\Form\ChoiceList\CategoryChoiceLoader as CategoryChoiceLoader;
use Sonata\ClassificationBundle\Model\CategoryInterface;
use Sonata\ClassificationBundle\Model\CategoryManagerInterface;
use Sonata\CoreBundle\Model\ManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\Loader\ChoiceLoaderInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Doctrine\ORM\EntityManagerInterface;

use Sonata\AdminBundle\Form\Type\ModelListType;



use Symfony\Component\Form\FormBuilderInterface as FormBuilder;
/**
 * Select a category.
 *
 * @author Thomas Rabaix <thomas.rabaix@sonata-project.org>
 */
class ContratsPropositionsType extends AbstractType
{
    /**
     * @var CategoryManagerInterface
     */
    protected $manager;

    /**
     * @param ManagerInterface $manager
     */
   /* public function __construct(Doctrine $doctrine)
    {
        $this->manager = $manager;
        sm($this);
        die();
    }
    */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    
    
    
    
    /**
     * NEXT_MAJOR: replace usage of deprecated 'choice_list' option, when bumping requirements to SF 2.7+.
     *
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'model_manager' => null,
            'current_item' => null,
        ]);
        
        $that = $this;

        $resolver->setDefaults([
            'context' => null,
            'category' => null,
            'choice_loader' => function (Options $opts, $previousValue) use ($that) {
                

            
           
            
            $subject=$opts['current_item'];
            $propositions=!empty($subject->getContratsPropositions())?$subject->getContratsPropositions()->toArray():false;

            foreach($propositions as $t){
                $propositionsList[]=$t->getId();
            }

            $query = $this->entityManager->getRepository('App\Entity\Wtype')->createQueryBuilder("w");

            $query->orderBy('w.label', 'ASC');

            $restults = $query->getQuery()->getResult();

            
            if(!empty($restults))
            {
                $option_list=array();
                foreach($restults as $r)
                {
                    
                    $option_list[$r->getId()]=$r->getLabel();
                    
                }
                return new CategoryChoiceLoader(array_flip($option_list));
            }
         },
        ]);
    }



    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return ModelType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sonata_category_selector_custom';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

}
