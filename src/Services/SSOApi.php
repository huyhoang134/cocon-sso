<?php

namespace App\Services;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class SSOApi.
 */
class SSOApi
{
    /** @var string */
    private $appKey;

    /** @var string */
    private $appSecret;

    /** @var string */
    private $ssoServerAddress;

    /**
     * Construct.
     */
    public function __construct(ParameterBagInterface $params)
    {
        $this->appKey           = $params->get('webapp_key');
        $this->appSecret        = $params->get('webapp_secret');
        $this->ssoServerAddress = $params->get('sso_server_address');
    }

    /**
     * @param $postFields
     *
     * @return mixed
     */
    public function request(string $url, bool $isPost = false, $postFields = [], array $headers = []): array
    {
        // init curl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->ssoServerAddress . $url);

        // set headers
        $headers = array_merge($headers, [
            'X-APP-ID: ' . $this->appKey,
            'X-APP-SECRET: ' . $this->appSecret,
        ]);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // set post params (get params should be added in query string)
        if ($isPost) {
            curl_setopt($ch, CURLOPT_POST, true);

            if ( ! empty($postFields)) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
            }
        }

        // return response
        $response = json_decode(curl_exec($ch), true);

        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return [$response, $code];
    }
}
