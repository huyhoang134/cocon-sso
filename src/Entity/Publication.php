<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Publication
 *
 * @ORM\Table(name="publication", uniqueConstraints={@ORM\UniqueConstraint(name="unicite", columns={"publication_titre", "publication_country_code", "publication_type"})}, indexes={@ORM\Index(name="titre_2", columns={"publication_titre", "publication_type"}), @ORM\Index(name="publication_titre", columns={"publication_titre"})})
 * @ORM\Entity(repositoryClass="App\Repository\PublicationRepository")
 */
class Publication
{
    /**
     * @var int
     *
     * @ORM\Column(name="publication_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $publicationId;

    /**
     * @var string
     *
     * @ORM\Column(name="publication_titre", type="string", length=250, nullable=false)
     */
    private $publicationTitre;

    /**
     * @var int|null
     *
     * @ORM\Column(name="publication_type", type="integer", nullable=true)
     */
    private $publicationType;

    /**
     * @var string
     *
     * @ORM\Column(name="publication_country_code", type="string", length=14, nullable=false, options={"default"="FR"})
     */
    private $publicationCountryCode = 'FR';

    /**
     * @var float
     *
     * @ORM\Column(name="publication_redevance_panorama", type="float", precision=10, scale=0, nullable=false)
     */
    private $publicationRedevancePanorama;

    /**
     * @var float
     *
     * @ORM\Column(name="publication_redevance_clipping", type="float", precision=10, scale=0, nullable=false)
     */
    private $publicationRedevanceClipping;

    /**
     * @var string|null
     *
     * @ORM\Column(name="publication_articles_auth_panorama", type="string", length=250, nullable=true)
     */
    private $publicationArticlesAuthPanorama;

    /**
     * @var string|null
     *
     * @ORM\Column(name="publication_inclusion_infographie", type="string", length=45, nullable=true)
     */
    private $publicationInclusionInfographie;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publication_date_effet_mandat", type="date", nullable=false)
     */
    private $publicationDateEffetMandat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="publication_observation", type="text", length=65535, nullable=true)
     */
    private $publicationObservation;

    public function __toString()
    {
        return $this->getPublicationTitre();
    }
    
    public function getPublicationId(): ?int
    {
        return $this->publicationId;
    }

    public function getPublicationTitre(): ?string
    {
        return $this->publicationTitre;
    }

    public function setPublicationTitre(string $publicationTitre): self
    {
        $this->publicationTitre = $publicationTitre;

        return $this;
    }

    public function getPublicationType(): ?int
    {
        return $this->publicationType;
    }

    public function setPublicationType(?int $publicationType): self
    {
        $this->publicationType = $publicationType;

        return $this;
    }

    public function getPublicationCountryCode(): ?string
    {
        return $this->publicationCountryCode;
    }

    public function setPublicationCountryCode(string $publicationCountryCode): self
    {
        $this->publicationCountryCode = $publicationCountryCode;

        return $this;
    }

    public function getPublicationRedevancePanorama(): ?float
    {
        return $this->publicationRedevancePanorama;
    }

    public function setPublicationRedevancePanorama(float $publicationRedevancePanorama): self
    {
        $this->publicationRedevancePanorama = $publicationRedevancePanorama;

        return $this;
    }

    public function getPublicationRedevanceClipping(): ?float
    {
        return $this->publicationRedevanceClipping;
    }

    public function setPublicationRedevanceClipping(float $publicationRedevanceClipping): self
    {
        $this->publicationRedevanceClipping = $publicationRedevanceClipping;

        return $this;
    }

    public function getPublicationArticlesAuthPanorama(): ?string
    {
        return $this->publicationArticlesAuthPanorama;
    }

    public function setPublicationArticlesAuthPanorama(?string $publicationArticlesAuthPanorama): self
    {
        $this->publicationArticlesAuthPanorama = $publicationArticlesAuthPanorama;

        return $this;
    }

    public function getPublicationInclusionInfographie(): ?string
    {
        return $this->publicationInclusionInfographie;
    }

    public function setPublicationInclusionInfographie(?string $publicationInclusionInfographie): self
    {
        $this->publicationInclusionInfographie = $publicationInclusionInfographie;

        return $this;
    }

    public function getPublicationDateEffetMandat(): ?\DateTimeInterface
    {
        return $this->publicationDateEffetMandat;
    }

    public function setPublicationDateEffetMandat(\DateTimeInterface $publicationDateEffetMandat): self
    {
        $this->publicationDateEffetMandat = $publicationDateEffetMandat;

        return $this;
    }

    public function getPublicationObservation(): ?string
    {
        return $this->publicationObservation;
    }

    public function setPublicationObservation(?string $publicationObservation): self
    {
        $this->publicationObservation = $publicationObservation;

        return $this;
    }


}
