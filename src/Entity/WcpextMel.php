<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WcpextMel
 *
 * @ORM\Table(name="wcpext_mel", indexes={@ORM\Index(name="wcpext_id", columns={"wcpext_id"})})
 * @ORM\Entity
 */
class WcpextMel
{
    /**
     * @var int
     *
     * @ORM\Column(name="wcpextmel_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $wcpextmelId;

    /**
     * @var int
     *
     * @ORM\Column(name="wcpext_id", type="integer", nullable=false)
     */
    private $wcpextId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="wcpext_stamp", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $wcpextStamp = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="wcpextmel_declar", type="integer", nullable=true)
     */
    private $wcpextmelDeclar;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wcpextmel_dossier", type="integer", nullable=true)
     */
    private $wcpextmelDossier;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wcpextmel_contrat", type="integer", nullable=true)
     */
    private $wcpextmelContrat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wcpextmel_mel", type="string", length=255, nullable=true)
     */
    private $wcpextmelMel;

    public function getWcpextmelId(): ?int
    {
        return $this->wcpextmelId;
    }

    public function getWcpextId(): ?int
    {
        return $this->wcpextId;
    }

    public function setWcpextId(int $wcpextId): self
    {
        $this->wcpextId = $wcpextId;

        return $this;
    }

    public function getWcpextStamp(): ?\DateTimeInterface
    {
        return $this->wcpextStamp;
    }

    public function setWcpextStamp(\DateTimeInterface $wcpextStamp): self
    {
        $this->wcpextStamp = $wcpextStamp;

        return $this;
    }

    public function getWcpextmelDeclar(): ?int
    {
        return $this->wcpextmelDeclar;
    }

    public function setWcpextmelDeclar(?int $wcpextmelDeclar): self
    {
        $this->wcpextmelDeclar = $wcpextmelDeclar;

        return $this;
    }

    public function getWcpextmelDossier(): ?int
    {
        return $this->wcpextmelDossier;
    }

    public function setWcpextmelDossier(?int $wcpextmelDossier): self
    {
        $this->wcpextmelDossier = $wcpextmelDossier;

        return $this;
    }

    public function getWcpextmelContrat(): ?int
    {
        return $this->wcpextmelContrat;
    }

    public function setWcpextmelContrat(?int $wcpextmelContrat): self
    {
        $this->wcpextmelContrat = $wcpextmelContrat;

        return $this;
    }

    public function getWcpextmelMel(): ?string
    {
        return $this->wcpextmelMel;
    }

    public function setWcpextmelMel(?string $wcpextmelMel): self
    {
        $this->wcpextmelMel = $wcpextmelMel;

        return $this;
    }


}
