<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Wabo
 *
 * @ORM\Table(name="wabo", indexes={@ORM\Index(name="wabo_declar", columns={"wabo_declar"}), @ORM\Index(name="wabo_contrat", columns={"wabo_contrat"}), @ORM\Index(name="wabo_dossier", columns={"wabo_dossier"})})
 * @ORM\Entity
 */
class Wabo
{
    /**
     * @var int
     *
     * @ORM\Column(name="wabo_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $waboId;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="wabo_stamp", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $waboStamp = 'CURRENT_TIMESTAMP';
    
    /**
     * @var int
     *
     * @ORM\Column(name="wabo_declar", type="integer", nullable=false)
     */
    private $waboDeclar;

    /**
     * @var int
     *
     * @ORM\Column(name="wabo_dossier", type="integer", nullable=false)
     */
    private $waboDossier;

    /**
     * @var int
     *
     * @ORM\Column(name="wabo_contrat", type="integer", nullable=false)
     */
    private $waboContrat;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wabo_prestataire", type="integer", nullable=true)
     */
    private $waboPrestataire;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wabo_prestataire_label", type="string", length=255, nullable=true)
     */
    private $waboPrestataireLabel;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wabo_fichier", type="integer", nullable=true)
     */
    private $waboFichier;

    /**
     * @var string
     *
     * @ORM\Column(name="wabo_type", type="string", length=255, nullable=false)
     */
    private $waboType;


    public function getWaboId(): ?int
    {
        return $this->waboId;
    }

    public function getWaboStamp(): ?\DateTimeInterface
    {
        return $this->waboStamp;
    }
    
    public function setWaboStamp(\DateTimeInterface $waboStamp): self
    {
        $this->waboStamp = $waboStamp;
    
        return $this;
    }
    
    public function getWaboDeclar(): ?int
    {
        return $this->waboDeclar;
    }

    public function setWaboDeclar(int $waboDeclar): self
    {
        $this->waboDeclar = $waboDeclar;

        return $this;
    }

    public function getWaboDossier(): ?int
    {
        return $this->waboDossier;
    }

    public function setWaboDossier(int $waboDossier): self
    {
        $this->waboDossier = $waboDossier;

        return $this;
    }

    public function getWaboContrat(): ?int
    {
        return $this->waboContrat;
    }

    public function setWaboContrat(int $waboContrat): self
    {
        $this->waboContrat = $waboContrat;

        return $this;
    }

    public function getWaboPrestataire(): ?int
    {
        return $this->waboPrestataire;
    }

    public function setWaboPrestataire(?int $waboPrestataire): self
    {
        $this->waboPrestataire = $waboPrestataire;

        return $this;
    }

    public function getWaboPrestataireLabel(): ?string
    {
        return $this->waboPrestataireLabel;
    }

    public function setWaboPrestataireLabel(?string $waboPrestataireLabel): self
    {
        $this->waboPrestataireLabel = $waboPrestataireLabel;

        return $this;
    }

    public function getWaboFichier(): ?int
    {
        return $this->waboFichier;
    }

    public function setWaboFichier(?int $waboFichier): self
    {
        $this->waboFichier = $waboFichier;

        return $this;
    }

    public function getWaboType(): ?string
    {
        return $this->waboType;
    }

    public function setWaboType(string $waboType): self
    {
        $this->waboType = $waboType;

        return $this;
    }


    
}
