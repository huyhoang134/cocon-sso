<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Wcpext
 *
 * @ORM\Table(name="wcpext", indexes={@ORM\Index(name="declar", columns={"wcpext_declar"}), @ORM\Index(name="dossier", columns={"wcpext_dossier"})})
 * @ORM\Entity
 */
class Wcpext
{
    /**
     * @var int
     *
     * @ORM\Column(name="wcpext_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $wcpextId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="wcpext_stamp", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $wcpextStamp = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="wcpext_declar", type="integer", nullable=true)
     */
    private $wcpextDeclar;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wcpext_dossier", type="integer", nullable=true)
     */
    private $wcpextDossier;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wcpext_contrat", type="integer", nullable=true)
     */
    private $wcpextContrat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wcpext_publication", type="string", length=255, nullable=true)
     */
    private $wcpextPublication;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wcpext_date_mel", type="string", length=255, nullable=true)
     */
    private $wcpextDateMel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wcpext_titre", type="string", length=255, nullable=true)
     */
    private $wcpextTitre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wcpext_auteur", type="string", length=255, nullable=true)
     */
    private $wcpextAuteur;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wcpext_parution", type="string", length=255, nullable=true)
     */
    private $wcpextParution;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wcpext_destinataires", type="integer", nullable=true)
     */
    private $wcpextDestinataires;

    public function getWcpextId(): ?int
    {
        return $this->wcpextId;
    }

    public function getWcpextStamp(): ?\DateTimeInterface
    {
        return $this->wcpextStamp;
    }

    public function setWcpextStamp(\DateTimeInterface $wcpextStamp): self
    {
        $this->wcpextStamp = $wcpextStamp;

        return $this;
    }

    public function getWcpextDeclar(): ?int
    {
        return $this->wcpextDeclar;
    }

    public function setWcpextDeclar(?int $wcpextDeclar): self
    {
        $this->wcpextDeclar = $wcpextDeclar;

        return $this;
    }

    public function getWcpextDossier(): ?int
    {
        return $this->wcpextDossier;
    }

    public function setWcpextDossier(?int $wcpextDossier): self
    {
        $this->wcpextDossier = $wcpextDossier;

        return $this;
    }

    public function getWcpextContrat(): ?int
    {
        return $this->wcpextContrat;
    }

    public function setWcpextContrat(?int $wcpextContrat): self
    {
        $this->wcpextContrat = $wcpextContrat;

        return $this;
    }

    public function getWcpextPublication(): ?string
    {
        return $this->wcpextPublication;
    }

    public function setWcpextPublication(?string $wcpextPublication): self
    {
        $this->wcpextPublication = $wcpextPublication;

        return $this;
    }

    public function getWcpextDateMel(): ?string
    {
        return $this->wcpextDateMel;
    }

    public function setWcpextDateMel(?string $wcpextDateMel): self
    {
        $this->wcpextDateMel = $wcpextDateMel;

        return $this;
    }

    public function getWcpextTitre(): ?string
    {
        return $this->wcpextTitre;
    }

    public function setWcpextTitre(?string $wcpextTitre): self
    {
        $this->wcpextTitre = $wcpextTitre;

        return $this;
    }

    public function getWcpextAuteur(): ?string
    {
        return $this->wcpextAuteur;
    }

    public function setWcpextAuteur(?string $wcpextAuteur): self
    {
        $this->wcpextAuteur = $wcpextAuteur;

        return $this;
    }

    public function getWcpextParution(): ?string
    {
        return $this->wcpextParution;
    }

    public function setWcpextParution(?string $wcpextParution): self
    {
        $this->wcpextParution = $wcpextParution;

        return $this;
    }

    public function getWcpextDestinataires(): ?int
    {
        return $this->wcpextDestinataires;
    }

    public function setWcpextDestinataires(?int $wcpextDestinataires): self
    {
        $this->wcpextDestinataires = $wcpextDestinataires;

        return $this;
    }


}
