<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Actualite
 *
 * @ORM\Table(name="actualite")
 * @ORM\Entity
 */
class Actualite
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=true)
     */
    private $label;

    /**
     * @var string|null
     *
     * @ORM\Column(name="content", type="text", length=65535, nullable=true)
     */
    private $content;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_deb", type="date", nullable=true)
     */
    private $dateDeb;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_fin", type="date", nullable=true)
     */
    private $dateFin;
    
    /**
     * @var bool|null
     *
     * @ORM\Column(name="all_wtype", type="boolean", nullable=false)
     */
    private $allWtype = '0';
    

    /**
     * @var bool
     *
     * @ORM\Column(name="actif", type="boolean", nullable=false)
     */
    private $actif = '0';


    
    
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Wtype", cascade={"persist"})
     * @ORM\JoinTable(
     *      name="actualite_has_wtype",
     *      joinColumns={@ORM\JoinColumn(name="actualite_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="wtype_id", referencedColumnName="id")},
     *
     * )
     *  @ORM\OrderBy({"label" = "ASC"})
     */
    protected $wtype;
    
    
    public function addWtype($wtype)
    {
        $this->$wtype[] = $wtype;
    }
    
    /**
     * Get $wtype
     *
     * @return array $wtype
     */
    public function getWtype()
    {
        return $this->wtype;
    }
    
    /**
     * @param $wtype
     *
     * @return mixed
     */
    public function setWtype($wtype)
    {
        $this->wtype = $wtype;
    }
    

    public function __toString()
    {
        return $this->getLabel();
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getDateDeb(): ?\DateTimeInterface
    {
        return $this->dateDeb;
    }

    public function setDateDeb(?\DateTimeInterface $dateDeb): self
    {
        $this->dateDeb = $dateDeb;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(?\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }


    public function getAllWtype(): ?bool
    {
        return $this->allWtype;
    }
    
    public function setAllWtype(?bool $allWtype): self
    {
        $this->allWtype = $allWtype;
    
        return $this;
    }
    
    
    public function getActif(): ?bool
    {
        return $this->actif;
    }

    public function setActif(bool $actif): self
    {
        $this->actif = $actif;

        return $this;
    }


}
