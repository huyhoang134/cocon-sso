<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="wcompos", indexes={@ORM\Index(name="declar_compos", columns={"wcp_declar"})})
 * @ORM\Entity(repositoryClass="App\Repository\WcomposRepository")
 */
class Wcompos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="wcp_declar", type="integer", nullable=false)
     */
    private $wcp_declar = '0';
    

    /**
     * @ORM\Column(name="wcp_stamp", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     * @ORM\Version
     */
    private $wcp_stamp = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="wcp_synchro", type="datetime", nullable=true)
     */
    private $wcp_synchro;


    /**
     * @var string
     *
     * @ORM\Column(name="wcp_libelle", type="string", length=100, nullable=false)
     */
    private $wcp_libelle = '';

    /**
     * @var int
     *
     * @ORM\Column(name="wcp_effectif", type="integer", nullable=false)
     */
    private $wcp_effectif = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="wcp_heures", type="integer", nullable=false)
     */
    private $wcp_heures;
    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWcpStamp(): ?\DateTimeInterface
    {
        return $this->wcp_stamp;
    }

    public function setWcpStamp(\DateTimeInterface $wcp_stamp): self
    {
        $this->wcp_stamp = $wcp_stamp;

        return $this;
    }

    public function getWcpSynchro(): ?\DateTimeInterface
    {
        return $this->wcp_synchro;
    }

    public function setWcpSynchro(?\DateTimeInterface $wcp_synchro): self
    {
        $this->wcp_synchro = $wcp_synchro;

        return $this;
    }

    public function getWcpDeclar(): ?int
    {
        return $this->wcp_declar;
    }

    public function setWcpDeclar(int $wcp_declar): self
    {
        $this->wcp_declar = $wcp_declar;

        return $this;
    }

    public function getWcpLibelle(): ?string
    {
        return $this->wcp_libelle;
    }

    public function setWcpLibelle(string $wcp_libelle): self
    {
        $this->wcp_libelle = $wcp_libelle;

        return $this;
    }

    public function getWcpEffectif(): ?int
    {
        return $this->wcp_effectif;
    }

    public function setWcpEffectif(int $wcp_effectif): self
    {
        $this->wcp_effectif = $wcp_effectif;

        return $this;
    }

    public function getWcpHeures(): ?int
    {
        return $this->wcp_heures;
    }

    public function setWcpHeures(int $wcp_heures): self
    {
        $this->wcp_heures = $wcp_heures;

        return $this;
    }
}
