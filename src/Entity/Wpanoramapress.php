<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Wpanoramapress
 *
 * @ORM\Table(name="wpanoramapress", indexes={@ORM\Index(name="dossier", columns={"wpp_dossier"}), @ORM\Index(name="declar", columns={"wpp_declar"})})
 * @ORM\Entity
 */
class Wpanoramapress
{
    /**
     * @var int
     *
     * @ORM\Column(name="wpp_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $wppId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wpp_declar", type="integer", nullable=true)
     */
    private $wppDeclar;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wpp_dossier", type="integer", nullable=true)
     */
    private $wppDossier;

    /**
     * @var int
     *
     * @ORM\Column(name="wpp_contrat", type="integer", nullable=false)
     */
    private $wppContrat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wpp_type", type="string", length=255, nullable=true)
     */
    private $wppType;

    public function getWppId(): ?int
    {
        return $this->wppId;
    }

    public function getWppDeclar(): ?int
    {
        return $this->wppDeclar;
    }

    public function setWppDeclar(?int $wppDeclar): self
    {
        $this->wppDeclar = $wppDeclar;

        return $this;
    }

    public function getWppDossier(): ?int
    {
        return $this->wppDossier;
    }

    public function setWppDossier(?int $wppDossier): self
    {
        $this->wppDossier = $wppDossier;

        return $this;
    }

    public function getWppContrat(): ?int
    {
        return $this->wppContrat;
    }

    public function setWppContrat(int $wppContrat): self
    {
        $this->wppContrat = $wppContrat;

        return $this;
    }

  

    public function getWppType(): ?string
    {
        return $this->wppType;
    }

    public function setWppType(?string $wppType): self
    {
        $this->wppType = $wppType;

        return $this;
    }


}
