<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Wadresses
 *
 * @ORM\Table(name="wadresses")
 * @ORM\Entity
 */
class Wadresses
{
    /**
     * @var int
     *
     * @ORM\Column(name="PkSiteGeo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pksitegeo;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="DateSQL", type="datetime", nullable=true)
     */
    private $datesql;

    /**
     * @var int
     *
     * @ORM\Column(name="FkCocontractant", type="integer", nullable=false)
     */
    private $fkcocontractant;

    /**
     * @var int|null
     *
     * @ORM\Column(name="NbSalles", type="smallint", nullable=true)
     */
    private $nbsalles;

    /**
     * @var int|null
     *
     * @ORM\Column(name="NbMachines", type="smallint", nullable=true)
     */
    private $nbmachines;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Facturation", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private $facturation;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Declaration", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private $declaration;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FkCodeNAF", type="string", length=5, nullable=true)
     */
    private $fkcodenaf;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Intitule", type="string", length=50, nullable=true)
     */
    private $intitule;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Nom1", type="string", length=50, nullable=true)
     */
    private $nom1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Nom2", type="string", length=50, nullable=true)
     */
    private $nom2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Nom3", type="string", length=50, nullable=true)
     */
    private $nom3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Adresse1", type="string", length=50, nullable=true)
     */
    private $adresse1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Adresse2", type="string", length=50, nullable=true)
     */
    private $adresse2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Adresse3", type="string", length=50, nullable=true)
     */
    private $adresse3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CodePostal", type="string", length=10, nullable=true)
     */
    private $codepostal;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Ville", type="string", length=40, nullable=true)
     */
    private $ville;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Web", type="string", length=80, nullable=true)
     */
    private $web;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Email", type="string", length=80, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Tel", type="string", length=25, nullable=true)
     */
    private $tel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Fax", type="string", length=25, nullable=true)
     */
    private $fax;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FkPays", type="string", length=3, nullable=true)
     */
    private $fkpays;

    public function getPksitegeo(): ?int
    {
        return $this->pksitegeo;
    }

    public function getDatesql(): ?\DateTimeInterface
    {
        return $this->datesql;
    }

    public function setDatesql(?\DateTimeInterface $datesql): self
    {
        $this->datesql = $datesql;

        return $this;
    }

    public function getFkcocontractant(): ?int
    {
        return $this->fkcocontractant;
    }

    public function setFkcocontractant(int $fkcocontractant): self
    {
        $this->fkcocontractant = $fkcocontractant;

        return $this;
    }

    public function getNbsalles(): ?int
    {
        return $this->nbsalles;
    }

    public function setNbsalles(?int $nbsalles): self
    {
        $this->nbsalles = $nbsalles;

        return $this;
    }

    public function getNbmachines(): ?int
    {
        return $this->nbmachines;
    }

    public function setNbmachines(?int $nbmachines): self
    {
        $this->nbmachines = $nbmachines;

        return $this;
    }

    public function getFacturation(): ?string
    {
        return $this->facturation;
    }

    public function setFacturation(?string $facturation): self
    {
        $this->facturation = $facturation;

        return $this;
    }

    public function getDeclaration(): ?string
    {
        return $this->declaration;
    }

    public function setDeclaration(?string $declaration): self
    {
        $this->declaration = $declaration;

        return $this;
    }

    public function getFkcodenaf(): ?string
    {
        return $this->fkcodenaf;
    }

    public function setFkcodenaf(?string $fkcodenaf): self
    {
        $this->fkcodenaf = $fkcodenaf;

        return $this;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(?string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    public function getNom1(): ?string
    {
        return $this->nom1;
    }

    public function setNom1(?string $nom1): self
    {
        $this->nom1 = $nom1;

        return $this;
    }

    public function getNom2(): ?string
    {
        return $this->nom2;
    }

    public function setNom2(?string $nom2): self
    {
        $this->nom2 = $nom2;

        return $this;
    }

    public function getNom3(): ?string
    {
        return $this->nom3;
    }

    public function setNom3(?string $nom3): self
    {
        $this->nom3 = $nom3;

        return $this;
    }

    public function getAdresse1(): ?string
    {
        return $this->adresse1;
    }

    public function setAdresse1(?string $adresse1): self
    {
        $this->adresse1 = $adresse1;

        return $this;
    }

    public function getAdresse2(): ?string
    {
        return $this->adresse2;
    }

    public function setAdresse2(?string $adresse2): self
    {
        $this->adresse2 = $adresse2;

        return $this;
    }

    public function getAdresse3(): ?string
    {
        return $this->adresse3;
    }

    public function setAdresse3(?string $adresse3): self
    {
        $this->adresse3 = $adresse3;

        return $this;
    }

    public function getCodepostal(): ?string
    {
        return $this->codepostal;
    }

    public function setCodepostal(?string $codepostal): self
    {
        $this->codepostal = $codepostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getWeb(): ?string
    {
        return $this->web;
    }

    public function setWeb(?string $web): self
    {
        $this->web = $web;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(?string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function setFax(?string $fax): self
    {
        $this->fax = $fax;

        return $this;
    }

    public function getFkpays(): ?string
    {
        return $this->fkpays;
    }

    public function setFkpays(?string $fkpays): self
    {
        $this->fkpays = $fkpays;

        return $this;
    }


}
