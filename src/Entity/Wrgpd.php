<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Wrgpd
 *
 * @ORM\Table(name="wrgpd")
 * @ORM\Entity
 */
class Wrgpd
{
    /**
     * @var int
     *
     * @ORM\Column(name="wrgpd_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $wrgpdId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="wrgpd_stamp", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $wrgpdStamp = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="wrgpd_declar", type="integer", nullable=true)
     */
    private $wrgpdDeclar;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wrgpd_dossier", type="integer", nullable=true)
     */
    private $wrgpdDossier;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wrgpd_contrat", type="integer", nullable=true)
     */
    private $wrgpdContrat;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wrgpd_value", type="integer", nullable=true)
     */
    private $wrgpdValue;

    public function getWrgpdId(): ?int
    {
        return $this->wrgpdId;
    }

    public function getWrgpdStamp(): ?\DateTimeInterface
    {
        return $this->wrgpdStamp;
    }

    public function setWrgpdStamp(\DateTimeInterface $wrgpdStamp): self
    {
        $this->wrgpdStamp = $wrgpdStamp;

        return $this;
    }

    public function getWrgpdDeclar(): ?int
    {
        return $this->wrgpdDeclar;
    }

    public function setWrgpdDeclar(?int $wrgpdDeclar): self
    {
        $this->wrgpdDeclar = $wrgpdDeclar;

        return $this;
    }

    public function getWrgpdDossier(): ?int
    {
        return $this->wrgpdDossier;
    }

    public function setWrgpdDossier(?int $wrgpdDossier): self
    {
        $this->wrgpdDossier = $wrgpdDossier;

        return $this;
    }

    public function getWrgpdContrat(): ?int
    {
        return $this->wrgpdContrat;
    }

    public function setWrgpdContrat(?int $wrgpdContrat): self
    {
        $this->wrgpdContrat = $wrgpdContrat;

        return $this;
    }

    public function getWrgpdValue(): ?int
    {
        return $this->wrgpdValue;
    }

    public function setWrgpdValue(?int $wrgpdValue): self
    {
        $this->wrgpdValue = $wrgpdValue;

        return $this;
    }


}
