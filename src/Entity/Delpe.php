<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Delpe
 *
 * @ORM\Table(name="Delpe")
 * @ORM\Entity
 */
class Delpe
{
    /**
     * @var int
     *
     * @ORM\Column(name="PkDelpe", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pkdelpe;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateSQL", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $datesql = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="DtSynchro", type="datetime", nullable=true)
     */
    private $dtsynchro;

    /**
     * @var int|null
     *
     * @ORM\Column(name="FkCocontractant", type="integer", nullable=true)
     */
    private $fkcocontractant;

    /**
     * @var int|null
     *
     * @ORM\Column(name="FkContrat", type="integer", nullable=true)
     */
    private $fkcontrat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Identifiant", type="string", length=8, nullable=true)
     */
    private $identifiant;

    /**
     * @var int|null
     *
     * @ORM\Column(name="CodeTypeContrat", type="integer", nullable=true)
     */
    private $codetypecontrat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LibelleTypeContrat", type="string", length=50, nullable=true)
     */
    private $libelletypecontrat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Gestion", type="string", length=1, nullable=true)
     */
    private $gestion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="FkSiteGeo", type="integer", nullable=true)
     */
    private $fksitegeo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LibelleSiege", type="string", length=50, nullable=true)
     */
    private $libellesiege;

    /**
     * @var int
     *
     * @ORM\Column(name="FkPrimaire", type="integer", nullable=false)
     */
    private $fkprimaire;

    /**
     * @var string
     *
     * @ORM\Column(name="IdMen", type="string", length=8, nullable=false)
     */
    private $idmen;

    /**
     * @var string
     *
     * @ORM\Column(name="Password", type="string", length=8, nullable=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="Denomination", type="string", length=50, nullable=false)
     */
    private $denomination;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Patronyme", type="string", length=50, nullable=true)
     */
    private $patronyme;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Nom3", type="string", length=50, nullable=true)
     */
    private $nom3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Adresse1", type="string", length=50, nullable=true)
     */
    private $adresse1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Adresse2", type="string", length=50, nullable=true)
     */
    private $adresse2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Adresse3", type="string", length=50, nullable=true)
     */
    private $adresse3;

    /**
     * @var string
     *
     * @ORM\Column(name="CodePostal", type="string", length=5, nullable=false)
     */
    private $codepostal;

    /**
     * @var string
     *
     * @ORM\Column(name="Ville", type="string", length=40, nullable=false)
     */
    private $ville;

    /**
     * @var int
     *
     * @ORM\Column(name="FkEnquete", type="integer", nullable=false)
     */
    private $fkenquete;

    /**
     * @var string
     *
     * @ORM\Column(name="Annee", type="string", length=11, nullable=false)
     */
    private $annee;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Echantillon", type="string", length=4, nullable=true)
     */
    private $echantillon;

    /**
     * @var string
     *
     * @ORM\Column(name="Statut", type="string", length=1, nullable=false)
     */
    private $statut;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="DtValidation", type="datetime", nullable=true)
     */
    private $dtvalidation;

    /**
     * @var string|null
     *
     * @ORM\Column(name="NomPrenom", type="string", length=50, nullable=true)
     */
    private $nomprenom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Civilite", type="string", length=50, nullable=true)
     */
    private $civilite;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Fonction", type="string", length=50, nullable=true)
     */
    private $fonction;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Telephone", type="string", length=25, nullable=true)
     */
    private $telephone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Email", type="string", length=80, nullable=true)
     */
    private $email;

    /**
     * @var int|null
     *
     * @ORM\Column(name="NbMaternelle", type="integer", nullable=true)
     */
    private $nbmaternelle;

    /**
     * @var int|null
     *
     * @ORM\Column(name="NbElementaire", type="integer", nullable=true)
     */
    private $nbelementaire;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DtDeb", type="datetime", nullable=false)
     */
    private $dtdeb;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DtFin", type="datetime", nullable=false)
     */
    private $dtfin;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="DtDeb2", type="datetime", nullable=true)
     */
    private $dtdeb2;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="DtFin2", type="datetime", nullable=true)
     */
    private $dtfin2;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="DtDeb3", type="datetime", nullable=true)
     */
    private $dtdeb3;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="DtFin3", type="datetime", nullable=true)
     */
    private $dtfin3;

    public function getPkdelpe(): ?int
    {
        return $this->pkdelpe;
    }

    public function getDatesql(): ?\DateTimeInterface
    {
        return $this->datesql;
    }

    public function setDatesql(\DateTimeInterface $datesql): self
    {
        $this->datesql = $datesql;

        return $this;
    }

    public function getDtsynchro(): ?\DateTimeInterface
    {
        return $this->dtsynchro;
    }

    public function setDtsynchro(?\DateTimeInterface $dtsynchro): self
    {
        $this->dtsynchro = $dtsynchro;

        return $this;
    }

    public function getFkcocontractant(): ?int
    {
        return $this->fkcocontractant;
    }

    public function setFkcocontractant(?int $fkcocontractant): self
    {
        $this->fkcocontractant = $fkcocontractant;

        return $this;
    }

    public function getFkcontrat(): ?int
    {
        return $this->fkcontrat;
    }

    public function setFkcontrat(?int $fkcontrat): self
    {
        $this->fkcontrat = $fkcontrat;

        return $this;
    }

    public function getIdentifiant(): ?string
    {
        return $this->identifiant;
    }

    public function setIdentifiant(?string $identifiant): self
    {
        $this->identifiant = $identifiant;

        return $this;
    }

    public function getCodetypecontrat(): ?int
    {
        return $this->codetypecontrat;
    }

    public function setCodetypecontrat(?int $codetypecontrat): self
    {
        $this->codetypecontrat = $codetypecontrat;

        return $this;
    }

    public function getLibelletypecontrat(): ?string
    {
        return $this->libelletypecontrat;
    }

    public function setLibelletypecontrat(?string $libelletypecontrat): self
    {
        $this->libelletypecontrat = $libelletypecontrat;

        return $this;
    }

    public function getGestion(): ?string
    {
        return $this->gestion;
    }

    public function setGestion(?string $gestion): self
    {
        $this->gestion = $gestion;

        return $this;
    }

    public function getFksitegeo(): ?int
    {
        return $this->fksitegeo;
    }

    public function setFksitegeo(?int $fksitegeo): self
    {
        $this->fksitegeo = $fksitegeo;

        return $this;
    }

    public function getLibellesiege(): ?string
    {
        return $this->libellesiege;
    }

    public function setLibellesiege(?string $libellesiege): self
    {
        $this->libellesiege = $libellesiege;

        return $this;
    }

    public function getFkprimaire(): ?int
    {
        return $this->fkprimaire;
    }

    public function setFkprimaire(int $fkprimaire): self
    {
        $this->fkprimaire = $fkprimaire;

        return $this;
    }

    public function getIdmen(): ?string
    {
        return $this->idmen;
    }

    public function setIdmen(string $idmen): self
    {
        $this->idmen = $idmen;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getDenomination(): ?string
    {
        return $this->denomination;
    }

    public function setDenomination(string $denomination): self
    {
        $this->denomination = $denomination;

        return $this;
    }

    public function getPatronyme(): ?string
    {
        return $this->patronyme;
    }

    public function setPatronyme(?string $patronyme): self
    {
        $this->patronyme = $patronyme;

        return $this;
    }

    public function getNom3(): ?string
    {
        return $this->nom3;
    }

    public function setNom3(?string $nom3): self
    {
        $this->nom3 = $nom3;

        return $this;
    }

    public function getAdresse1(): ?string
    {
        return $this->adresse1;
    }

    public function setAdresse1(?string $adresse1): self
    {
        $this->adresse1 = $adresse1;

        return $this;
    }

    public function getAdresse2(): ?string
    {
        return $this->adresse2;
    }

    public function setAdresse2(?string $adresse2): self
    {
        $this->adresse2 = $adresse2;

        return $this;
    }

    public function getAdresse3(): ?string
    {
        return $this->adresse3;
    }

    public function setAdresse3(?string $adresse3): self
    {
        $this->adresse3 = $adresse3;

        return $this;
    }

    public function getCodepostal(): ?string
    {
        return $this->codepostal;
    }

    public function setCodepostal(string $codepostal): self
    {
        $this->codepostal = $codepostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getFkenquete(): ?int
    {
        return $this->fkenquete;
    }

    public function setFkenquete(int $fkenquete): self
    {
        $this->fkenquete = $fkenquete;

        return $this;
    }

    public function getAnnee(): ?string
    {
        return $this->annee;
    }

    public function setAnnee(string $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

    public function getEchantillon(): ?string
    {
        return $this->echantillon;
    }

    public function setEchantillon(?string $echantillon): self
    {
        $this->echantillon = $echantillon;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getDtvalidation(): ?\DateTimeInterface
    {
        return $this->dtvalidation;
    }

    public function setDtvalidation(?\DateTimeInterface $dtvalidation): self
    {
        $this->dtvalidation = $dtvalidation;

        return $this;
    }

    public function getNomprenom(): ?string
    {
        return $this->nomprenom;
    }

    public function setNomprenom(?string $nomprenom): self
    {
        $this->nomprenom = $nomprenom;

        return $this;
    }

    public function getCivilite(): ?string
    {
        return $this->civilite;
    }

    public function setCivilite(?string $civilite): self
    {
        $this->civilite = $civilite;

        return $this;
    }

    public function getFonction(): ?string
    {
        return $this->fonction;
    }

    public function setFonction(?string $fonction): self
    {
        $this->fonction = $fonction;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getNbmaternelle(): ?int
    {
        return $this->nbmaternelle;
    }

    public function setNbmaternelle(?int $nbmaternelle): self
    {
        $this->nbmaternelle = $nbmaternelle;

        return $this;
    }

    public function getNbelementaire(): ?int
    {
        return $this->nbelementaire;
    }

    public function setNbelementaire(?int $nbelementaire): self
    {
        $this->nbelementaire = $nbelementaire;

        return $this;
    }

    public function getDtdeb(): ?\DateTimeInterface
    {
        return $this->dtdeb;
    }

    public function setDtdeb(\DateTimeInterface $dtdeb): self
    {
        $this->dtdeb = $dtdeb;

        return $this;
    }

    public function getDtfin(): ?\DateTimeInterface
    {
        return $this->dtfin;
    }

    public function setDtfin(\DateTimeInterface $dtfin): self
    {
        $this->dtfin = $dtfin;

        return $this;
    }

    public function getDtdeb2(): ?\DateTimeInterface
    {
        return $this->dtdeb2;
    }

    public function setDtdeb2(?\DateTimeInterface $dtdeb2): self
    {
        $this->dtdeb2 = $dtdeb2;

        return $this;
    }

    public function getDtfin2(): ?\DateTimeInterface
    {
        return $this->dtfin2;
    }

    public function setDtfin2(?\DateTimeInterface $dtfin2): self
    {
        $this->dtfin2 = $dtfin2;

        return $this;
    }

    public function getDtdeb3(): ?\DateTimeInterface
    {
        return $this->dtdeb3;
    }

    public function setDtdeb3(?\DateTimeInterface $dtdeb3): self
    {
        $this->dtdeb3 = $dtdeb3;

        return $this;
    }

    public function getDtfin3(): ?\DateTimeInterface
    {
        return $this->dtfin3;
    }

    public function setDtfin3(?\DateTimeInterface $dtfin3): self
    {
        $this->dtfin3 = $dtfin3;

        return $this;
    }


}
