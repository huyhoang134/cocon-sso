<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ActualiteHasWtype
 *
 * @ORM\Table(name="actualite_has_wtype", indexes={@ORM\Index(name="fk_actualite_has_wtype_actualite1_idx", columns={"actualite_id"}), @ORM\Index(name="fk_actualite_has_wtype_wtype1_idx", columns={"wtype_id"})})
 * @ORM\Entity
 */
class ActualiteHasWtype
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Actualite
     *
     * @ORM\ManyToOne(targetEntity="Actualite")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="actualite_id", referencedColumnName="id")
     * })
     */
    private $actualite;

    /**
     * @var \Wtype
     *
     * @ORM\ManyToOne(targetEntity="Wtype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="wtype_id", referencedColumnName="id")
     * })
     */
    private $wtype;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getActualite(): ?Actualite
    {
        return $this->actualite;
    }

    public function setActualite(?Actualite $actualite): self
    {
        $this->actualite = $actualite;

        return $this;
    }

    public function getWtype(): ?Wtype
    {
        return $this->wtype;
    }

    public function setWtype(?Wtype $wtype): self
    {
        $this->wtype = $wtype;

        return $this;
    }


}
