<?php

namespace App\Controller;

use App\Entity\Wcontrat;
use App\Services\CustomerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\KernelInterface;


use App\Entity\Wcocon;
use App\Entity\Wdeclar;
use App\Entity\Wtype;
use App\Entity\Wcontact;
use App\Entity\Wadresses;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class InterfaceController extends AbstractController
{
    /**
     * @var Request|null
     */
    private $request;
    /**
     * @var CustomerService
     */
    private $customerService;
    /**
     * @var ParameterBagInterface
     */
    private $params;
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    public function __construct(
        RequestStack $requestStack,
        CustomerService $customerService,
        ParameterBagInterface $params,
        UrlGeneratorInterface $urlGenerator,
        AuthenticationManagerInterface $authManager,
        AuthorizationCheckerInterface $authorizationChecker,
        TokenStorageInterface $tokenStorage
    ) {
        $this->request         = $requestStack->getCurrentRequest();
        $this->customerService = $customerService;
        $this->params          = $params;
        $this->urlGenerator    = $urlGenerator;
        $this->authManager     = $authManager;
        $this->tokenStorage    = $tokenStorage;
        
   
    }

    /**
     * @Route("/dashboard", methods={"GET","POST"}, name="dashboard")
     */
    public function dashboard(Request $request, UserInterface $wcocon= NULL)
    {

        $cookieLogoutChecked = $this->request->get('cookieLogoutChecked', null);
        $cookieChecked       = $this->request->get('cookieChecked', null);

        if (!$cookieChecked) {
            //check cookie
            $postAppKeyParam      = 'appKey=' . $this->customerService->encryptParameter($this->params->get('webapp_key'));
            $postAppSecretParam   = '&appSecret=' . $this->customerService->encryptParameter($this->params->get('webapp_secret'));
            $postUrlBackLinkParam = '&urlBackLink=' . $this->customerService->encryptParameter($this->urlGenerator->generate('dashboard',
                    [], UrlGeneratorInterface::ABSOLUTE_URL));

            return new RedirectResponse($this->params->get('sso_server_cache_saved_check') . '?' . $postAppKeyParam . $postAppSecretParam . $postUrlBackLinkParam,
                Response::HTTP_MOVED_PERMANENTLY);
        }

        if ($cookieLogoutChecked) {
            return $this->redirectToRoute('security_logout');
        }

        //si uniquement delpe, alors pas d'acces a l'espace cocontractant.
        if(empty($wcocon->getWcoDossier()) && !empty($wcocon->getDelpeId())){
            return new RedirectResponse($this->params->get('delpe_server_address') , Response::HTTP_MOVED_PERMANENTLY);
        }

        return $this->render('interface/dashboard.html.twig', [ ]);
    }
    /**
     * @Route("/mes-contrats", methods={"GET","POST"}, name="mes_contrats")
     */
    public function myContracts(Request $request, UserInterface $wcocon= NULL)
    {


        $cookieLogoutChecked = $this->request->get('cookieLogoutChecked', null);
        $cookieChecked       = $this->request->get('cookieChecked', null);

        if (!$cookieChecked) {
            //check cookie
            $postAppKeyParam      = 'appKey=' . $this->customerService->encryptParameter($this->params->get('webapp_key'));
            $postAppSecretParam   = '&appSecret=' . $this->customerService->encryptParameter($this->params->get('webapp_secret'));
            $postUrlBackLinkParam = '&urlBackLink=' . $this->customerService->encryptParameter($this->urlGenerator->generate('mes_contrats',[], UrlGeneratorInterface::ABSOLUTE_URL));

            return new RedirectResponse($this->params->get('sso_server_cache_saved_check') . '?' . $postAppKeyParam . $postAppSecretParam . $postUrlBackLinkParam,
                Response::HTTP_MOVED_PERMANENTLY);
        }

        if ($cookieLogoutChecked) {
            return $this->redirectToRoute('security_logout');
        }

        //si uniquement delpe, alors pas d'acces a l'espace cocontractant.
        if(empty($wcocon->getWcoDossier()) && !empty($wcocon->getDelpeId())){
            return new RedirectResponse($this->params->get('delpe_server_address') , Response::HTTP_MOVED_PERMANENTLY);
        }


        $wdeclar=$wcocon->getWcontrats();
        $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
        $contract=$WcoconRepository->getContract($wcocon->getUsername());

        /*sm($contract['wdeclar']->getWdeEtatDeclar());
        die();*/
        $wtype=$contract['wtype'];
        
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon);
        $declaration_conf_chained = $WcoconRepository->getDeclarationConfiguration($wcocon,false,false);
        $contrats=$WcoconRepository->getListContrats($declaration_conf);
        $contratsChained=$WcoconRepository->getListContratsChained($declaration_conf_chained);
        //on a besoin des contrats indépendant en mode restreint login/password
        $contratsChained_restreint=$WcoconRepository->getListContratsChained($declaration_conf);
        $contratsChained["independant"]=$contratsChained_restreint["independant"];
        
        /*fix pour chainer les boutons lorsque les contrats sont de meme type*/
        $fix_type_contrat=[];
        foreach($contratsChained['independant'] as $i=>$c)
        {
            if($i>0){
                $fix_type_contrat[]=$c->getWconfTypcont()->getId();
            }
        
        }
        
        if(count($contratsChained['independant'])-1>count(array_unique($fix_type_contrat)))
        {
            //on est dans un cas ou les contrats sont les meme donc on les chaines.
            foreach($contratsChained['independant'] as $i=>$c)
            {
                if($i==0){
                    $c->chaine_force=1;
                    $contratsChained['chaine'][]=$c;
                }else{
                    $c->chaine_force=1;
                    $contratsChained['chaine'][]=$c;
                    unset($contratsChained['independant'][$i]);
                }
        
            }
        }
   

        $has_status_p=false;
        foreach($declaration_conf as $c){
            if( $c[0]->type_etape=='declaration' &&  in_array($c[0]->wde_etat_declar, ['P','W'])){
                $has_status_p=true;
            }
            if(  in_array($c[0]->wde_etat_declar, ['W'])){
               // return $this->redirectToRoute('interface_bdc_success');
            }
        }
        
        //si on a qu'un seul contrat chainé, mettons le en contrat indépendant.
        if(count($contratsChained['chaine'])==1){
            
            $contratsChained['independant'][]=$contratsChained['chaine'][0];
            unset($contratsChained['chaine'][0]);
        }

        if($has_status_p){
            return $this->redirectToRoute('interface_bdc');
        }

        if(!empty($wcocon->getDelpeId())){
            $delpe = $this->container->get('doctrine')->getManager('delpe')->createQueryBuilder('c')->select('c');
            $delpe->from(\App\Entity\Delpe::class, 'c');
            $delpe->where('c.pkdelpe  = '.$wcocon->getDelpeId());
            $delpe_declars = $delpe->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        }else{
            $delpe_declars=[];
        }

//dd($wcocon->getWcontrats()[0]);
        return $this->render('interface/my-contracts.html.twig', [
            'wdeclar' => $wcocon->getWcontrats()[0],
            'wtype'   => $wtype,
            'contrats'=> $contrats,
            'contrats_chained'=> $contratsChained,
            'WtypeCont'     => $declaration_conf[0][0]->getWconfTypcont(),
            'delpe_id' => $wcocon->getDelpeId(),
            'delpe_declars' => $delpe_declars,
            'delef_declars' => $wcocon->getWcontrats(),

        ]);

    }
    
    
    /**
     * @Route("/interface/bon-de-commande", methods={"GET","POST"}, name="interface_bdc")
     */
    public function Bdc(Request $request, UserInterface $wcocon = NULL, ParameterBagInterface $params, \Swift_Mailer $mailer)
    {
        /*$cookieLogoutChecked = $this->request->get('cookieLogoutChecked', null);
        $cookieChecked       = $this->request->get('cookieChecked', null);

        if (!$cookieChecked) {
            //check cookie
            $postAppKeyParam      = 'appKey=' . $this->customerService->encryptParameter($this->params->get('webapp_key'));
            $postAppSecretParam   = '&appSecret=' . $this->customerService->encryptParameter($this->params->get('webapp_secret'));
            $postUrlBackLinkParam = '&urlBackLink=' . $this->customerService->encryptParameter($this->urlGenerator->generate('interface_open',
                    [], UrlGeneratorInterface::ABSOLUTE_URL));

            return new RedirectResponse($this->params->get('sso_server_cache_saved_check') . '?' . $postAppKeyParam . $postAppSecretParam . $postUrlBackLinkParam,
                Response::HTTP_MOVED_PERMANENTLY);
        }

        if ($cookieLogoutChecked) {
            return $this->redirectToRoute('security_logout');
        }*/
        

        
        $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
        $contract=$WcoconRepository->getContract($wcocon->getUsername());
        $wtype=$contract['wtype'];

        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon);
        $declaration_conf_chained = $WcoconRepository->getDeclarationConfiguration($wcocon,false,false);
        
        $contrats=$WcoconRepository->getListContrats($declaration_conf);
        $contratsChained=$WcoconRepository->getListContratsChained($declaration_conf_chained);
        
       
        
        //on a besoin des contrats indépendant en mode restreint login/password
        $contratsChained_restreint=$WcoconRepository->getListContratsChained($declaration_conf);
        $contratsChained["independant"]=$contratsChained_restreint["independant"];
        

        
        
        
        
        $has_status_p=false;
        $BDC_list=[];
        $BDC_panorama_info=[];
        foreach($declaration_conf as $c){
            if(in_array($c[0]->wde_etat_declar, ['P', 'W'/**/])){
                if(!$has_status_p){
                    $has_status_p=$c[0];
                }

                foreach($c as $item){
                    if(in_array($item->type_object,['chorus_pro_extended','chorus_pro', 'panorama_press_numerique', 'bon_commande'])){
                        $wde_order_number=$WcoconRepository->getValue("wde_order_number", $c[0]->wde_dossier, $c[0]->wde_declar, $c[0]->wdf_declar);
                       
                        if(in_array($item->type_object,['chorus_pro_extended','chorus_pro', 'bon_commande'])){
                            $BDC_list[]=[
                                'type_object'=>'chorus_pro',
                                'wde_annee'=>$c[0]->wde_annee,
                                'label_annee'=>$c[0]->label_annee,
                                'wde_dossier'=>$c[0]->wde_dossier,
                                'wde_declar'=>$c[0]->wde_declar,
                                'wde_contrat'=>$c[0]->wde_contrat,
                                'wde_etat_declar'=>$c[0]->wde_etat_declar,
                                'label_interface'=>$c[0]->label_interface,
                                'wde_order_number'=>$wde_order_number
                            ];
                            
                        }
                        
                        if(in_array($item->type_object,['panorama_press_numerique']))
                        {
                            $wde_order_number=$WcoconRepository->getValue("panorama_press", $c[0]->wde_dossier, $c[0]->wde_declar, $c[0]->wdf_declar);
                            $panorama_bdc=[];
                            foreach($wde_order_number['wpanoramapress_liste'] as $i=>$ppl){
                                $panorama_bdc[]=[
                                    'wppl_order_number'=>$ppl['wppl_order_number'],
                                    'wppl_id'=>$ppl['wppl_id'],
                                    'wppl_label'=>$ppl['wppl_label'],
                                    'wppl_index'=>$i+1,
                                ];
                                $BDC_panorama_info[$ppl['wppl_id']]=$ppl['wppl_label'];
                            }
                            $BDC_list[]=[
                                'type_object'=>'panorama_press',
                                'wde_annee'=>$c[0]->wde_annee,
                                'label_annee'=>$c[0]->label_annee,
                                'wde_dossier'=>$c[0]->wde_dossier,
                                'wde_declar'=>$c[0]->wde_declar,
                                'wde_contrat'=>$c[0]->wde_contrat,
                                'wde_etat_declar'=>$c[0]->wde_etat_declar,
                                'label_interface'=>$c[0]->label_interface,
                                'wde_order_number'=>$panorama_bdc
                            ];
                            
                        }
                    }
                    
                
                }
                
            }
        }
       
        
        if(empty($has_status_p)){
            return $this->redirectToRoute('dashboard');
        }
      

        
        $wdeclar_id=$has_status_p->wde_declar;
        $has_status_p_wdeclar_object=$WcoconRepository->getDeclarById($wdeclar_id);
        $em=$this->getDoctrine()->getManager();
        
        $submit_data = $request->request->all();
        
        if(!empty($submit_data)){
            if(!empty($submit_data['panorama'])){
                $have_empty_bdc=false;
                $have_some_bdc=false;
                foreach($submit_data['panorama'] as $wdeclar=>$panoramas){
                    
                    foreach($panoramas as $wppl=>$wppl_bdc){
                        if(empty(trim($wppl_bdc)))
                        {
                            $have_empty_bdc=true;
                        }else{
                            $have_some_bdc=true;
                            $WcoconRepository->update_BDC_press_liste($wppl, $wppl_bdc,  $wcocon->getWcoDossier(), $wdeclar);
                        }
                        
                    }
                }
                
                $wdeclar_object=$WcoconRepository->getDeclarById($wdeclar);
               
                if(empty($have_empty_bdc)){
                    $wdeclar_object=$WcoconRepository->getDeclarById($wdeclar);
                    $type_contrat=$wdeclar_object->getWdeTypcont();
                    $mail_content = $this->renderView(
                        'emails/notification_bdc.html.twig', [
                            'wdeclar' => $wdeclar_object,
                            'type_contrat'=>$type_contrat,
                            'panoramas'=>$panoramas,
                            'BDC_panorama_info'=>$BDC_panorama_info,
                        ]
                        );
                    $from_email = $params->get('delef_from_email');
                    $from_name = $params->get('delef_from_name');
                    $wtype = $this->getDoctrine()->getRepository('App\Entity\Wtype')->findOneBy(['label'=>$type_contrat]);
                    $contact_email=$wtype->getContactEmail();
                    //$contact_email="tbourdin@partitech.com";
                    $message = (new \Swift_Message('DELEF réception de bon de commande'))
                    ->setFrom($from_email, $from_name)
                    ->setTo($contact_email);
                    $message->setBody($mail_content, 'text/html');
                    $ret=$mailer->send($message);
                    
                }

                
                if(empty($have_empty_bdc)){
                    
                    $wdeclar_object=$WcoconRepository->getDeclarById($wdeclar);
                    $type_contrat=$wdeclar_object->getWdeTypcont();
                    $wdeclar_object->setWdeEtatDeclar('W');
                    $wdeclar_object->setWdeDateValidation(new \DateTime());
                    $em->persist($wdeclar_object);
                    $em->flush();
 
                }

            }
            
            if(!empty($submit_data['bdc'])){
                foreach($submit_data['bdc'] as $wdeclar=>$bdc){
                    if(!empty(trim($bdc))){
                        $WcoconRepository->update_BDC_declar($bdc,  $wcocon->getWcoDossier(), $wdeclar);
                        
                        
                        $wdeclar_object=$WcoconRepository->getDeclarById($wdeclar);
                        $type_contrat=$wdeclar_object->getWdeTypcont();
                        $mail_content = $this->renderView(
                            'emails/notification_bdc.html.twig', [
                                'wdeclar' => $wdeclar_object,
                                'type_contrat'=>$type_contrat,
                                'bdc'=>$bdc,
                            ]
                            );
                        $from_email = $params->get('delef_from_email');
                        $from_name = $params->get('delef_from_name');
                        $wtype = $this->getDoctrine()->getRepository('App\Entity\Wtype')->findOneBy(['label'=>$type_contrat]);
                        $contact_email=$wtype->getContactEmail();
                        //$contact_email="tbourdin@partitech.com";
                        $message = (new \Swift_Message('DELEF réception de bon de commande'))
                        ->setFrom($from_email, $from_name)
                        ->setTo($contact_email)
                        ->setBody($mail_content, 'text/html');
                        $ret=$mailer->send($message);
                        
                        $wdeclar_object=$WcoconRepository->getDeclarById($wdeclar);
                        $type_contrat=$wdeclar_object->getWdeTypcont();
                        $wdeclar_object->setWdeEtatDeclar('W');
                        $wdeclar_object->setWdeDateValidation(new \DateTime());
                        $em->persist($wdeclar_object);
                        $em->flush();
                    }
                    
                    
                    
                    
                    
                }
            }
            //return $this->redirectToRoute('interface_bdc_success');
            return $this->redirectToRoute('interface_bdc');
        }
        

        
        return $this->render('interface/invoice/invoice_validate.html.twig', [
            'wdeclar' => $wdeclar,
            'wtype'   => $wtype,
            
            'contrats'=> $contrats,
            'WtypeCont'     => $declaration_conf[0][0]->getWconfTypcont(),
            'status_p_label' => $has_status_p->label_interface,
            'status_p_contrat' => $has_status_p->wde_contrat,
            'contrats_chained'=> $contratsChained,
            'BDC_list'=>$BDC_list
            
        ]);
        
    }
    
    /**
     * @Route("/interface/bon-de-commande-confirme", methods={"GET","POST"}, name="interface_bdc_success")
     */
    public function BdcSuccess(Request $request, UserInterface $wcocon = NULL)
    {
        /*$cookieLogoutChecked = $this->request->get('cookieLogoutChecked', null);
        $cookieChecked       = $this->request->get('cookieChecked', null);

        if (!$cookieChecked) {
            //check cookie
            $postAppKeyParam      = 'appKey=' . $this->customerService->encryptParameter($this->params->get('webapp_key'));
            $postAppSecretParam   = '&appSecret=' . $this->customerService->encryptParameter($this->params->get('webapp_secret'));
            $postUrlBackLinkParam = '&urlBackLink=' . $this->customerService->encryptParameter($this->urlGenerator->generate('interface_open',
                    [], UrlGeneratorInterface::ABSOLUTE_URL));

            return new RedirectResponse($this->params->get('sso_server_cache_saved_check') . '?' . $postAppKeyParam . $postAppSecretParam . $postUrlBackLinkParam,
                Response::HTTP_MOVED_PERMANENTLY);
        }

        if ($cookieLogoutChecked) {
            return $this->redirectToRoute('security_logout');
        }
    */
        $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
        $contract=$WcoconRepository->getContract($wcocon->getUsername());
        $wtype=$contract['wtype'];
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon);
        $contrats=$WcoconRepository->getListContrats($declaration_conf);
        
        return $this->render('interface/invoice/invoice_validate_success.html.twig', [
            'wdeclar' => $wdeclar,
            'wtype'   => $wtype,
            'contrats'=> $contrats,
            'WtypeCont'     => $declaration_conf[0][0]->getWconfTypcont()
        ]);
    }
    
    
    /**
     * @Route("/interface-ferme", methods={"GET","POST"}, name="interface_closed")
     */
    public function InterfaceClosed(Request $request, UserInterface $wcocon = NULL)
    {
        /*$cookieLogoutChecked = $this->request->get('cookieLogoutChecked', null);
        $cookieChecked       = $this->request->get('cookieChecked', null);

        if (!$cookieChecked) {
            //check cookie
            $postAppKeyParam      = 'appKey=' . $this->customerService->encryptParameter($this->params->get('webapp_key'));
            $postAppSecretParam   = '&appSecret=' . $this->customerService->encryptParameter($this->params->get('webapp_secret'));
            $postUrlBackLinkParam = '&urlBackLink=' . $this->customerService->encryptParameter($this->urlGenerator->generate('interface_open',
                    [], UrlGeneratorInterface::ABSOLUTE_URL));

            return new RedirectResponse($this->params->get('sso_server_cache_saved_check') . '?' . $postAppKeyParam . $postAppSecretParam . $postUrlBackLinkParam,
                Response::HTTP_MOVED_PERMANENTLY);
        }

        if ($cookieLogoutChecked) {
            return $this->redirectToRoute('security_logout');
        }*/

        $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
        $contract=$WcoconRepository->getContract($wcocon->getUsername());
        $wtype=$contract['wtype'];

        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon);
        $contrats=$WcoconRepository->getListContrats($declaration_conf);
        $primary_declaration = $this->getDoctrine()->getRepository(Wdeclar::class)->findOneBy(["wde_declar" => $declaration_conf[1][0]->wde_declar]);
        

        return $this->render('interface/interface-closed.html.twig', [
            'wdeclar'  => $wdeclar,
            'wtype'    => $wtype,
            'contrats' => $contrats,
            'WtypeCont'     => $declaration_conf[0][0]->getWconfTypcont(),
            'primary_declaration'=>$primary_declaration,
        ]);
    }
    
    
    
    /**
     * @Route("/interface/modifier-vos-coordonnees", methods={"GET","POST"}, name="interface_closed_coordonnees")
     */
    public function ModifyCoordinate(Request $request, UserInterface $wcocon = NULL)
    {
        
        /*$cookieLogoutChecked = $this->request->get('cookieLogoutChecked', null);
        $cookieChecked       = $this->request->get('cookieChecked', null);

        if (!$cookieChecked) {
            //check cookie
            $postAppKeyParam      = 'appKey=' . $this->customerService->encryptParameter($this->params->get('webapp_key'));
            $postAppSecretParam   = '&appSecret=' . $this->customerService->encryptParameter($this->params->get('webapp_secret'));
            $postUrlBackLinkParam = '&urlBackLink=' . $this->customerService->encryptParameter($this->urlGenerator->generate('interface_open',
                    [], UrlGeneratorInterface::ABSOLUTE_URL));

            return new RedirectResponse($this->params->get('sso_server_cache_saved_check') . '?' . $postAppKeyParam . $postAppSecretParam . $postUrlBackLinkParam,
                Response::HTTP_MOVED_PERMANENTLY);
        }

        if ($cookieLogoutChecked) {
            return $this->redirectToRoute('security_logout');
        }*/


        $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
        $contract=$WcoconRepository->getContract($wcocon->getUsername());
        $wtype=$contract['wtype'];
        
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon);
        $primary_declaration = $this->getDoctrine()->getRepository(Wcontrat::class)->findOneBy(["wct_contrat" => $declaration_conf[1][0]->wde_declar]);
        
        $f = $this->createFormBuilder([]);
        $wde_dossier = $declaration_conf[0][0]->wde_dossier;
        $wde_declar = $declaration_conf[0][0]->wde_declar;
        $wdf_declar = $declaration_conf[0][0]->wdf_declar;
        $submit_data = $request->request->all();
        $submit_data = $request->request->all();
        
        //on enleve les éléments qui ne sont pas affiché en modif de coordonée de l'espace fermé
        $declaration_conf_ferme=array();
        foreach($declaration_conf[0] as $i=>$c){
            if($c->getAffichageEspaceFerme()){
                $declaration_conf_ferme[]=$c;
            }
       
        }
        
        foreach($declaration_conf_ferme as $i=>$c){
       
            $db_data[$c->getSynchroField()]=$WcoconRepository->getValue($c->getSynchroField(), $wde_dossier, $wde_declar, $wdf_declar);
            $object_conf=$c->getTypeObjetConf();
            //populate data with db or submited data
            if (!empty($c->getSynchroField())) {
                $data=empty($submit_data)?$db_data[$c->getSynchroField()]:@$submit_data[$c->getSynchroField()];
            }
            
        
            if(in_array($c->getTypeObjet(), ['text_max_25', "text_max_50", 'text_max_40', 'text_max_80', 'text_max_100', 'email','telephone','list','civilite','checkbox'])){
                $f->add($c->getSynchroField(), TextType::class, ['data'=>$data, 'required' => $c->getMandatory()]);
                    if(!empty($object_conf['contact_field_id'])){
                        $db_data[$object_conf['contact_field_id']]=$WcoconRepository->getValue($object_conf['contact_field_id'], $c->wde_dossier, $c->wde_declar, $wdf_declar);
                        $db_data[$object_conf['contact_field_id'].'_question']=$WcoconRepository->getValue($object_conf['contact_field_id'], $c->wde_dossier, $c->wde_declar, $wdf_declar);
                }
                if(!empty($object_conf['adress_field_id'])){
                    $db_data[$object_conf['adress_field_id']]=$WcoconRepository->getValue($object_conf['adress_field_id'], $c->wde_dossier, $c->wde_declar, $wdf_declar);
                    $db_data[$object_conf['adress_field_id'].'_question']=$WcoconRepository->getValue($object_conf['adress_field_id'], $c->wde_dossier, $c->wde_declar, $wdf_declar);
                }
            
            }
        
        
            if(in_array($c->getTypeObjet(), ['num_2', "num_3", 'num_4', 'num_5','num_6','num_7', 'zipcode', 'title_num_5','title_num_6','title_num_7','subtitle_num_5','subtitle_num_7'])){
                if(empty($data)){
                    $data=null;
                }
                $f->add($c->getSynchroField(), IntegerType::class, ['data'=>$data, 'required' => $c->getMandatory()]);
            }
            if(in_array($c->getTypeObjet(), ['zipcode'])){
                if(empty($data)){
                    $data=null;
                }
                $f->add($c->getSynchroField(), TextType::class, ['data'=>$data, 'required' => $c->getMandatory()]);
            }
        }
        
        $f->add('submit', SubmitType::class, ['label' => 'VALIDER']);
        $form = $f->getForm();
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $submit_data = $request->request->all();
            $WcoconRepository->updateForm($submit_data, $declaration_conf_ferme);
            return $this->redirectToRoute('interface_closed_coordonnees_success');
        }

        $contrats=$WcoconRepository->getListContrats($declaration_conf);
        return $this->render('interface/declaration/modification-coordinate.html.twig', [
                    'db_data'    => $db_data,
                    'primary_declaration'=>$primary_declaration,
                    'wcocon' => $wcocon,
                    'WcoconRepository' => $WcoconRepository,
                    'conf'      => $declaration_conf_ferme,
                    'form'      => $form->createView(),
                    'contrats'  => $contrats,
                    'WtypeCont'     => $declaration_conf_ferme[0]->getWconfTypcont(),
                    /*'wdeclar' => $wdeclar,*/
                    'contacts' =>  $this->getDoctrine()->getRepository(Wcontact::class)->findBy(["fkcocontractant" => $declaration_conf[1][0]->wde_dossier, 'actif'=>'O']),
                    'adresses' =>  $this->getDoctrine()->getRepository(Wadresses::class)->findBy(["fkcocontractant" => $wde_dossier /*, 'actif'=>'O'*/])

        ]);
    }
    
    
    /**
     * @Route("/interface/modifier-vos-coordonnees-success", methods={"GET"}, name="interface_closed_coordonnees_success")
     */
    public function ModifyCoordinateSuccess(Request $request, UserInterface $wcocon = NULL)
    {
        /*$cookieLogoutChecked = $this->request->get('cookieLogoutChecked', null);
        $cookieChecked       = $this->request->get('cookieChecked', null);

        if (!$cookieChecked) {
            //check cookie
            $postAppKeyParam      = 'appKey=' . $this->customerService->encryptParameter($this->params->get('webapp_key'));
            $postAppSecretParam   = '&appSecret=' . $this->customerService->encryptParameter($this->params->get('webapp_secret'));
            $postUrlBackLinkParam = '&urlBackLink=' . $this->customerService->encryptParameter($this->urlGenerator->generate('interface_open',
                    [], UrlGeneratorInterface::ABSOLUTE_URL));

            return new RedirectResponse($this->params->get('sso_server_cache_saved_check') . '?' . $postAppKeyParam . $postAppSecretParam . $postUrlBackLinkParam,
                Response::HTTP_MOVED_PERMANENTLY);
        }

        if ($cookieLogoutChecked) {
            return $this->redirectToRoute('security_logout');
        }
        */
        $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
        $contract=$WcoconRepository->getContract($wcocon->getUsername());
        $wtype=$contract['wtype'];

        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon);
        $primary_declaration = $this->getDoctrine()->getRepository(Wcontrat::class)->findOneBy(["wct_contrat" => $declaration_conf[1][0]->wde_declar]);

        $contrats=$WcoconRepository->getListContrats($declaration_conf);
        return $this->render('interface/declaration/modification-coordinate-success.html.twig', [
            'primary_declaration'=>$primary_declaration,
            'wcocon' => $wcocon,
            'WcoconRepository' => $WcoconRepository,
            //'conf'      => $declaration_conf[0],
            //'form'      => $form->createView(),
            'contrats'  => $contrats,
            'WtypeCont'     => $declaration_conf[0][0]->getWconfTypcont(),
            /*'wdeclar' => $wdeclar,*/
        ]);
        
    }

    /**
     * @Route("/interface/modifier-vos-coordonnees-de-facturation/{wtype_id}", requirements={"wtype_id"="\d+","step"="\d+"}, methods={"GET","POST"}, name="modify_billing_address")
     */
    public function ModifyBillingAddress(Request $request, UserInterface $wcontrat = NULL, $wtype_id )
    {

        /*$cookieLogoutChecked = $this->request->get('cookieLogoutChecked', null);
        $cookieChecked       = $this->request->get('cookieChecked', null);

        if (!$cookieChecked) {
            //check cookie
            $postAppKeyParam      = 'appKey=' . $this->customerService->encryptParameter($this->params->get('webapp_key'));
            $postAppSecretParam   = '&appSecret=' . $this->customerService->encryptParameter($this->params->get('webapp_secret'));
            $postUrlBackLinkParam = '&urlBackLink=' . $this->customerService->encryptParameter($this->urlGenerator->generate('interface_open',
                    [], UrlGeneratorInterface::ABSOLUTE_URL));

            return new RedirectResponse($this->params->get('sso_server_cache_saved_check') . '?' . $postAppKeyParam . $postAppSecretParam . $postUrlBackLinkParam,
                Response::HTTP_MOVED_PERMANENTLY);
        }

        if ($cookieLogoutChecked) {
            return $this->redirectToRoute('security_logout');
        }*/

        if(empty($wtype_id)){
            return $this->redirectToRoute('dashboard');
        }

        $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
        $declaration_configuration=$WcoconRepository->getDeclarationConfiguration($wcontrat, $wtype_id);

        //on enleve les éléments qui ne sont pas affiché en modif de facturation
        $declaration_conf_facturation=array();
        foreach($declaration_configuration[0] as $i=>$c){
            if($c->getFacturation()){
                $declaration_conf_facturation[]=$c;
            }

        }


        $contract=$WcoconRepository->getContract($wcontrat->getUsername());




        $wtype=$contract['wtype'];

        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcontrat);
        $primary_declaration = $this->getDoctrine()->getRepository(Wcontrat::class)->findOneBy(["wct_contrat" => $declaration_conf[1][0]->wde_declar]);

        $f = $this->createFormBuilder([]);
        $wde_dossier = $declaration_conf[0][0]->wde_dossier;
        $wde_declar = $declaration_conf[0][0]->wde_declar;
        $wdf_declar = $declaration_conf[0][0]->wdf_declar;
        $submit_data = $request->request->all();
        $submit_data = $request->request->all();

        //on enleve les éléments qui ne sont pas affiché en modif de coordonée de l'espace fermé
       /* $declaration_conf_ferme=array();
        foreach($declaration_conf[0] as $i=>$c){
            if($c->getAffichageEspaceFerme()){
                $declaration_conf_ferme[]=$c;
            }

        }*/

        foreach($declaration_conf_facturation as $i=>$c){

            $db_data[$c->getSynchroField()]=$WcoconRepository->getValue($c->getSynchroField(), $wde_dossier, $wde_declar, $wdf_declar);
            $object_conf=$c->getTypeObjetConf();
            //populate data with db or submited data
            if (!empty($c->getSynchroField())) {
                $data=empty($submit_data)?$db_data[$c->getSynchroField()]:@$submit_data[$c->getSynchroField()];
            }


            if(in_array($c->getTypeObjet(), ['text_max_25', "text_max_50", 'text_max_40', 'text_max_80', 'text_max_100', 'email','telephone','list','civilite','checkbox'])){
                $f->add($c->getSynchroField(), TextType::class, ['data'=>$data, 'required' => $c->getMandatory()]);
                if(!empty($object_conf['contact_field_id'])){
                    $db_data[$object_conf['contact_field_id']]=$WcoconRepository->getValue($object_conf['contact_field_id'], $c->wde_dossier, $c->wde_declar, $wdf_declar);
                    $db_data[$object_conf['contact_field_id'].'_question']=$WcoconRepository->getValue($object_conf['contact_field_id'], $c->wde_dossier, $c->wde_declar, $wdf_declar);
                }
                if(!empty($object_conf['adress_field_id'])){
                    $db_data[$object_conf['adress_field_id']]=$WcoconRepository->getValue($object_conf['adress_field_id'], $c->wde_dossier, $c->wde_declar, $wdf_declar);
                    $db_data[$object_conf['adress_field_id'].'_question']=$WcoconRepository->getValue($object_conf['adress_field_id'], $c->wde_dossier, $c->wde_declar, $wdf_declar);
                }

            }


            if(in_array($c->getTypeObjet(), ['num_2', "num_3", 'num_4', 'num_5','num_6','num_7', 'zipcode', 'title_num_5','title_num_6','title_num_7','subtitle_num_5','subtitle_num_7'])){
                if(empty($data)){
                    $data=null;
                }
                $f->add($c->getSynchroField(), IntegerType::class, ['data'=>$data, 'required' => $c->getMandatory()]);
            }
            if(in_array($c->getTypeObjet(), ['zipcode'])){
                if(empty($data)){
                    $data=null;
                }
                $f->add($c->getSynchroField(), TextType::class, ['data'=>$data, 'required' => $c->getMandatory()]);
            }
        }

        $f->add('submit', SubmitType::class, ['label' => 'VALIDER']);
        $form = $f->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $submit_data = $request->request->all();
            $WcoconRepository->updateForm($submit_data, $declaration_conf_facturation);
            return $this->redirectToRoute('modify_billing_address_success');
        }

        $contrats=$WcoconRepository->getListContrats($declaration_conf);
        return $this->render('interface/declaration/modification-billing.html.twig', [
            'db_data'    => $db_data,
            'primary_declaration'=>$primary_declaration,
            'wcocon' => $wcontrat,
            'WcoconRepository' => $WcoconRepository,
            'conf'      => $declaration_conf_facturation,
            'form'      => $form->createView(),
            'contrats'  => $contrats,
            'WtypeCont'     => $declaration_conf_facturation[0]->getWconfTypcont(),
            /*'wdeclar' => $wdeclar,*/
            'contacts' =>  $this->getDoctrine()->getRepository(Wcontact::class)->findBy(["fkcocontractant" => $declaration_conf[1][0]->wde_dossier, 'actif'=>'O']),
            'adresses' =>  $this->getDoctrine()->getRepository(Wadresses::class)->findBy(["fkcocontractant" => $wde_dossier /*, 'actif'=>'O'*/])

        ]);
    }


    /**
     * @Route("/interface/modifier-vos-coordonnees-success", methods={"GET"}, name="modify_billing_address_success")
     */
    public function ModifyBillingAddressSuccess(Request $request, UserInterface $wcocon = NULL)
    {
        /*$cookieLogoutChecked = $this->request->get('cookieLogoutChecked', null);
        $cookieChecked       = $this->request->get('cookieChecked', null);

        if (!$cookieChecked) {
            //check cookie
            $postAppKeyParam      = 'appKey=' . $this->customerService->encryptParameter($this->params->get('webapp_key'));
            $postAppSecretParam   = '&appSecret=' . $this->customerService->encryptParameter($this->params->get('webapp_secret'));
            $postUrlBackLinkParam = '&urlBackLink=' . $this->customerService->encryptParameter($this->urlGenerator->generate('interface_open',
                    [], UrlGeneratorInterface::ABSOLUTE_URL));

            return new RedirectResponse($this->params->get('sso_server_cache_saved_check') . '?' . $postAppKeyParam . $postAppSecretParam . $postUrlBackLinkParam,
                Response::HTTP_MOVED_PERMANENTLY);
        }

        if ($cookieLogoutChecked) {
            return $this->redirectToRoute('security_logout');
        }
        */
        $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
        $contract=$WcoconRepository->getContract($wcocon->getUsername());
        $wtype=$contract['wtype'];

        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon);
        $primary_declaration = $this->getDoctrine()->getRepository(Wcontrat::class)->findOneBy(["wct_contrat" => $declaration_conf[1][0]->wde_declar]);

        $contrats=$WcoconRepository->getListContrats($declaration_conf);
        return $this->render('interface/declaration/modification-coordinate-success.html.twig', [
            'primary_declaration'=>$primary_declaration,
            'wcocon' => $wcocon,
            'WcoconRepository' => $WcoconRepository,
            //'conf'      => $declaration_conf[0],
            //'form'      => $form->createView(),
            'contrats'  => $contrats,
            'WtypeCont'     => $declaration_conf[0][0]->getWconfTypcont(),
            /*'wdeclar' => $wdeclar,*/
        ]);

    }









    private function getHistoryFiles2($contrat, $wde_etat_declar, $wde_annee, $type_contrat , $kernel){
    
        $projectRoot = $kernel->getProjectDir()."/public/pdf";
        $status = strtoupper($wde_etat_declar);
        $type = str_replace(' / ', '', $type_contrat);
    
        $arr = $this->list_dir($projectRoot, $type, $contrat, $status, $wde_annee);
        krsort($arr);
        $i=0;
        foreach($arr as $year=>$file){
            if($i>=5){
                unset($arr[$year]);
            }
            $i++;
        }
        return $arr;
    }
    
    
    /**
     * @Route("/interface/consulter-votre-declaration", methods={"GET","POST"}, name="interface_history")
     */
    public function InterfaceHistory(Request $request, UserInterface $wcocon = NULL,  KernelInterface $kernel)
    {
        /*$cookieLogoutChecked = $this->request->get('cookieLogoutChecked', null);
        $cookieChecked       = $this->request->get('cookieChecked', null);

        if (!$cookieChecked) {
            //check cookie
            $postAppKeyParam      = 'appKey=' . $this->customerService->encryptParameter($this->params->get('webapp_key'));
            $postAppSecretParam   = '&appSecret=' . $this->customerService->encryptParameter($this->params->get('webapp_secret'));
            $postUrlBackLinkParam = '&urlBackLink=' . $this->customerService->encryptParameter($this->urlGenerator->generate('interface_open',
                    [], UrlGeneratorInterface::ABSOLUTE_URL));

            return new RedirectResponse($this->params->get('sso_server_cache_saved_check') . '?' . $postAppKeyParam . $postAppSecretParam . $postUrlBackLinkParam,
                Response::HTTP_MOVED_PERMANENTLY);
        }

        if ($cookieLogoutChecked) {
            return $this->redirectToRoute('interface_history');
        }*/
        
        $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
        $contract=$WcoconRepository->getContract($wcocon->getUsername());
        $wtype=$contract['wtype'];

        $history=[];
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon);
        foreach($declaration_conf as $d){

            $history[$d[0]->wde_contrat]=[
                'conf'=>$d[0],
                'wtype'=>$d[0]->wtype
            ];
        }
        
        
        foreach($history as $i=>$h){
           $history_list =$this->getHistoryFiles2(
                $h['conf']->wde_contrat,
                $h['conf']->wde_etat_declar,
                $h['conf']->wde_annee,
                $h['wtype']->getLabel() , $kernel);
                
           if(!empty($history_list)){
               if($h['wtype']->getArchive()==1)
               {
                   $history[$i]['list']=[array_key_first($history_list)=>$history_list[array_key_first($history_list)]];
               }else{
                   $history[$i]['list'] = $history_list;
               }
           }else{
               $history[$i]['list']=[];
           }

            
        }
        $primary_declaration = $this->getDoctrine()->getRepository(Wdeclar::class)->findOneBy(["wde_declar" => $declaration_conf[1][0]->wde_declar]);
        $WtypeCont=$declaration_conf[0][0]->getWconfTypcont();

        $contrats=$WcoconRepository->getListContrats($declaration_conf);
        return $this->render('interface/interface-history.html.twig', [
            'primary_declaration'=>$primary_declaration,
            'wdeclar' => $wdeclar,
            'wtype'   => $wtype,
            'contrats'  => $contrats,
            'WtypeCont' => $WtypeCont,
            'history' => $history
        ]);
    }
    
    /**
     * @Route("/interface/historiques/tous", name="interface_history_download_all")
     */
    public function InterfaceHistoryDownloadAll(Request $request, UserInterface $wcocon = NULL,  KernelInterface $kernel /*int $id, MediaManager $mediaManager, FileProvider $provider, KernelInterface $kernel*/)
    {
        $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon);
        $primary_declaration = $this->getDoctrine()->getRepository(Wdeclar::class)->findOneBy(["wde_declar" => $declaration_conf[1][0]->wde_declar]);
        $WtypeCont=$declaration_conf[0][0]->getWconfTypcont();
        $contrat = $primary_declaration->getWdeContrat();
        
        $history=[];
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon);
        foreach($declaration_conf as $d){
        
            $history[$d[0]->wde_contrat]=[
                'conf'=>$d[0],
                'wtype'=>$d[0]->wtype
            ];
        }
        foreach($history as $i=>$h){
        
            $history[$i]['list']=$this->getHistoryFiles2(
                $h['conf']->wde_contrat,
                $h['conf']->wde_etat_declar,
                $h['conf']->wde_annee,
                $h['wtype']->getLabel() , $kernel);
        }
        
        
        $temp_file= $kernel->getProjectDir()."/var/cache/".$_SERVER['APP_ENV']."/tmp_".$contrat.".zip";
        $zip = new \ZipArchive();
        
        if ($zip->open($temp_file, \ZipArchive::CREATE) === TRUE) {
            
            
            foreach($history as $contrat_id=>$c){
                foreach($c['list'] as $year=>$file){
                    $filename=basename($file);
                    $filename=str_replace('.pdf', '_'.$year.'.pdf',$filename);
                     $zip->addFile($file, $filename);
                }
            }
            
            $zip->close();

            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=\"archive.zip\"");
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: " . filesize($temp_file));
            readfile($temp_file);
            exit;
        }

        return $this->redirectToRoute('index');
    }
    
    /**
     * @Route("/interface/historiques/{year}/{contrat}/", requirements={"year"="(.*)","contrat"="\d+"}, name="interface_history_download")
     */
    public function InterfaceHistoryDownload($year=false, $contrat=false, Request $request, UserInterface $wcocon = NULL,  KernelInterface $kernel /*int $id, MediaManager $mediaManager, FileProvider $provider, KernelInterface $kernel*/)
    {
        $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon);
        $primary_declaration = $this->getDoctrine()->getRepository(Wdeclar::class)->findOneBy(["wde_declar" => $declaration_conf[1][0]->wde_declar]);
        $WtypeCont=$declaration_conf[0][0]->getWconfTypcont();

        
        $history=[];
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon);
        foreach($declaration_conf as $d){
        
            $history[$d[0]->wde_contrat]=[
                'conf'=>$d[0],
                'wtype'=>$d[0]->wtype
            ];
        }
        foreach($history as $i=>$h){
        
            $history[$i]['list']=$this->getHistoryFiles2(
                $h['conf']->wde_contrat,
                $h['conf']->wde_etat_declar,
                $h['conf']->wde_annee,
                $h['wtype']->getLabel() , $kernel);
        }

        if(empty($history[$contrat])){
            return $this->redirectToRoute('index');
        }else{
            if(empty($history[$contrat]['list'][$year])){
                return $this->redirectToRoute('index');
            }else{
                $filename=basename($history[$contrat]['list'][$year]);
                $filename=str_replace('.pdf', '_'.$year.'.pdf',$filename);
                header("Pragma: public");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Cache-Control: public");
                header("Content-Description: File Transfer");
                header("Content-type: application/octet-stream");
                header("Content-Disposition: attachment; filename=\"".$filename."\"");
                header("Content-Transfer-Encoding: binary");
                header("Content-Length: " . filesize($history[$contrat]['list'][$year]));
                readfile($history[$contrat]['list'][$year]);
                exit;
            }
        }
        
        return $this->redirectToRoute('index');
    }
    

    /**
     * @Route("/interface/historiques/derniere", name="interface_history_download_last")
     */
    public function InterfaceHistoryDownloadLast(Request $request, UserInterface $wcocon = NULL,  KernelInterface $kernel )
    {
        $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon);
        $primary_declaration = $this->getDoctrine()->getRepository(Wdeclar::class)->findOneBy(["wde_declar" => $declaration_conf[1][0]->wde_declar]);
        $WtypeCont=$declaration_conf[0][0]->getWconfTypcont();
        $contrat = $primary_declaration->getWdeContrat();
    
        $arr = $this->getHistoryFiles($primary_declaration, $WtypeCont , $kernel);

        if (!empty($arr)) {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=\"declaration_".array_key_first($arr).".pdf\"");
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: " . filesize(reset($arr)));
            readfile(reset($arr));
            exit;
        }
    
        return $this->redirectToRoute('index');
    }
    
    /**
     * @Route("/interface/contact", methods={"GET","POST"}, name="interface_contact")
     */
    public function InterfaceContact(Request $request, UserInterface $wcocon = NULL, ParameterBagInterface $params, \Swift_Mailer $mailer)
    {
        $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
        $contract=$WcoconRepository->getContract($wcocon->getUsername());
        $wtype=$contract['wtype'];
        
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon);
        $primary_declaration = $this->getDoctrine()->getRepository(Wdeclar::class)->findOneBy(["wde_declar" => $declaration_conf[1][0]->wde_declar]);
        $contrats=$WcoconRepository->getListContrats($declaration_conf);

        $submit_data = $request->request->all();

        $f = $this->createFormBuilder([]);
        
        $f->add('civilite', ChoiceType::class, [
            'choices' => [
                'Madame' => 'Madame',
                'Monsieur' => 'Monsieur',
            ],
            'label' => 'Civilité',
            'required' => true,
            'expanded' => true
        ]
            )
            ->add('nom', TextType::class, [
                'label' => 'Nom',
                'required' => true
            ])
            ->add('fonction', TextType::class, [
                'label' => 'Fonction'
            ])
            ->add('telephone', TelType::class, [
                'label' => 'Téléphone'
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'required' => true
            ])
            
            ->add('etablissement', TextType::class, [
                'label' => 'Etablissement',
                'required' => true,
                'data' => !empty($submit_data['form'])?$submit_data['form']['etablissement']:$wcocon->getWcoRaisoc1().' '.$wcocon->getWcoRaisoc2()
            ])
            
            ->add('code_postal', IntegerType::class, [
                'label' => 'CODE POSTAL',
                'required' => true,
                'attr' => ['maxlength' => '5','min' => '1','maxlength' => '99999'],
                'data' => !empty($submit_data['form'])?$submit_data['form']['code_postal']:$wcocon->getWcoCopos()
            ])
            ->add('ville', TextType::class, [
                'label' => 'VILLE',
                'required' => true,
                'data' => !empty($submit_data['form'])?$submit_data['form']['ville']:$wcocon->getWcoVille()
            ])

            ->add('sujet', TextType::class, [
                'label' => 'Sujet',
                'required' => true
            ])
            
            ->add('message', TextareaType::class, [
                'label' => 'Message',
                'required' => true,
                'attr' =>["rows"=>"6", "cols"=>"60"]
            ])
            
            ->add('submit', SubmitType::class, [
                'label' => 'Envoyer'
            ]);
            
            $form = $f->getForm();
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $submit_data = $request->request->all();
            
            $from_email = $params->get('delef_from_email');
            $from_name = $params->get('delef_from_name');
            
            $data = $form->getData();
            
            $mail_content = $this->renderView('emails/contact.html.twig', [
                'data' => $data
            ]);
            
            $recipients = $wtype->getContactEmail();
            //$recipients = 'tbourdin@partitech.com';
            $message = (new \Swift_Message('Extranet CFC - Contact - '.$data['sujet']))
            ->setFrom($from_email, $from_name)
            ->setTo($recipients)
            ->setBody($mail_content, 'text/html');
            
            $mailer->send($message);
            return $this->render('interface/interface-contact-success.html.twig', [
                'primary_declaration'=>$primary_declaration,
                'wdeclar' => $wdeclar,
                'wtype'   => $wtype,
                'contrats'  => $contrats,
                'WtypeCont'     => $declaration_conf[0][0]->getWconfTypcont()
            ]);

        }
        
        
        
        return $this->render('interface/interface-contact.html.twig', [
            'primary_declaration'=>$primary_declaration,
            'wdeclar' => $wdeclar,
            'wtype'   => $wtype,
            'contrats'  => $contrats,
            'form'  => $form->createView(),
            'WtypeCont'     => $declaration_conf[0][0]->getWconfTypcont()
        ]);
    }
       
    private function getHistoryFiles($primary_declaration, $WtypeCont , $kernel){
    
        $name = $projectRoot = $kernel->getProjectDir()."/public/pdf";
        $contrat = $primary_declaration->getWdeContrat();
         
        $status = strtoupper($primary_declaration->getWdeEtatDeclar());
        $annee = $primary_declaration->getWdeAnnee();
        $type = str_replace(' / ', '', $WtypeCont->getLabel());
    
        $arr = $this->list_dir($name, $type, $contrat, $status, $annee);
        krsort($arr);
        $i=0;
        foreach($arr as $year=>$file){
            if($i>=5){
                unset($arr[$year]);
            }
            $i++;
        }
        return $arr;
    }
    
    private function list_dir($name, $type, $contrat, $status, $annee, $arr = array())
    {

        $type = str_replace(' / ', '', $type);
        $type = str_replace('/', '', $type);
        $type = str_replace(' ', '', $type);
        if ($dir = opendir($name)) {
            while ($file = readdir($dir)) {
                if (in_array($file, array(".", "..")))
                    continue;
    
                    if (is_dir($name . '/' . $file)) {
                        $arr = $this->list_dir($name . '/' . $file, $type, $contrat, $status, $annee, $arr);
                    } elseif ($file == $type . '_' . $contrat . '.pdf') {
                        $expl=explode('/', $name);
                        $key = end($expl);
    
                        if ($status == 'V' && $annee == $key)
                            continue;
                            $arr[$key] = $name . '/' . $file;
                    }
            }
            closedir($dir);
        }
        return $arr;
    }
}
