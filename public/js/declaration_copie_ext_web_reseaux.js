var copie_ext_web_structure="";
var copie_ext_web_structure_default="";
jQuery(document).ready(function() {
	

	
	init_copie_externe_web_autocomplete();
	init_copie_externe_web_pub_autocomplete();
	init_copie_externe_web_dob();
	close_copie_ext_web_reseau();
	display_default_add_copie_externe_web_button();

	

    setTimeout(function () {
    	copie_ext_web_structure_default=$('#container_copie_ext_web_reseau > div').eq($('#container_copie_ext_web_reseau > div').length-1).clone();
    }, 1000);

	
	$('form').submit(function( event ) {
		if($('.copie_externe_web').length){

			//si formulaire vide
			if($('.copie_externe_web').length<2){
				var data_index=0;
				var copie_publication=$('.copie_externe_web').eq(data_index).find('.copie_externe_publication').eq(0).val().trim();
				var copie_titre=$('.copie_externe_web').eq(data_index).find('.copie_externe_titre').eq(0).val().trim();
				var copie_auteur=$('.copie_externe_web').eq(data_index).find('.copie_externe_auteur').eq(0).val().trim();
				var copie_parution=$('.copie_externe_web').eq(data_index).find('.copie_externe_parution').eq(0).val().trim();
				var copie_date_mel=$('.copie_externe_web').eq(data_index).find('.copie_externe_date_mel').eq(0).val().trim();
				
				if(copie_publication.length==0 && copie_titre.length==0 && copie_auteur.length==0 && copie_parution.length==0 && copie_date_mel.length==0 ){
					if(confirm("Vous n'avez pas déclaré de publications. Confirmez vous cette déclaration ?"))
					{
						$('.copie_externe_web').eq(0).remove();
						if($('#container_copie_ext_web_reseau > div').length<1){
							$("#default_copie_externe_web_button").show();
						}else{
							$("#default_copie_externe_web_button").hide();
						}
						
					}
				}
	
			
			}
			
			$('.copie_externe_web').each(function(){
				var data_index=$(this).data('index');
				$(this).find('.copie_externe_publication').attr('name','wcpextw['+data_index+'][publication]');
				$(this).find('.copie_externe_titre').attr('name','wcpextw['+data_index+'][titre]');
				$(this).find('.copie_externe_auteur').attr('name','wcpextw['+data_index+'][auteur]');
				$(this).find('.copie_externe_parution').attr('name','wcpextw['+data_index+'][parution]');
				$(this).find('.copie_externe_date_mel').attr('name','wcpextw['+data_index+'][date_mel]');
				
				$(this).find('.wcpextMel').each(function(){
					$(this).attr('name','wcpextw['+data_index+'][wcpextMel][]');
				});

				
				var copie_publication=$('.copie_externe_web').eq(data_index).find('.copie_externe_publication').eq(0).val().trim();
				var copie_titre=$('.copie_externe_web').eq(data_index).find('.copie_externe_titre').eq(0).val().trim();
				var copie_auteur=$('.copie_externe_web').eq(data_index).find('.copie_externe_auteur').eq(0).val().trim();
				var copie_parution=$('.copie_externe_web').eq(data_index).find('.copie_externe_parution').eq(0).val().trim();
				var copie_date_mel=$('.copie_externe_web').eq(data_index).find('.copie_externe_date_mel').eq(0).val().trim();

				
				$('.copie_externe_web[data-index='+data_index+'] .error').html('');
	
				
				if(copie_publication.length==0){
					$('.copie_externe_web[data-index='+data_index+'] .error').html("Merci de renseigner le titre de la publication ou de supprimer la ligne.");
	
					event.preventDefault();
					return;
				}else if(copie_titre.length==0){
						$('.copie_externe_web[data-index='+data_index+'] .error').html("Merci de renseigner le titre de l'article ou de supprimer la ligne.");
						event.preventDefault();
				}else if(copie_auteur.length==0){
					$('.copie_externe_web[data-index='+data_index+'] .error').html("Merci de renseigner l'auteur de l'article ou de supprimer la ligne.");
					event.preventDefault();
				}else if(copie_parution.length==0){
					$('.copie_externe_web[data-index='+data_index+'] .error').html("Merci de renseigner la date de parution de l'article ou de supprimer la ligne.");
					event.preventDefault();
				}else if(copie_date_mel.length==0){
					$('.copie_externe_web[data-index='+data_index+'] .error').html("Merci de renseigner la date de mise en ligne de la copie ou de supprimer la ligne.");
					event.preventDefault();
				}
				

			});
		}

		

		
	});
	

});

function add_default_copie_externe_web()
{
	var clone=$(copie_ext_web_structure_default).clone(true, true);
	$('#container_copie_ext_web_reseau').append(clone);
	display_default_add_copie_externe_web_button();
	init_copie_externe_web_autocomplete();
	init_copie_externe_web_pub_autocomplete();
}

function init_copie_externe_web_autocomplete(){
	$('.copie_externe_publication').devbridgeAutocomplete({
		serviceUrl: '/api-repertoire-cne-web/?',
	    onSelect: function (suggestion) {}
	});
	

	display_default_add_copie_externe_web_button()
}


function display_default_add_copie_externe_web_button()
{

	if($('.copie_externe_web').length){
		$('#default_copie_externe_web_button').hide();
	}else{
		$('#default_copie_externe_web_button').show();
	}
}


function close_copie_ext_web_reseau()
{
	return;
	$('.copie_externe_web .panorama_content:visible').each(function(){
		 if($(this).closest('.copie_externe_web').find('.panorama_content').is(":visible")){
		    	$(this).closest('.copie_externe_web').find(".ico-expend-collapse").removeClass('ico-expend');
		    	$(this).closest('.copie_externe_web').find(".ico-expend-collapse").addClass('ico-collapse');
		    }else{
		    	$(this).closest('.copie_externe_web').find(".ico-expend-collapse").removeClass('ico-collapse');
		    	$(this).closest('.copie_externe_web').find(".ico-expend-collapse").addClass('ico-expend');
		    }
		$(this).closest('.copie_externe_web').find('.panorama_content').toggle("blind", {},500, function() {});
	});
}

function toggle_copie_externe_web(item){
	

	//close_copie_ext_web_reaseau();

	if($(item).closest('.copie_externe_web').find('.copie_externe_content').is(":visible")==false){
		
		if($(item).closest('.copie_externe_web').find('.copie_externe_content').is(":visible")){
	    	$(item).parent().removeClass('ico-expend');
	    	$(item).parent().addClass('ico-collapse');
	    }else{
	    	$(item).parent().removeClass('ico-collapse');
	    	$(item).parent().addClass('ico-expend');
	    }
		
		$(item).closest('.copie_externe_web').find('.copie_externe_content').toggle("blind", {},500, function() {
		    
		    if($(item).closest('.copie_externe_web').find('input:invalid').length)
		    {
		    	$( $(item).closest('.copie_externe_web').find('input:invalid').eq(0) )[0].scrollIntoView(); 
		    }
		    
		});
	}else{
		$(item).parent().removeClass('ico-expend');
    	$(item).parent().addClass('ico-collapse');
    	$(item).closest('.copie_externe_web').find('.copie_externe_content').toggle("blind", {},500, function() {
		    
		    if($(item).closest('.copie_externe_web').find('input:invalid').length)
		    {
		    	$( $(item).closest('.copie_externe_web').find('input:invalid').eq(0) )[0].scrollIntoView(); 
		    }
		    
		});
	}



}
function add_copie_externe_web(item){
	
	var copie_index=$(item).closest('.copie_externe_web').data('index');

	var copie_publication=$(item).closest('.copie_externe_web').find('.copie_externe_publication').eq(0).val();
	var copie_titre=$(item).closest('.copie_externe_web').find('.copie_externe_titre').eq(0).val();
	var copie_auteur=$(item).closest('.copie_externe_web').find('.copie_externe_auteur').eq(0).val();
	var copie_parution=$(item).closest('.copie_externe_web').find('.copie_externe_parution').eq(0).val();

	var copie_date_mel=$(item).closest('.copie_externe_web').find('.copie_externe_date_mel').eq(0).val();


	if(copie_publication==""){
		$('.copie_externe_web[data-index='+copie_index+'] .error').html("Merci de renseigner le titre de la publication ou de supprimer la ligne.");
	}else if(copie_titre==""){
			$('.copie_externe_web[data-index='+copie_index+'] .error').html("Merci de renseigner le titre de l'article ou de supprimer la ligne.");
	}else if(copie_auteur==""){
		$('.copie_externe_web[data-index='+copie_index+'] .error').html("Merci de renseigner l'auteur de l'article ou de supprimer la ligne.");
	}else if(copie_parution==""){
		$('.copie_externe_web[data-index='+copie_index+'] .error').html("Merci de renseigner la date de parution de l'article ou de supprimer la ligne.");
	}else if(copie_date_mel==""){
		$('.copie_externe_web[data-index='+copie_index+'] .error').html("Merci de renseigner la date de mise en ligne de l'article ou de supprimer la ligne.");

	}else{
		$('.copie_externe_web[data-index='+copie_index+'] .error').html("");
		
		if($('#container_copie_ext_web_reseau > div').length){
			copie_ext_web_structure=$('#container_copie_ext_web_reseau > div').eq($('#container_copie_ext_web_reseau > div').length-1).clone()
		}else{
			copie_ext_web_structure=copie_ext_web_structure_default;
		}
		
		var clone=$(copie_ext_web_structure).clone(true, true);
		clone.find('.copie_ext_web_publications_line').each(function (index) {
			if(index>0){
				$(this).remove();
			}else{
				$(this).find('input').val('');
			}
	        var name = $(this).prop('name');

	    });

		$('#container_copie_ext_web_reseau').append(clone);
		$('#container_copie_ext_web_reseau > div').eq($('#container_copie_ext_web_reseau > div').length - 1).attr("data-index",$('#container_copie_ext_web_reseau > div').length - 1)
		
		
	    $(".copie_externe_web[data-index="+($('#container_copie_ext_web_reseau > div').length - 1)+"] .copie_externe_publication").val('');
		$(".copie_externe_web[data-index="+($('#container_copie_ext_web_reseau > div').length - 1)+"] .copie_externe_titre").val('');
		$(".copie_externe_web[data-index="+($('#container_copie_ext_web_reseau > div').length - 1)+"] .copie_externe_auteur").val('');
		$(".copie_externe_web[data-index="+($('#container_copie_ext_web_reseau > div').length - 1)+"] .copie_externe_parution").val('');
		$(".copie_externe_web[data-index="+($('#container_copie_ext_web_reseau > div').length - 1)+"] .copie_externe_date_mel").val('');
		

		init_copie_externe_web_autocomplete();
		init_copie_externe_web_pub_autocomplete();
		init_copie_externe_web_dob()

		
	}
	


	//display_default_add_copie_externe_button();
}
function delete_copie_externe_web(item){
	if(confirm('Voulez-vous supprimer cette publication web ?')){
		$(item).closest('.copie_externe_web').remove();
		if($('#container_copie_ext_web_reseau > div').length<1){
			$("#default_copie_externe_web_button").show();
		}else{
			$("#default_copie_externe_web_button").hide();
		}
		var i = 1

	}
	display_default_add_copie_externe_web_button()
}


function calculate_publications(panorama_index)
{
	var num=$('.copie_externe_web[data-index='+panorama_index+'] .copie_externe_web_publications .wcpextMel').length;
	$('.copie_externe_web[data-index='+panorama_index+'] .copie_ext_web_total_sites').html(num);
}

function add_copie_externe_web_pub(item)
{
	var panorama_index=$(item).closest('.copie_externe_web').data('index');
	var compos_title=$(item).closest('.copie_ext_web_publications_line').find('input').eq(0).val();
	if(compos_title==""){
		$('.copie_externe_web[data-index='+panorama_index+'] .error').html("Merci de renseigner l'intitulé de la plateforme de mise en ligne ou de supprimer la ligne.");
	}else{
		$('.copie_externe_web[data-index='+panorama_index+'] .copie_externe_web_publications').append(copie_externe_web_pub);
		$('.copie_externe_web[data-index='+panorama_index+'] .error').html("");
	}
	init_copie_externe_web_pub_autocomplete();
	
	init_copie_externe_web_dob();
	calculate_publications(panorama_index);
}

function remove_copie_externe_web_pub(item)
{
	var panorama_index=$(item).closest('.copie_externe_web').data('index');
	$(item).parent().parent().parent().remove();
	if($(".copie_externe_web[data-index="+panorama_index+"]  .copie_ext_web_publications_line ").length==0){
		$('.copie_externe_web[data-index='+panorama_index+'] .row-add-first-publication-press').show();
	}
	$('.copie_externe_web[data-index='+panorama_index+'] .error').html('');
	calculate_publications(panorama_index);
}

function add_first_copie_externe_web_pub(item){
	var panorama_index=$(item).closest('.copie_externe_web').data('index');
	if( $(".copie_externe_web[data-index="+panorama_index+"]  .copie_ext_web_publications_line ").length==0){
		$('.copie_externe_web[data-index='+panorama_index+'] .copie_externe_web_publications').append(copie_externe_web_pub);
		$('.copie_externe_web[data-index='+panorama_index+'] .row-add-first-publication-press').hide();
	}
	
	init_copie_externe_web_pub_autocomplete();
	
	init_copie_externe_web_dob();
	calculate_publications(panorama_index);
}


function init_copie_externe_web_pub_autocomplete(){
	return;
	$('.wpplp_label').devbridgeAutocomplete({
		serviceUrl: '/api-panorama-press-publications/?',
	    onSelect: function (suggestion) {}
	});
}

function init_copie_externe_web_dob()
{
    $('.dob').keyup(function(e) {
        var fieldval=$(this).val();
        val= fieldval.replace(/[^0-9\.]/g, '');
        var arrChar = val.split('');
        var dob="";
        for (var i = 0; i < arrChar.length; i++) {
            dob +=arrChar[i];
            if(i==1 || i==3 ){
                dob +="/";
            }
        }
        $(this).val(dob.substring(0, 10));
    });
    
    $('.dob-short').keyup(function(e) {
        var fieldval=$(this).val();
        val= fieldval.replace(/[^0-9\.]/g, '');
        var arrChar = val.split('');
        var dob="";
        for (var i = 0; i < arrChar.length; i++) {
            dob +=arrChar[i];
            if(i==1 ){
                dob +="/";
            }
        }
        $(this).val(dob.substring(0, 7));
    });
}
