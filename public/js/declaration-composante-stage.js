/*compos_stages*/
function addfirstcompos_stages(){
	$('.compos-stages-errors').html("");
	if($('.compos-line').length)
	{
		addcompos_stages($('.compos-line .col-md-add-compos a').eq($('.compos-line').length-1));
	}else{
		$('.compos-stages').append(compos_stages_line);
	}
}
function addcompos_stages(item){

	var compos_title=$(item).parent().parent().parent().parent().find('.compos-title-field input').val();
	var compos_num=$(item).parent().parent().find('input').eq(0).val();

	if(compos_num==""){
		$('.compos-stages-errors').html("Merci de renseigner les nombre de stagiaires ou de supprimer la ligne.");
	}else if(compos_title==""){
		$('.compos-stages-errors').html("Merci de renseigner l'intitulé du stage ou de supprimer la ligne.");
	}else{
		$('.compos-stages-errors').html("");
		$('.compos-stages').append(compos_stages_line);
		 $('.col-compos-num-field input').on('input', function() {
			 calculatecompos_stages();
		});
	}
	activate_ifone_get_all();
	
}

$( document ).ready(function() {

	activate_ifone_get_all()
});

function activate_ifone_get_all(){
	$('.compos-line ').each(function(){
		$(this).find('input').each(function(i, item){
			   $(item).on( "focusout, blur, change, keyup", function(){
				   compostante_stage_ifone_get_all();
				   
			   } );
		   
		  });
	});
}

function compostante_stage_ifone_get_all(){
	$('.compos-line ').each(function(){
		var activate_ifone_get_all_empty = 0;
		  $(this).find('input').each(function(i, item){
			  activate_ifone_get_all_empty = activate_ifone_get_all_empty + $(item).val().length;
		   
		  });
		  if(activate_ifone_get_all_empty>0){
			  $(this).find('input').each(function(i, item){
				  $(item).attr('required', 'required');
			   
			  });
		  }else{
			  console.log("==>  empty");
			  $(this).find('input').each(function(i, item){
				  $(item).removeAttr('required');
			   
			  });
		  }
		});
}


function removecompos_stages(item){
	$(item).parent().parent().parent().parent().remove();
	$('.compos-errors').html("");
	calculatecompos_stages();
	activate_ifone_get_all();
}

function calculatecompos_stages(){
	var sum=0;
	$('.col-md-stagiaire-field input').each(function(){
		var val=parseInt($(this).val())||0;
		sum = sum + parseInt(val);
	})
	$('.compos-total-bloc-total-stagiaires').html(sum);
	
	var sum=0;
	$('.col-md-heures-field input').each(function(){
		var val=parseInt($(this).val())||0;
		sum = sum + parseInt(val);
	})
	$('.compos-total-bloc-total').html(sum);

}