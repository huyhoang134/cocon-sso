var panorama_structure="";
var panorama_valid_Exts = new Array(".xlsx", ".xls", ".csv", ".xl");
jQuery(document).ready(function() {
	

	init_panorama_presse_papier_autocomplete();
	close_panorama_press_papier_all()
	
	
	$('.custom-file-upload-filename-text').val(panorama_valid_Exts.join(", "));
	$(" input[type=file]").val('')
		
	
	panorama_structure=$('#container_panorama_press_papier > div').eq(0).clone();

	$('.panorama_press_papier .nb-total-pages').each(function(e){
		panorama_press_papier_calculate(this);
	});
	
	


	
	$('form').submit(function( event ) {


		
		$('.panorama_press_papier').each(function(){
			var data_index=$(this).data('index');
			$(this).find('.wppl_label').attr('name','wpanoramapress_liste['+data_index+'][wppl_label]');
			$(this).find('.wppl_periode').attr('name','wpanoramapress_liste['+data_index+'][wppl_periode]');
			$(this).find('.wppl_total').attr('name','wpanoramapress_liste['+data_index+'][wppl_total]');
			$(this).find('.wppl_fichier').attr('name','wpanoramapress_liste['+data_index+'][wppl_fichier]');
			
			$(this).find('.wppl_periode_nombre').attr('name','wpanoramapress_liste['+data_index+'][wppl_periode_nombre]');
			$(this).find('.wppl_moyenne_repro').attr('name','wpanoramapress_liste['+data_index+'][wppl_moyenne_repro]');
			$(this).find('.wppl_moyenne_exemplaire').attr('name','wpanoramapress_liste['+data_index+'][wppl_moyenne_exemplaire]');
			
			
			$(this).find('.wppl_fichier_media_key').attr('name','wpanoramapress_liste['+data_index+'][wppl_fichier_media_key]');
			$(this).find('.wppl_fichier_name').attr('name','wpanoramapress_liste['+data_index+'][wppl_fichier_name]');
			$(this).find('.panorama_press_papier_publications > div').each(function(i,k){
				$(this).find('.wpplp_label').attr('name','wpanoramapress_liste['+data_index+'][publication]['+i+'][wpplp_label]');
				$(this).find('.wpplp_total').attr('name','wpanoramapress_liste['+data_index+'][publication]['+i+'][wpplp_total]');


			});
		})
		
		$('.panorama_press_papier').each(function(){
			var data_index=$(this).data('index');
			console.log($(this).find('.panorama_press_papier_publications > div').length);
			console.log($(this).find('.wppl_fichier').val().length);
			console.log($(this).find('.wppl_fichier_media_key').val().length);
			if($(this).find('.panorama_press_papier_publications > div').length < 1 && ( $(this).find('.wppl_fichier').val().length < 1 && $(this).find('.wppl_fichier_media_key').val().length <1))
			{
				$(this).find('.error').html("Vous devez reseigner au moins une publication ou charger un fichier pour ce panorama, ou supprimer celui-ci.");
				event.preventDefault();
			}
		});
		

		
	});
	
	$('.panorama_press_papier').each(function(){
		var data_index=$(this).data('index');
		if($(this).find('.wppl_fichier_media_key').length){
			if($(this).find('.panorama_press_papier_publications > div').length < 1 && $(this).find('.wppl_fichier_media_key').val().length>32)
			{
				$(this).find('.custom-file-upload-filename-text').val($(this).find('.wppl_fichier_name').val());
				$('.panorama_press_papier[data-index='+data_index+'] .row-add-first-publication-press').show();
				$(this).find('.delete_file').show();
			}
		}

	});
	
	


});

function init_panorama_presse_papier_autocomplete(){
	$('.wpplp_label').devbridgeAutocomplete({
		serviceUrl: '/api-panorama-press-publications/?',
	    onSelect: function (suggestion) {}
	});
	
	$('.panorama_press_papier input[type="file"]').change(function(e){
		var fileName = e.target.files[0].name;
	    var sender = e.target.files[0];
	    
		var panorama_index=$(this).closest('.panorama_press_papier').data('index');

	    fileName = fileName.substring(fileName.lastIndexOf('.'));
	    if (panorama_valid_Exts.indexOf(fileName) < 0) {
	    	alert("Type de fichier invalide. Veuillez séléctionner un fichier de type " + panorama_valid_Exts.toString() + ".");
	    	$(e.target).parent().find(".custom-file-upload-filename input").val(panorama_valid_Exts.toString());
	    	$(e.target).parent().find(".delete_file").hide();
	    }else{

	    	if($('.panorama_press_papier[data-index='+panorama_index+'] .panorama_press_papier_publications > div').length){
	    		
	    		if(confirm("Le chargement d'un fichier pour ce panorama nécessite la suppression des publications précédement renseignées.\r\nConfirmez-vous la supression des publications déclarées pour ce panorama ?"))
		    	{
		    		$(e.target).parent().find(".custom-file-upload-filename input").val(e.target.files[0].name);
		    		$(e.target).parent().find(".delete_file").show();
		    		$(".panorama_press_papier[data-index="+panorama_index+"] .panorama_press_papier_publications").html("");
		    		$('.panorama_press_papier[data-index='+panorama_index+'] .error').html("");
		    		$('.panorama_press_papier[data-index='+panorama_index+'] .row-add-first-publication-press').show();
		    	}else{
			    	$(e.target).parent().find(".custom-file-upload-filename input").val(panorama_valid_Exts.toString());
			    	$(e.target).parent().find(".delete_file").hide();
		    	}
	    	}else{
	    		$(e.target).parent().find(".custom-file-upload-filename input").val(e.target.files[0].name);
	    		$(e.target).parent().find(".delete_file").show();
	    	}
	    	
			
	    }
	});
	
}

function close_panorama_press_papier_all()
{
	return;
	$('.panorama_press_papier .panorama_content:visible').each(function(){
		if($(this).closest('.panorama_press_papier').find('.panorama_content').is(":visible")){
	    	$(this).closest('.panorama_press_papier').find(".ico-expend-collapse").removeClass('ico-expend');
	    	$(this).closest('.panorama_press_papier').find(".ico-expend-collapse").addClass('ico-collapse');
	    }else{
	    	$(this).closest('.panorama_press_papier').find(".ico-expend-collapse").removeClass('ico-collapse');
	    	$(this).closest('.panorama_press_papier').find(".ico-expend-collapse").addClass('ico-expend');
	    }
		$(this).closest('.panorama_press_papier').find('.panorama_content').toggle("blind", {},500, function() {});
	});
}



function toggle_panorama_press_papier(item){
	//close_panorama_all();
	if($(item).closest('.panorama_press_papier').find('.panorama_content').is(":visible")==false){
		if($(item).closest('.panorama_press_papier').find('.panorama_content').is(":visible")){
	    	$(item).parent().removeClass('ico-expend');
	    	$(item).parent().addClass('ico-collapse');
	    }else{
	    	$(item).parent().removeClass('ico-collapse');
	    	$(item).parent().addClass('ico-expend');
	    }
		$(item).closest('.panorama_press_papier').find('.panorama_content').toggle("blind", {},500, function() {
		    
		    if($(item).closest('.panorama_press_papier').find('input:invalid').length)
		    {
		    	$( $(item).closest('.panorama_press_papier').find('input:invalid').eq(0) )[0].scrollIntoView(); 
		    }
		});
	}else{
		$(item).parent().removeClass('ico-expend');
    	$(item).parent().addClass('ico-collapse');
    	$(item).closest('.panorama_press_papier').find('.panorama_content').toggle("blind", {},500, function() {
		    if($(item).closest('.panorama_press_papier').find('input:invalid').length)
		    {
		    	$( $(item).closest('.panorama_press_papier').find('input:invalid').eq(0) )[0].scrollIntoView(); 
		    }
		});
	}
}

function display_default_add_press_button()
{
	return;
	if($('.panorama_press_papier').length){
		$('#default_panorama_press_button').hide();
	}else{
		$('#default_panorama_press_button').show();
	}
}

function add_panorama_press_papier(){
	
	if($('#container_panorama_press_papier > div').length){
		panorama_structure=$('#container_panorama_press_papier > div').eq($('#container_panorama_press_papier > div').length-1).clone()
	}
	var clone=$(panorama_structure).clone(true, true);
	clone.find('input[type=radio]').each(function (index) {
        var name = $(this).prop('name');
        $(this).prop('name', name + $('#container_panorama_press_papier > div').length);
    });

	$('#container_panorama_press_papier').append(clone);

	$('#container_panorama_press_papier > div').eq($('#container_panorama_press_papier > div').length - 1).attr("data-index",$('#container_panorama_press_papier > div').length - 1)
	
	$(".panorama_press_papier[data-index="+($('#container_panorama_press_papier > div').length - 1)+"] .custom-file-upload-filename input").val(panorama_valid_Exts.toString());
	$(".panorama_press_papier[data-index="+($('#container_panorama_press_papier > div').length - 1)+"] .delete_file").hide();
	$(".panorama_press_papier[data-index="+($('#container_panorama_press_papier > div').length - 1)+"] input[type=file]").val('');
	$(".panorama_press_papier[data-index="+($('#container_panorama_press_papier > div').length - 1)+"] .wppl_label").val('');
	
	$(".panorama_press_papier[data-index="+($('#container_panorama_press_papier > div').length - 1)+"] .wppl_periode_nombre").val('');
	$(".panorama_press_papier[data-index="+($('#container_panorama_press_papier > div').length - 1)+"] .wppl_moyenne_repro").val('');
	$(".panorama_press_papier[data-index="+($('#container_panorama_press_papier > div').length - 1)+"] .wppl_moyenne_exemplaire").val('');
	$(".panorama_press_papier[data-index="+($('#container_panorama_press_papier > div').length - 1)+"] .nb-total-pages").html('');
	
	//$(".panorama_press_papier[data-index="+($('#container_panorama_press_papier > div').length - 1)+"] .wppl_total").val('')

	if($(".panorama_press_papier[data-index="+($('#container_panorama_press_papier > div').length - 1)+"] .panorama_press_papier_publications > div").length <1){
		$(".panorama_press_papier[data-index="+($('#container_panorama_press_papier > div').length - 1)+"] .row-add-first-publication-press > div").show();
	}
	
	var i = 1
	$('#container_panorama_press_papier > div').each(function(){
		$(this).find('.label-panorama-press ').html("Panorama de presse "+i);
		$(this).find('label.custom-file-upload').attr('for','file-upload-'+i);
		$(this).find('.wppl_fichier').attr('id','file-upload-'+i);
		i++;
	});
	init_panorama_presse_papier_autocomplete();
	$( document ).trigger( "chorus_pro_multi.init", [  ] );
	display_default_add_press_button();
}
function delete_panorama_press_papier(item){
	if(confirm('Voulez-vous supprimer ce panorama ?')){
		$(item).closest('.panorama_press_papier').remove();
		/*if($('#container_panorama_press_papier > div').length<1){
			$("#default_panorama_press_button").show();
		}else{
			$("#default_panorama_press_button").hide();
		}*/
		var i = 1
		$('#container_panorama_press_papier > div').each(function(){
			$(this).find('.label-panorama-press').html("Panorama de presse "+i);
			i++;
		});
	}
	
	display_default_add_press_button()

}

function add_first_panorama_press_papier_pub(item){
	var panorama_index=$(item).closest('.panorama_press_papier').data('index');
	if($(".panorama_press_papier[data-index="+panorama_index+"]  input[type=file]").val().length==0 && $(".panorama_press_papier[data-index="+panorama_index+"]  input[name=wppl_fichier_name]").val().length==0){
		$('.panorama_press_papier[data-index='+panorama_index+'] .panorama_press_papier_publications').append(pano_pub_line);
		$('.panorama_press_papier[data-index='+panorama_index+'] .row-add-first-publication-press').hide();
	}
	if(
			($(".panorama_press_papier[data-index="+panorama_index+"]  input[type=file]").val().length > 0 || $(".panorama_press_papier[data-index="+panorama_index+"]  input[name=wppl_fichier_name]").val().length > 0) 
			
			&& confirm("L'ajout d'une publication nécessite la suppresion du fichier précédement chargé.\r\nConfirmez-vous la supression du fichier chargé pour ce panorama ?"))
	{
		$(".panorama_press_papier[data-index="+panorama_index+"] .custom-file-upload-filename input").val(panorama_valid_Exts.toString());
		$(".panorama_press_papier[data-index="+panorama_index+"]  input[type=file]").val('');
		$(".panorama_press_papier[data-index="+panorama_index+"] .delete_file").hide();
		$(".panorama_press_papier[data-index="+panorama_index+"]  input[name=wppl_fichier_media_key]").val("");
		$(".panorama_press_papier[data-index="+panorama_index+"]  input[name=wppl_fichier_name]").val("");
		$('.panorama_press_papier[data-index='+panorama_index+'] .panorama_press_papier_publications').append(pano_pub_line);
		$('.panorama_press_papier[data-index='+panorama_index+'] .row-add-first-publication-press').hide();
	}
	init_panorama_presse_papier_autocomplete();
}

function add_panorama_press_papier_pub(item){
	var panorama_index=$(item).closest('.panorama_press_papier').data('index');
	
	var compos_title=$(item).closest('.panorama_press_papier_publications_line').find('input').eq(0).val();
	var compos_num=$(item).closest('.panorama_press_papier_publications_line').find('input').eq(1).val();
	if(compos_num==""){
		$('.panorama_press_papier[data-index='+panorama_index+'] .error').html("Merci de renseigner le nombre d'articles utilisés ou de supprimer la ligne.");
	}else if(compos_title==""){
		$('.panorama_press_papier[data-index='+panorama_index+'] .error').html("Merci de renseigner l'intitulé de la publication ou de supprimer la ligne.");
	}else{
		$('.panorama_press_papier[data-index='+panorama_index+'] .panorama_press_papier_publications').append(pano_pub_line);
		$('.panorama_press_papier[data-index='+panorama_index+'] .error').html("");
	}
	init_panorama_presse_papier_autocomplete();
}

function remove_panorama_press_papier_pub(item)
{
	var panorama_index=$(item).closest('.panorama_press_papier').data('index');
	$(item).parent().parent().parent().remove();
	if($('.panorama_press_papier[data-index='+panorama_index+'] .panorama_press_papier_publications > div').length<1){
		$('.panorama_press_papier[data-index='+panorama_index+'] .row-add-first-publication-press').show();
	}
	$('.panorama_press_papier[data-index='+panorama_index+'] .error').html('');
	
}


function reset_file_upload_panorama_press(el){
	$(el).parent().find('input').val(panorama_valid_Exts.toString());
	$(el).parent().parent().find("file").val('');
	$(el).parent().find(".delete_file").hide();
}

function panorama_press_papier_calculate(item){
	var total = 0;
	wppl_periode_nombre = ( parseInt( $(item).closest('.panorama_press_papier').find('.wppl_periode_nombre').val())||0);
	wppl_moyenne_repro = (parseInt($(item).closest('.panorama_press_papier').find('.wppl_moyenne_repro').val())||0);
	wppl_moyenne_exemplaire = (parseInt($(item).closest('.panorama_press_papier').find('.wppl_moyenne_exemplaire').val())||0);
	total=wppl_periode_nombre * wppl_moyenne_repro * wppl_moyenne_exemplaire;
	/*$(item).closest('.panorama_press_papier').find('.wpplp_total').each(function(){
		var val=$(this).val();
		val=parseInt(val);
		if(val>0){
			total = total + parseInt(val);
		}
		
	});*/
	$(item).closest('.panorama_press_papier').find('.nb-total-pages').html(total)
}

